import win32print

def check_printer_ready():
    printer_name = "Xprinter XP-420B"
    handle = win32print.OpenPrinter(printer_name)
    status = win32print.GetPrinter(handle, 2)['Attributes']

    if status == 6728:
        return "online"
    else:
        return "offline"

