from collections import defaultdict
import datetime
from itertools import chain
import time
import logging
from analysis.models import MubeaIdent, FaureciaIdent, MubeaStock, MubeaOrders, AdientIdent, AdientOrders
from tools.models import Hangers, Barcodes
from django.db.models import Sum, Max


logger = logging.getLogger(__name__)

def translate_to_polish(date_string):
    days = {
        'Monday'   : 'poniedziałek',
        'Tuesday'  : 'wtorek',
        'Wednesday': 'środa',
        'Thursday' : 'czwartek',
        'Friday'   : 'piątek',
        'Saturday' : 'sobota',
        'Sunday'   : 'niedziela'
    }

    months = {
        'Jan': 'sty',
        'Feb': 'lut',
        'Mar': 'mar',
        'Apr': 'kwi',
        'May': 'maj',
        'Jun': 'cze',
        'Jul': 'lip',
        'Aug': 'sie',
        'Sep': 'wrz',
        'Oct': 'paź',
        'Nov': 'lis',
        'Dec': 'gru'
    }

    day, day_number, month = date_string.split()
    translated_day = days[day] if day in days else day
    translated_month = months[month] if month in months else month

    return f"{translated_day} {day_number} {translated_month}"


def split_values(lst, mult):
    mult = 1 if mult == 0 else mult
    # Find indices of non-empty values
    result = [0] * len(lst)
    non_empty_indices = [i for i, value in enumerate(lst) if value != '']
    prev_val = 0
    for indice in non_empty_indices:
        value = lst[indice] + prev_val
        split_value = value // (indice + 1)

        for i in range(indice + 1):
            if value >= 0:
                result[i] += split_value
                value -= split_value
            else:
                prev_val = value
                break

    #result = ['' if element == 0 else element for element in result]
    return result


def process_demand_data(demand_model, stock_model, prod_model, ship_model, ident_model, ident_dict, date_start, date_end, day_list):
    demand_query = demand_model.objects.filter(
        date__range=[date_start, date_end + datetime.timedelta(days=2)],
    ).values('ident__number', 'ident__description', 'date', 'quantity')

    for item in demand_query:
        item['date'] = item['date'] - datetime.timedelta(days=2)
        if item['date'] < day_list[0]:
            item['date'] = day_list[0]

        ident_key = f"{item['ident__number']}\n{item['ident__description']}"

        if ident_key not in ident_dict:
            ident_dict[ident_key] = ['' for _ in range(len(day_list) + 1)]
            # Add stock
            try:
                stock = stock_model.objects.filter(
                    ident__number=item['ident__number'],
                    date=date_start,
                ).values('quantity').first()
            except Exception as e :
                stock = False

            ident_dict[ident_key].append(stock['quantity'] if stock else 0)
            # Add sum demand
            try:
                demand = demand_model.objects.filter(
                    ident__number=item['ident__number'],
                    date__range=[date_start, date_end + datetime.timedelta(days=2)],
                ).aggregate(sum_quantity=Sum('quantity'))
            except Exception as e :
                stock = False

            ident_dict[ident_key].append(demand['sum_quantity'] if demand else 0)

            # Add production
            try:
                production = prod_model.objects.filter(
                    ident__number=item['ident__number'],
                    date__range=[date_start, date_end],
                ).values('ident__number').annotate(quantity=Sum('l1_quantity') + Sum('l2_quantity') + Sum('l7_quantity'))
                ident_dict[ident_key].append(production[0]['quantity'] if production else 0)
            except Exception as e :
                production = prod_model.objects.filter(
                    ident__number=item['ident__number'],
                    date__range=[date_start, date_end],
                ).values('quantity').first()
                ident_dict[ident_key].append(production['quantity'] if production else 0)
                production = False
            
            # Add shipment
            try:
                shipment = ship_model.objects.filter(
                    ident__number=item['ident__number'],
                    date__range=[date_start, date_end],
                ).values('ident__number', 'quantity').first()
            except Exception as e :
                shipment = False
            
            ident_dict[ident_key].append(shipment['quantity'] if shipment else 0)

            # Add line and hanger and capacity
            ident = ident_model.objects.filter(
                number=item['ident__number'],
            ).values('line', 'hanger', 'capacity').first()

            if ident:
                ident_dict[ident_key].extend([ident['line'], ident['hanger'], ident['capacity']])
            else:
                ident_dict[ident_key].extend([0, 0, 0])

        ident_dict[ident_key][day_list.index(item['date'])] = item['quantity']


def calc_days(date_start, date_end, day_list, sub_deli, sub_prod, prio_ls, excl_ls):
    hangers_instance = Hangers.objects.latest('id')
    hanger_cap = {
        1: hangers_instance.line1,
        2: hangers_instance.line2,

    }
    t_start = time.time()
    mubea_demand_query = MubeaIdent.objects.filter(
        mubeademand__date__range=[date_start, date_end + datetime.timedelta(days=2)]
    ).annotate(
        demand_sum=Sum('mubeademand__quantity'),
    ).order_by('-demand_sum')

    faurecia_demand_query = FaureciaIdent.objects.filter(
        faureciademand__date__range=[date_start, date_end + datetime.timedelta(days=2)]
    ).annotate(
        demand_sum=Sum('faureciademand__quantity'),
    ).order_by('-demand_sum')

    adient_demand_query = AdientIdent.objects.filter(
        adientorders__dateDelivered__range=[date_start - datetime.timedelta(days=7), date_end + datetime.timedelta(days=2)]
    ).annotate(
        demand_sum=Sum('adientorders__quantity'),
    ).order_by('-demand_sum')
    

    mubea_shipment_query = MubeaIdent.objects.filter(
        mubeashipment__date__range=[date_start, date_end]
    ).annotate(
        shipment_sum=Sum('mubeashipment__quantity'),
    )

    faurecia_shipment_query = FaureciaIdent.objects.filter(
        faureciashipment__date__range=[date_start, date_end]
    ).annotate(
        shipment_sum=Sum('faureciashipment__quantity'),
    )


    mubea_production_query = MubeaIdent.objects.filter(
        mubeaproduction__date__range=[date_start, date_end]
    ).annotate(
        production_sum=Sum('mubeaproduction__l1_quantity') + Sum('mubeaproduction__l2_quantity') + Sum('mubeaproduction__l7_quantity'),
    )

    faurecia_production_query = FaureciaIdent.objects.filter(
        faureciaproduction__date__range=[date_start, date_end]
    ).annotate(
        production_sum=Sum('faureciaproduction__quantity'),
    )


    mubea_stock_query = MubeaIdent.objects.filter(
        mubeastock__date=MubeaStock.objects.aggregate(latest_date=Max('date'))['latest_date']
    ).annotate(
        stock_sum=Sum('mubeastock__quantity'),
    )
    # END QUERTY

    # ).order_by('-demand_sum', Case(
    #     *[When(number=number, then=pos) for pos, number in enumerate(priority_numbers)],
    #     default=len(priority_numbers), output_field=IntegerField()
    # ))

    # Merge the querysets
    merged_demand_query = list(chain(mubea_demand_query, faurecia_demand_query, adient_demand_query))

    merged_query_with_ratio = []
    for item in merged_demand_query:
        try:
            item.order_capacity_ratio = item.demand_sum / item.capacity
        except:
            item.order_capacity_ratio = item.demand_sum / 1

        merged_query_with_ratio.append(item)

    merged_demand_sorted = sorted(merged_query_with_ratio, key=lambda x: x.order_capacity_ratio, reverse=True)

    no_prio = []
    w_prio = []
    for x in merged_demand_sorted:
        if x.number not in prio_ls:
            no_prio.append(x)
        else:
            w_prio.append(x)
    
    w_prio = sorted(w_prio, key=lambda x: prio_ls.index(x.number))
    w_prio.extend(no_prio)
    merged_demand_sorted = w_prio

    shipment = {}
    for item in list(chain(mubea_shipment_query, faurecia_shipment_query)):
        shipment[item.number] = item.shipment_sum

    production = {}
    for item in list(chain(mubea_production_query, faurecia_production_query)):
        production[item.number] = item.production_sum

    stock = {}
    for item in mubea_stock_query:
        stock[item.number] = item.stock_sum

    # Get how many id are before control
    chni_codes = set(Barcodes.objects.filter(line="Chromoniklowanie").values_list('code', flat=True))
    qa_codes = set(Barcodes.objects.filter(line="Kontrola jakości").values_list('code', flat=True))
    # Convert the QuerySet to a list
    res_set = chni_codes - qa_codes
    filtered_orders_mub = MubeaOrders.objects.filter(orderId__in=res_set)

    buffor_dict = {}
    for i in filtered_orders_mub:
        buffor_dict[i.ident.number] = i.quantity


    day_id_qty_dict = {}
    ident_stat = {}
    for ident in merged_demand_sorted:
        if ident.line not in [1, 2]:
            ident.line = 1

        ident_stat_list = []

        ident_stat_list.append(getattr(ident, "description", ''))
        ident_stat_list.append(getattr(ident, "demand_sum", ''))
        ident_stat_list.append(getattr(ident, "line", ''))
        ident_stat_list.append(getattr(ident, "hanger", ''))
        ident_stat_list.append(getattr(ident, "capacity", ''))
        ident_stat_list.append(getattr(ident, "type", ''))
        ident_stat_list.append(getattr(ident, "client", ''))

        ident_stat[ident.number] = ident_stat_list

        if sub_deli:
            ident.demand_sum -= shipment.get(ident.number, 0)

        elif sub_prod:
            ident.demand_sum -= production.get(ident.number, 0)

    for day in day_list:
        hanger_cap_day = hanger_cap.copy()
        day_id_qty_dict[day] = {}
        # 1st iteration < less than 50% of cap
        for ident in merged_demand_sorted:
            if ident.line not in [1, 2]:
                ident.line = 1

            ident_number = ident.number
            day_id_qty_dict[day][ident_number] = 0
            if day.weekday() == 6:
                continue
            
            if ident.demand_sum <= 0:
                continue
            
            if ident_number in excl_ls:
                continue

            #multiplier = ident.capacity
            if ident_number in prio_ls:
                min_to_add = int(min(hanger_cap[ident.line] // 1.5, ident.demand_sum, hanger_cap_day[ident.line]))
            else:
                # max third of daily cap
                min_to_add = int(min(hanger_cap[ident.line] // 6, ident.demand_sum, hanger_cap_day[ident.line]))
            #if min_to_add < 0.6 * multiplier:
            multiplier = 100

            to_add = int(min_to_add // multiplier * multiplier) if min_to_add / multiplier >= 2 else 0

            ident.demand_sum -= to_add
            hanger_cap_day[ident.line] = hanger_cap_day.get(ident.line, 0) - to_add
            day_id_qty_dict[day][ident_number] = day_id_qty_dict[day].get(ident_number, 0) + to_add


        # 2nd iteration
        for ident in merged_demand_sorted:
            if ident.line not in [1, 2]:
                ident.line = 1

            ident_number = ident.number
            if day.weekday() == 6:
                continue

            if ident.demand_sum <= 0:
                continue

            if ident_number in excl_ls:
                continue

            #multiplier = ident.capacity
            min_to_add = int(min(hanger_cap[ident.line], ident.demand_sum, hanger_cap_day[ident.line]))
            #if min_to_add < multiplier:
            multiplier = 100

            to_add = int(min_to_add // multiplier * multiplier) if min_to_add / multiplier > 0 else 0

            ident.demand_sum -= to_add
            hanger_cap_day[ident.line] = hanger_cap_day.get(ident.line, 0) - to_add
            day_id_qty_dict[day][ident_number] = day_id_qty_dict[day].get(ident_number, 0) + to_add


    flipped_dict = defaultdict(list)

    # Iterate through the original dictionary and populate the result dictionary
    total_iterations = len(day_id_qty_dict)
    for index, (date, inner_dict) in enumerate(sorted(day_id_qty_dict.items())):
        for id_, quantity in inner_dict.items():
            if quantity == 0:
                flipped_dict[f"{id_}\n{ident_stat.get(id_, 0)[0]}"].append('')
            else:
                flipped_dict[f"{id_}\n{ident_stat.get(id_, 0)[0]}"].append(quantity)

        if index == total_iterations - 1:

            for id_ in inner_dict.keys():
                flipped_dict[f"{id_}\n{ident_stat.get(id_, 0)[0]}"].insert(0, ident_stat.get(id_, 0)[6])
                flipped_dict[f"{id_}\n{ident_stat.get(id_, 0)[0]}"].insert(0, ident_stat.get(id_, 0)[5])
                flipped_dict[f"{id_}\n{ident_stat.get(id_, 0)[0]}"].append('')
                flipped_dict[f"{id_}\n{ident_stat.get(id_, 0)[0]}"].append(stock.get(id_, 0))
                flipped_dict[f"{id_}\n{ident_stat.get(id_, 0)[0]}"].append(ident_stat.get(id_, 0)[1])
                flipped_dict[f"{id_}\n{ident_stat.get(id_, 0)[0]}"].append(production.get(id_, 0))
                flipped_dict[f"{id_}\n{ident_stat.get(id_, 0)[0]}"].append(shipment.get(id_, 0))
                flipped_dict[f"{id_}\n{ident_stat.get(id_, 0)[0]}"].append(ident_stat.get(id_, 0)[2])
                flipped_dict[f"{id_}\n{ident_stat.get(id_, 0)[0]}"].append(buffor_dict.get(id_, 0))
                # hanger flipped_dict[f"{id_}\n{ident_stat.get(id_, 0)[0]}"].append(ident_stat.get(id_, 0)[3])
                flipped_dict[f"{id_}\n{ident_stat.get(id_, 0)[0]}"].append(ident_stat.get(id_, 0)[4])

                if ident_stat.get(id_, 0)[2] == '1' or ident_stat.get(id_, 0)[2] == 1:
                    flipped_dict[f"{id_}\n{ident_stat.get(id_, 0)[0]}"].append("5 + 1 op")
                elif ident_stat.get(id_, 0)[2] == '2' or ident_stat.get(id_, 0)[2] == 2:
                    flipped_dict[f"{id_}\n{ident_stat.get(id_, 0)[0]}"].append("3 + 1 op")
                
    elapsed_time = time.time() - t_start

    return dict(flipped_dict), elapsed_time


def make_plan(date_start, date_end, sub_deli, sub_prod, prio, excl):
    try:
        date_start = date_start.date()
        date_end = date_end.date()
    except Exception:
        pass

    n_days = (date_end - date_start).days
    day_list = []
    # make days range
    for i in range(n_days + 1):
        day_list.append(date_start + datetime.timedelta(days=i))

    if ',' in prio:
        if prio[0] == ',':
            prio = prio[1:]
        prio_ls = [int(x) for x in prio.split(',')]
    else:
        prio_ls = [int(prio)] if prio != '' else []

    if ',' in excl:
        if excl[0] == ',':
            excl = excl[1:]
        excl_ls = [int(x) for x in excl.split(',')]
    else:
        excl_ls = [int(excl)] if excl != '' else []

    date_start = date_start - datetime.timedelta(days=1)
    ident_dict, e_time = calc_days(date_start, date_end, day_list, sub_deli, sub_prod, prio_ls, excl_ls)

    day_list = [translate_to_polish(x.strftime("%A %d %b")) for x in day_list]
    day_list.insert(0, 'Klient')
    day_list.insert(0, 'Typ')
    day_list.append('Suma')
    day_list.append('Stan')
    day_list.append('OV')
    day_list.append('Prod.')
    day_list.append('Wys.')
    day_list.append('Linia')
    day_list.append('Przed\nkontrolą')
    day_list.append('Poj.')
    day_list.append('os./ \nzmiana')
    # For html (how many before the border)
    n_days += 1

    return day_list, ident_dict, n_days, e_time
