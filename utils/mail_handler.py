import configparser
import smtplib

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


config = configparser.ConfigParser()
config.read('config.ini')

SENDER_EMAIL = config['Credentials']['mail_app_sender_mail']
SENDER_PASSWORD = config['Credentials']['mail_app_password']

SMTP_SERVER = config['Credentials']['mail_app_server']
SMTP_PORT = int(config['Credentials']['mail_app_port'])

MINUTES = 20

def send_email(subject, body, receiver):
    message = MIMEMultipart()
    message["From"] = SENDER_EMAIL
    message["To"] = receiver
    message["Subject"] = subject

    # Attach the body of the email
    message.attach(MIMEText(body, "plain"))

    try:
        server = smtplib.SMTP_SSL(SMTP_SERVER, SMTP_PORT)
        server.login(SENDER_EMAIL, SENDER_PASSWORD)
        server.sendmail(SENDER_EMAIL, message["To"], message.as_string())
        server.quit()
        print("Email sent successfully")
    except Exception as e:
        print(f"Error sending email: {str(e)}")
