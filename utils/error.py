import datetime
import time
import traceback
from contextlib import ContextDecorator
from dataclasses import dataclass, field
from typing import Any, Callable, ClassVar, Dict, Optional

class ErrorDecoratorError(Exception):
    """A custom exception used to report errors in use of ErrorDecorator"""

@dataclass
class Error(ContextDecorator):
    """Decorate a function to capture and handle errors"""

    errors: ClassVar[Dict[str, int]] = {}
    name: Optional[str] = None
    text: str = "An error occurred: {}"
    logger: Optional[Callable[[str], None]] = print
    _error_count: int = field(default=0, init=False, repr=False)
    try_n: int = 0


    def __post_init__(self) -> None:
        """Initialization: add decorator to dict of decorators"""
        if self.name:
            self.errors.setdefault(self.name, 0)

    def __call__(self, func):
        def wrapper(*args, **kwargs):
            # Attempt the function execution with retries
            while self.try_n <= 10:  # Up to 10 attempts
                try:
                    result = func(*args, **kwargs)  # Call the decorated function
                    self.try_n = 0  # Reset retries on success
                    return result
                except Exception as e:
                    self.try_n += 1
                    self.handle_error(func, e)  # Log and handle the error
                    if self.try_n >= 10:  # Max retries reached
                        raise  # Re-raise the exception after final attempt
                    time.sleep(5)  # Delay before the next retry
        return wrapper

    def handle_error(self, func: Callable, error: Exception):
        """Handle the error and report it"""
        if self.logger:
            if self.try_n == 1:
                self.logger(f"[{datetime.datetime.now()}]" + self.text.format(error))
            else:
                self.logger(f"[{datetime.datetime.now()}] retry {self.try_n}" + self.text.format(error))

            if (isinstance(error, FileExistsError) and error.errno == 2) or \
                (isinstance(error, FileNotFoundError) and error.errno == 2) or \
                (isinstance(error, OSError) and error.errno == 22) or \
                (isinstance(error, PermissionError) and error.errno == 13):
                pass
                
            else:
                traceback_str = traceback.format_exc()
                self.logger(f"Traceback:\n{traceback_str}")

        if self.name:
            self.errors[self.name] += 1

    def __enter__(self) -> "Error":
        """Start capturing errors as a context manager"""
        return self

    def __exit__(self, *exc_info: Any) -> None:
        """Stop the context manager for error capture"""

