import datetime
from django.db.models.functions import Substr
from django.db.models import Subquery
from analysis.models import BrussOrders, MetalMartinOrders, HutchinsonOrders
from tools.models import Barcodes
from collections import defaultdict

def b_query(n_days=7):
    # Subquery to get orderId from Barcodes where line is specified
    bruss_codes_subquery = Barcodes.objects.filter(
        line__in=["Magazyn (Kolejowa) Bruss WZ", "Fosforanowanie", "Kontrola jakości (KTL)"],
        timestamp__gt=datetime.datetime.now() - datetime.timedelta(days=n_days)
    ).annotate(
        order_id=Substr('code', 4, None)  # Extract substring starting from the 4th character to the end
    ).values('order_id')

    # Query BrussOrders where orderId is not in the list of order_ids from the subquery
    bruss_orders = BrussOrders.objects.filter(
        dateConfirmed__gt=datetime.datetime.now() - datetime.timedelta(days=n_days),
        use_in_plan_p=True
    ).exclude(
        orderId__in=Subquery(bruss_codes_subquery)
    )

    return bruss_orders


def mm_query(n_days=7):
    # Subquery to get codes from Barcodes where line is specified
    mm_codes_subquery = Barcodes.objects.filter(
        line__in=["Magazyn wysyłkowy (Kolejowa)", "Fosforanowanie", "Kontrola jakości (KTL)"]
    ).values('code')

    # Query MetalMartinOrders where orderId is not in the list of order_ids from the subquery
    mm_orders = MetalMartinOrders.objects.filter(
        dateDelivered__gt=datetime.datetime.now() - datetime.timedelta(days=n_days),
        use_in_plan_p=True
    ).exclude(
        orderId__in=Subquery(mm_codes_subquery)
    )

    return mm_orders


def get_data(bruss_orders=0, mm_orders=0):
    if bruss_orders == 0:
        bruss_orders=b_query()
    if mm_orders == 0:
        mm_orders=mm_query()

    # Processing BrussOrders
    processed_bruss_orders = []
    for order in bruss_orders:
        if order.dateDeadline is None:
            continue

        order_id = order.orderId
        if order_id == 'nan':
            continue
        
        if order_id[0] == "B":
            order_id = "000" + order_id
        elif order_id[0:3] == "GTM":
            order_id = "MRG" + order_id
        else:
            order_id = "KES" + order_id

        ph_exits = order.ident.n_exits_ph if order.ident.n_exits_ph != 0 else 100

        processed_bruss_orders.append({
            'dateDelivered': order.dateConfirmed,
            'dateDeadline': order.dateDeadline,
            'deadlineHour': '',
            'description': f'{order.ident.number} {order.ident.description}',
            'delivery_note': order.delivery_note,
            'quantity': order.quantity,
            'orderId': order_id,
            'hanger': order.ident.hanger_ph,
            'n_exits_ph': round_to_half(order.quantity / ph_exits)
        })

    # Processing MetalMartinOrders
    processed_mm_orders = []
    for order in mm_orders:
        ph_exits = order.ident.n_exits_ph if order.ident.n_exits_ph != 0 else 100

        processed_mm_orders.append({
            'dateDelivered': order.dateDelivered,
            'dateDeadline': order.dateDelivered + datetime.timedelta(days=1),
            'deadlineHour': '',
            'description': f'{order.ident.number} {order.ident.description}',
            'delivery_note': '',
            'quantity': order.quantity,
            'orderId': order.orderId,
            'hanger': 'zawieszka',
            'n_exits_ph': round_to_half(order.quantity / ph_exits)
        })

    # Combine and sort orders by dateDeadline
    combined_orders = processed_bruss_orders + processed_mm_orders

    sorted_orders = sorted(combined_orders, key=lambda x: x['dateDeadline'])

    return sorted_orders


def split_orders_into_days_and_prios(sorted_orders, prio_limit=3*50):
    current_date = datetime.datetime.now().date()
    orders_by_day_and_prios = defaultdict(lambda: defaultdict(list))
    
    prio_names = ['zawieszka', 'tromel', 'aluminium']
    current_prio_index = 2
    current_prio_n_exits_ph = 0
    current_day = current_date

    # Initialize all days and prios
    # for i in range(1):
    #     day = current_date + datetime.timedelta(days=i)
    for prio in prio_names:
        orders_by_day_and_prios[current_date][prio] = []
    
    for order in sorted_orders:
        # Skip Sundays
        # if current_day.weekday() == 6:
        #     current_day += datetime.timedelta(days=1)
        
        # if current_prio_n_exits_ph > prio_limit:
        #     # current_prio_index = (current_prio_index + 1) % 3
        #     current_prio_n_exits_ph = 0
        #     # if current_prio_index == 0:
        #     current_day += datetime.timedelta(days=1)
        #     # Skip Sundays
        #     if current_day.weekday() == 6:
        #         current_day += datetime.timedelta(days=1)
        
        orders_by_day_and_prios[current_day][order['hanger']].append(order)
        current_prio_n_exits_ph += order['n_exits_ph']
    
    # Convert defaultdict to dict for Django template compatibility
    return dict(orders_by_day_and_prios)


def round_to_half(num):
    rounded_num = round(num * 2) / 2
    return int(rounded_num) if rounded_num.is_integer() else rounded_num


def make_plan():
    sorted_o = get_data()
    sorted_obj = split_orders_into_days_and_prios(sorted_o)

    return sorted_obj