import logging
import datetime
import pandas as pd
from utils.calculations import start_end_day_cw

from mysite import settings

from utils.timer import Timer
from analysis.models import BrussOrders, MubeaOrders, SiegeniaOrders, FaureciaOrders, MetalMartinOrders, AdientOrders
from tools.models import PrasyLabel

logger = logging.getLogger(__name__)

@Timer(text=" FUNCTION:    mubea_get_demand_overview    {:0.4f} seconds", logger=logger.info)
def mubea_get_demand_overview(calendar_week):
    df = pd.read_excel(settings.OVERVIEW_PATH, sheet_name=f'CW{calendar_week}')
    df = df.sort_values(by='quantity', ascending=False)

    return df


@Timer(text=" FUNCTION:    mubea_get_stock              {:0.4f} seconds", logger=logger.info)
def mubea_get_stock(calendar_week):
    first_day, last_day = start_end_day_cw(calendar_week)
    df = pd.read_excel(settings.RAW_MATERIAL_PATH,
                       sheet_name=f'aktualny raw',
                       header=9,
                       usecols=['delivery date ', 'Gotowe', 'Quantity', 'data produkcji '],
                       )

    df = df[(df.apply(lambda row: isinstance(row['delivery date '], datetime.datetime), axis=1))]
    df = df[(df.apply(lambda row: row['delivery date '] < last_day, axis=1))]

    df['data produkcji '] = pd.to_datetime(df['data produkcji '], errors='coerce')
    df = df[(df['data produkcji '].isna()) | (df['data produkcji '] >= first_day)]

    df = df.groupby(['Gotowe'])['Quantity'].sum().reset_index()

    return df


@Timer(text=" FUNCTION:    mubea_get_production         {:0.4f} seconds", logger=logger.info)
def mubea_get_production(calendar_week):
    first_day, last_day = start_end_day_cw(calendar_week)
    df = pd.read_excel(settings.PRODUCTION_PATH,
                       sheet_name=f'Production ',
                       skiprows=range(1, 11000),
                       usecols=['Ident.', 'Teilart', 'Data', 'KW', 'Produktion linie I', 'Produktion linie II', 'Produktion linie VII CHROM III']
                       )

    df = df.loc[(df['Data'] >= first_day) &
                (df['Data'] <= last_day)]
    
    df[['Produktion linie I', 'Produktion linie II', 'Produktion linie VII CHROM III']] = df[['Produktion linie I', 'Produktion linie II', 'Produktion linie VII CHROM III']].fillna(0)
    df['Production Total'] = df['Produktion linie I'] + df['Produktion linie II'] + df['Produktion linie VII CHROM III']

    df = df.groupby(['Ident.', 'Teilart'])['Production Total'].sum().reset_index()

    return df


@Timer(text=" FUNCTION:    mubea_get_shipments          {:0.4f} seconds", logger=logger.info)
def mubea_get_shipments(calendar_week):
    first_day, last_day = start_end_day_cw(calendar_week)
    df = pd.read_excel(settings.SHIPMENT_PATH,
                       sheet_name=f'nowa specyfikacja ',
                       header=4,
                       skiprows=range(5, 18000),
                       usecols=['Datum','Teile-Nummer', 'Anzahl der Teile Szt']
                       )
    df = df.rename(columns={'Anzahl der Teile Szt': 'Shipment Total'})
    df = df[df.apply(lambda row: isinstance(row['Datum'], datetime.datetime), axis=1)]
    df = df.loc[(df['Datum'] >= first_day) &
                (df['Datum'] <= last_day)]

    df.dropna(subset=['Teile-Nummer', 'Shipment Total'], inplace=True)

    df['Teile-Nummer'] = df['Teile-Nummer'].astype(int)

    df = df.groupby('Teile-Nummer')['Shipment Total'].sum().reset_index()

    return df


@Timer(text=" FUNCTION:    mubea_get_ident_legend       {:0.4f} seconds", logger=logger.info)
def mubea_get_ident_legend():
    df = pd.read_excel(settings.PRODUCTION_PATH,
                       sheet_name=f'Legenda NEW formula',
                       usecols=['Mubea\nPart Number ', 'Teilart']
                       )
    
    return df


def get_latest_file(folder_path, start_str, end_str):
    import os

    files = os.listdir(folder_path)
    filtered_files = [file for file in files if file.startswith(start_str) and file.endswith(end_str)]
    filtered_files.sort(key=lambda x: os.path.getmtime(folder_path + '\\' + x), reverse=True)
    latest_file_path = folder_path + '\\' + filtered_files[0]

    return latest_file_path


def get_add_bcode(code):
    try:
        if code[0:3] == 'KES' or code[0:3] == 'MRG' or code[0:4] == '000B':
            first_matching_object = BrussOrders.objects.filter(orderId=code[3:]).first()
            if first_matching_object is not None:
                return str(first_matching_object.ident.number) + '\n' + str(first_matching_object.ident.description) + '\n' + str(first_matching_object.quantity) + 'pcs.'
            else:
                raise LookupError(f"Code {code} not found in DB")
            
        elif code[0:3] == 'MUB' or (code.isdigit() and len(code) == 6):
            first_matching_object = MubeaOrders.objects.filter(orderId=code).first()
            if first_matching_object is not None and first_matching_object.ident.number is not None and first_matching_object.ident.description is not None:
                return str(first_matching_object.ident.number) + ' \n' + str(first_matching_object.ident.description) + '\n' + str(first_matching_object.quantity) + 'pcs.'
            else:
                raise LookupError(f"Code {code} not found in DB")
        
        elif code[0:2] == '30' and len(code) == 5:
            first_matching_object = BrussOrders.objects.filter(orderId=code).first()
            if first_matching_object is not None:
                return first_matching_object.ident.description
            else:
                raise LookupError(f"Code {code} not found in DB")
            
        elif code[5] == '-':
            code_id = int(code.split('-')[0])
            first_matching_object = PrasyLabel.objects.filter(number=code_id).first()

            try:
                item_desc = first_matching_object.description
                return item_desc
            except:
                raise LookupError(f"Code {code} not found in DB")

        elif code[0:3] == 'SGN':
            first_matching_object = SiegeniaOrders.objects.filter(orderId=code).first()
            if first_matching_object is not None:
                return str(first_matching_object.ident.number) + ' \n' + str(first_matching_object.ident.description) + '\n' + str(first_matching_object.quantity) + 'pcs.'
            else:
                raise LookupError(f"Code {code} not found in DB")
            
        elif code[0] == 'F' or code[0:3] == 'TKN':
            first_matching_object = FaureciaOrders.objects.filter(orderId=code).first()
            if first_matching_object is not None:
                return str(first_matching_object.ident.number) + ' \n' + str(first_matching_object.ident.description) + '\n' + str(first_matching_object.quantity) + 'pcs.'
            else:
                raise LookupError(f"Code {code} not found in DB")

        elif code[0:3] == 'MET':
            first_matching_object = MetalMartinOrders.objects.filter(orderId=code).first()
            if first_matching_object is not None:
                return str(first_matching_object.ident.number) + ' \n' + str(first_matching_object.ident.description) + '\n' + str(first_matching_object.quantity) + 'pcs.'
            else:
                raise LookupError(f"Code {code} not found in DB")

        elif code[0:3] == 'ADI':
            first_matching_object = AdientOrders.objects.filter(orderId=code).first()
            if first_matching_object is not None:
                return str(first_matching_object.ident.number) + ' \n' + str(first_matching_object.ident.description) + '\n' + str(first_matching_object.quantity) + 'pcs.'
            else:
                raise LookupError(f"Code {code} not found in DB")

    except Exception as e:
        logger.info(f'[{datetime.datetime.now()}] GET Barcode info FAILURE {e}')
        return ''
