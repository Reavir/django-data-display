import datetime
import logging
import pandas as pd
import re
from utils.calculations import convert_to_int_or_zero, func_whitespace

from mysite import settings

from utils.timer import Timer
from utils.error import Error
from utils.get_data import get_latest_file

from tools.models import Barcodes

from django.core.exceptions import ValidationError
from django.db import transaction
from django.db.models import Max

logger = logging.getLogger(__name__)
orderLogger = logging.getLogger('orderLogger')

pd.set_option('future.no_silent_downcasting', True)

@Timer(text=" FUNCTION: " + func_whitespace("mubea_update_demand_overview") + "{:0.4f} seconds", logger=logger.info)
@Error(text=" FUNCTION: " + func_whitespace("mubea_update_demand_overview") + "{}", logger=logger.error)
def mubea_update_demand_overview(file_path=False):
    from analysis.models import MubeaDemand, MubeaIdent

    def extract_date(lst, last_index):
        date_indices = []
        date_pattern = r'\d{2}\.\d{2}\.\d{4}'
        for idx, item in enumerate(lst):
            match = re.search(date_pattern, item)
            if match and 'Loaded' not in item:
                if idx != last_index:
                    date_indices.append(item)

        return date_indices
    
    start_day = datetime.datetime.now() - datetime.timedelta(days=7)
    end_day = datetime.datetime.now() + datetime.timedelta(days=7)

    if file_path:
        import warnings

        warnings.filterwarnings("ignore", message="Cannot parse header or footer so it will be ignored", category=UserWarning)

        column_names = pd.read_excel(
                file_path,
                sheet_name='Quantity',
                header=6,
            ).columns
            
        columns_list = column_names.str.strip().to_list()

        selected_columns = [
            'Part No.',
        ] + extract_date(columns_list, len(column_names) - 1)

        raw_data = pd.read_excel(
            file_path,
            sheet_name='Quantity',
            header=6,
            usecols=selected_columns
        )


        days = selected_columns[1:]
        for day in days:
            for index, row in raw_data.iterrows():
                if row['Part No.'] == 'Total' or row['Part No.'] == 'Celkem':
                    break
                if pd.isna(row['Part No.']):
                    continue

                ident = int(row['Part No.'])
                value = row[day]
                if not pd.isna(value):
                    try:
                        quan = int(value)
                        parsed_date = datetime.datetime.strptime(day, "%d.%m.%Y")
                        date_str = parsed_date.strftime("%Y-%m-%d")
                        ident, created = MubeaIdent.objects.get_or_create(number=ident)
                        obj, created = MubeaDemand.objects.update_or_create(
                            ident=ident,
                            date=date_str,
                            defaults={
                                'quantity': quan,
                            }
                        )
                        
                        if created:
                            orderLogger.info(f"SD:{start_day} ED:{end_day}")
                            orderLogger.info(f"CREATED I:{ident.number, ident.description} Q:{quan} DL:{date_str}")

                    except ValueError:
                        continue

    else:
        df = pd.read_excel(settings.OVERVIEW_PATH, sheet_name='overview')

        date_cols = [col for col in df.columns[1:] if start_day <= col <= end_day]
        date_cols = [df.columns[0]] + date_cols

        result_df = df[date_cols]

        for date in result_df.columns[1:]:
            for index, value in zip(result_df['index'],result_df[date]):
                if not pd.isna(value):
                    date_str = date.strftime('%Y-%m-%d')

                    ident, created = MubeaIdent.objects.get_or_create(number=index)
                    MubeaDemand.objects.update_or_create(
                        ident=ident,
                        date=date_str,
                        defaults={
                            'quantity': value,
                        }
                    )


@Timer(text=" FUNCTION: " + func_whitespace("mubea_update_stock") + "{:0.4f} seconds", logger=logger.info)
@Error(text=" FUNCTION: " + func_whitespace("mubea_update_stock") + "{}", logger=logger.error)
def mubea_update_stock():
    start_day = datetime.datetime.now()
    from analysis.models import MubeaStock, MubeaIdent
    
    df = pd.read_excel(settings.RAW_MATERIAL_PATH,
                        sheet_name=f'aktualny raw',
                        header=9,
                        usecols=['delivery date ', 'Gotowe', 'stan', 'data produkcji '],
                        )

    df = df[(df.apply(lambda row: isinstance(row['delivery date '], datetime.datetime), axis=1))]
    df = df[(df.apply(lambda row: row['delivery date '] <= start_day, axis=1))]

    df['data produkcji '] = pd.to_datetime(df['data produkcji '], errors='coerce')
    df = df[(df['data produkcji '].isna()) | (df['data produkcji '] >= start_day)]

    df = df.groupby(['Gotowe'])['stan'].sum().reset_index()
    
    for index, row in df.iterrows():
        ident_value = row['Gotowe']
        quantity = row['stan']
        date = start_day.strftime("%Y-%m-%d")
        ident, created = MubeaIdent.objects.get_or_create(number=ident_value)
        MubeaStock.objects.update_or_create(
            ident=ident,
            date=date,
            defaults={
                'quantity': quantity,
            }
        )


@Timer(text=" FUNCTION: " + func_whitespace("mubea_update_production") + "{:0.4f} seconds", logger=logger.info)
@Error(text=" FUNCTION: " + func_whitespace("mubea_update_production") + "{}", logger=logger.error)
def mubea_update_production():
    from analysis.models import MubeaProduction, MubeaIdent

    start_day = datetime.datetime.now() - datetime.timedelta(days=7)
    end_day = datetime.datetime.now() + datetime.timedelta(days=7)

    df = pd.read_excel(settings.PRODUCTION_PATH,
                        sheet_name=f'Production ',
                        usecols=['Ident.', 'Teilart', 'Data', 'Produktion linie I', 'Produktion linie II', 'Produktion linie VII CHROM III']
                        )

    if not pd.api.types.is_datetime64_any_dtype(df['Data']):
        df['Data'] = pd.to_datetime(df['Data'])
        
    df = df.loc[(df['Data'] >= start_day) &
                (df['Data'] <= end_day)]

    
    df[['Produktion linie I', 'Produktion linie II', 'Produktion linie VII CHROM III']] = df[['Produktion linie I', 'Produktion linie II', 'Produktion linie VII CHROM III']].fillna(0)

    columns_to_convert = ['Produktion linie I', 'Produktion linie II', 'Produktion linie VII CHROM III']

    for column in columns_to_convert:
        df[column] = pd.to_numeric(df[column], errors='coerce')
    
    df[columns_to_convert] = df[columns_to_convert].astype('Int64')

    df['Production Total'] = df['Produktion linie I'] + df['Produktion linie II'] + df['Produktion linie VII CHROM III']
    
    df = df.groupby(['Ident.', 'Teilart', 'Data'])[['Produktion linie I', 'Produktion linie II', 'Produktion linie VII CHROM III']].sum().reset_index()

    for index, row in df.iterrows():
        ident_value = row['Ident.']
        l1_quantity = row['Produktion linie I']
        l2_quantity = row['Produktion linie II']
        l7_quantity = row['Produktion linie VII CHROM III']
        date = row['Data']
        ident, created = MubeaIdent.objects.get_or_create(number=ident_value)
        MubeaProduction.objects.update_or_create(
            ident=ident,
            date=date,
            defaults={
                'l1_quantity': l1_quantity,
                'l2_quantity': l2_quantity,
                'l7_quantity': l7_quantity,
            }
        )


@Timer(text=" FUNCTION: " + func_whitespace("mubea_update_shipments") + "{:0.4f} seconds", logger=logger.info)
@Error(text=" FUNCTION: " + func_whitespace("mubea_update_shipments") + "{}", logger=logger.error)
def mubea_update_shipments():
    from analysis.models import MubeaShipment, MubeaIdent

    start_day = datetime.datetime.now() - datetime.timedelta(days=7)
    end_day = datetime.datetime.now() + datetime.timedelta(days=7)

    df = pd.read_excel(settings.SHIPMENT_PATH,
                    sheet_name=f'Specyfikacja',
                    header=3,
                    )

    df = df[df.apply(lambda row: isinstance(row['Datum'], datetime.datetime), axis=1)]

    df = df.loc[(df['Datum'] >= start_day) &
                (df['Datum'] <= end_day)]
    df.dropna(subset=['Teile-Nummer', 'Anzahl der Teile Szt'], inplace=True)

    df['Teile-Nummer'] = df['Teile-Nummer'].apply(convert_to_int_or_zero)
    df = df[df['Teile-Nummer'] != 0]

    for index, row in df.iterrows():
        try:
            orderId = row['nr karty']
            spec = row['Spezifikation']
            delivery_note = row['Mubea DN']
            charge = row['Charge  ']
            weight = row['Gewicht  kg']
            packaging = row['Verpackung']
            ident_value = row['Teile-Nummer']
            quantity = row['Anzahl der Teile Szt']
            date = row['Datum']

            if packaging.lower() == 'gb':
                packaging = "026"

            elif packaging.lower() == 'karton':
                packaging = "037"

            with transaction.atomic():
                ident, created = MubeaIdent.objects.get_or_create(number=ident_value)
                MubeaShipment.objects.update_or_create(
                    row_no=index,
                    defaults={
                        'ident': ident,
                        'date': date,
                        'orderId': orderId,
                        'spec': spec,
                        'delivery_note': delivery_note,
                        'charge': charge,
                        'weight': weight,
                        'packaging': packaging,
                        'quantity': quantity,
                    }
                )
                latest_records = (Barcodes.objects
                                .filter(code=orderId, used=False)
                                .values('line')
                                .annotate(latest_id=Max('id')))

                for record in latest_records:
                    latest_barcode = Barcodes.objects.get(id=record['latest_id'])
                    latest_barcode.used = True
                    latest_barcode.save()
                    logger.info(f'[{datetime.datetime.now()}] Barcode USED TRUE {latest_barcode.timestamp} {latest_barcode.code} {latest_barcode.line}')

        except Exception as e:
            logger.error(f'[{datetime.datetime.now()}] mubea_update_shipments error: {e}; code: {orderId}, ident_value: {ident_value}, quantity: {quantity}, date: {date}')
            continue


@Timer(text=" FUNCTION: " + func_whitespace("mubea_update_ident_legend") + "{:0.4f} seconds", logger=logger.info)
@Error(text=" FUNCTION: " + func_whitespace("mubea_update_ident_legend") + "{}", logger=logger.error)
def mubea_update_ident_legend():
    from analysis.models import MubeaIdent

    df = pd.read_excel(settings.PRODUCTION_PATH,
                    sheet_name=f'Legenda NEW formula',
                    usecols=['Mubea\nPart Number ', 'Teilart', 'Part \nDescription 1', 'Fläche dm2/Teil','Linia', 'zawieszka', 'Ile w GB', 'cena [EUR]', 'waga [kg]', 'klient']
                    )

    for index, row in df.iterrows():
        number = row['Mubea\nPart Number ']
        if pd.isna(number):
            continue
        
        type = row['Teilart']
        description = row['Part \nDescription 1']
        area = row['Fläche dm2/Teil'] if not pd.isna(row['Fläche dm2/Teil']) else 0
        line = row['Linia'] if not pd.isna(row['Linia']) else 0
        hanger = row['zawieszka'] if not pd.isna(row['zawieszka']) else 0
        capacity = row['Ile w GB'] if not pd.isna(row['Ile w GB']) else 0
        price = row['cena [EUR]'] if not pd.isna(row['cena [EUR]']) else 0
        weight = row['waga [kg]'] if not pd.isna(row['waga [kg]']) else 0
        client = row['klient'] if not pd.isna(row['klient']) else ''

        MubeaIdent.objects.update_or_create(
            number=number,
            defaults={
                'type': type,
                'description': description,
                'area_dm': area,
                'line': line,
                'hanger': hanger,
                'capacity': capacity,
                'price_eur': price,
                'weight_kg': weight,
                'client': client
            }
        )


@Timer(text=" FUNCTION: " + func_whitespace("mubea_update_orders") + "{:0.4f} seconds", logger=logger.info)
@Error(text=" FUNCTION: " + func_whitespace("mubea_update_orders") + "{}", logger=logger.error)
def mubea_update_orders():
    from analysis.models import MubeaOrders, MubeaIdent
    
    df = pd.read_excel(settings.RAW_MATERIAL_PATH,
                        sheet_name=f'aktualny raw',
                        header=9,
                        usecols=['kod GTM', 'delivery date ','Delivery Note', 'Charge', 'Surowe',
                                  'Gotowe', 'Quantity', 'data produkcji ', 'data wysyłki'],
                        skiprows=list(range(10, MubeaOrders.objects.count()))
                        )

    df = df[(df.apply(lambda row: isinstance(row['delivery date '], datetime.datetime), axis=1))]


    
    for index, row in df.iterrows():
        production_date = row['data produkcji '] if not pd.isna(row['data produkcji ']) else None
        delivery_note = row['Delivery Note']
        shipment_date = row['data wysyłki'] if not pd.isna(row['data wysyłki']) else None
        ident_value = row['Gotowe']
        raw_ident_name = row['Surowe']

        try:
            raw_ident_name = int(raw_ident_name)
        
        except:
            raw_ident_name = 0
        
        try:
            delivery_note = int(delivery_note)
        except:
            pass

        if pd.isna(ident_value):
            break
        ident, created = MubeaIdent.objects.get_or_create(number=ident_value)

        try:
            MubeaOrders.objects.update_or_create(
                    orderId=row['kod GTM'],
                    defaults={
                        'dateDelivered': row['delivery date '],
                        'delivery_note': delivery_note,
                        'charge': row['Charge'],
                        'raw_ident_name': raw_ident_name,
                        'ident': ident,
                        'quantity': row['Quantity'],
                        'production_date': production_date,
                        'shipment_date': shipment_date,
                }
            )
        except ValidationError as val:
            print(val)
            continue
        except TypeError as t:
            print(t)
            continue
        except ValueError as v:
            print(v)
            continue


def mubea_update_all():
    mubea_update_ident_legend()
    #mubea_update_demand_overview()
    mubea_update_production()
    mubea_update_shipments()
    mubea_update_orders()


@Timer(text=" FUNCTION: " + func_whitespace("bruss_update_stock") + "{:0.4f} seconds", logger=logger.info)
@Error(text=" FUNCTION: " + func_whitespace("bruss_update_stock") + "{}", logger=logger.error)
def bruss_update_stock():
    from analysis.models import BrussIdent, BrussStock

    file_path = get_latest_file(settings.DESKTOP_PATH, 'Towary', '.csv')
    try:
        df = pd.read_csv(file_path,
                    delimiter='\t',
                    encoding='utf-16',
                    usecols=['Kod', 'Workers.StanMagazynu.StanMagazynu', 'Dostawca.Nazwa']
                    )
        supplier = 'Dostawca.Nazwa'
        warehouse = 'Workers.StanMagazynu.StanMagazynu'
    except:
        df = pd.read_csv(file_path,
                    delimiter='\t',
                    encoding='utf-16',
                    usecols=['Kod', 'Stan magazynu', 'Nazwa']
                    )
        supplier = 'Nazwa'
        warehouse = 'Stan magazynu'
        
    suppliers = ['Troger S.A', 'Spill & Oehmig', 'Kolle Gmbh', 'Estampaciones FUERTE S.A.U.', 'CIE EGANA', 'Werner Schmid GmbH']

    df = df[df[supplier].isin(suppliers)]

    for index, row in df.iterrows():
        name = row['Kod']
        quantity = row[warehouse]
        if '/' in name:
            splt = name.split('/')
            try:
                name_0 = int(splt[0])
            except:
                name_0 = 0
            
            try:
                name_1 = int(splt[1])
            except:
                name_1 = 0
            
        else:
            name_0 = name

        try:
            ident = BrussIdent.objects.get(number=name_0)
        except BrussIdent.DoesNotExist:
            try:
                ident= BrussIdent.objects.get(number=name_1)
            except BrussIdent.DoesNotExist:
                continue
        
        BrussStock.objects.update_or_create(
            ident=ident,
            defaults={
                'quantity': quantity,
            }
        )


@Timer(text=" FUNCTION: " + func_whitespace("bruss_update_orders") + "{:0.4f} seconds", logger=logger.info)
@Error(text=" FUNCTION: " + func_whitespace("bruss_update_orders") + "{}", logger=logger.error)
def bruss_update_orders():
    from analysis.models import BrussOrders, BrussIdent

    start_day = datetime.datetime.now() - datetime.timedelta(days=7)
    end_day = datetime.datetime.now() + datetime.timedelta(days=7)

    df = pd.read_excel(settings.BRUSS_ORDERS_PATH,
                    sheet_name=f'Zamówienia',
                    )

    df = df.loc[(df['confirmation date'] >= start_day) &
                    (df['confirmation date'] <= end_day)]
    df['Qty'] = pd.to_numeric(df['Qty'], errors='coerce')  # Convert to numeric, converting non-numeric to NaN
    df.dropna(subset=['Qty'], inplace=True)  # Drop rows with NaN values in the 'Qty' column
    df['Qty'] = df['Qty'].astype(int)  # Convert to integer

    for index, row in df.iterrows():
        ident = row['article number']
        quantity = row['Qty']
        order_id = str(row['order number'])
        deadline = row['Date of order fulfillment']
        confirmation = row['confirmation date']
        ls = row['LS number']
        shipment = row[' order exit date']
        production = row['production date']

        if len(order_id) != 6 and "B" in order_id:
            continue
        
        if len(order_id) == 5 and order_id.isdigit():
            continue

        try:
            if isinstance(row['card…'], int):
                kan_1 = str(int(row['card…']))
            else:
                kan_1 = str(row['card…'])
        except:
            kan_1 = ''

        try:
            if isinstance(row['from...'], int):
                kan_2 = str(int(row['from...']))
            else:
                kan_2 = str(row['from...'])
        except:
            kan_2 = ''

        kanban = kan_1 + '/' + kan_2

        try:
            deadline_str = deadline.strftime('%Y-%m-%d')
        except:
            deadline_str = None

        try:
            shipment_str = shipment.strftime('%Y-%m-%d')
        except:
            shipment_str = None

        try:
            production_str = production.strftime('%Y-%m-%d')
        except:
            production_str = None

        try:
            confirmation_str = confirmation.strftime('%Y-%m-%d')
        except:
            confirmation_str = None


        with transaction.atomic():
            ident, created = BrussIdent.objects.get_or_create(number=ident)
            obj, created = BrussOrders.objects.update_or_create(
                orderId=order_id,
                defaults={
                    'ident': ident,
                    'quantity': quantity,
                    'dateDeadline': deadline_str,
                    'dateProduction': production_str,
                    'dateShipment': shipment_str,
                    'dateConfirmed': confirmation_str,
                    'delivery_note': '' if pd.isna(ls) else ls,
                    'kanbanCard': kanban
                }
            )
            if created:
                orderLogger.info(f"SD:{start_day} ED:{end_day}")
                orderLogger.info(f"CREATED ID:{order_id} I:{ident} Q:{quantity} DN:{'' if pd.isna(ls) else ls} KCRD:{kanban}")

            if shipment_str:
                if len(order_id) == 6 and order_id.isdigit():
                    mod_order_id = f'KES{order_id}'
                elif len(order_id) == 6 and order_id[0] == 'B':
                    mod_order_id = f'000{order_id}'
                elif order_id[0] == 'G':
                    mod_order_id = f'MRG{order_id}'
                else:
                    continue
                
                latest_records = (Barcodes.objects
                                .filter(code=mod_order_id, used=False)
                                .values('line')
                                .annotate(latest_id=Max('id')))

                for record in latest_records:
                    latest_barcode = Barcodes.objects.get(id=record['latest_id'])
                    latest_barcode.used = True
                    latest_barcode.save()
                    logger.info(f'[{datetime.datetime.now()}] Barcode USED TRUE {latest_barcode.timestamp} {latest_barcode.code} {latest_barcode.line}')


@Timer(text=" FUNCTION: " + func_whitespace("bruss_update_legend ") + "{:0.4f} seconds", logger=logger.info)
@Error(text=" FUNCTION: " + func_whitespace("bruss_update_legend ") + "{}", logger=logger.error)
def bruss_update_legend():
    from analysis.models import BrussIdent

    df = pd.read_excel(settings.BRUSS_ORDERS_PATH,
                    sheet_name=f'Legenda',
                    usecols=['nr artykułu po fosforanowaniu', 'dostawca ', 'nazwa artykułu ', 'Ilość na zawieszkę [KTL]', 'Ilość na zawieszkę [Ph]', 'wózek [Ph]']
                    )

    df.dropna(subset=['nr artykułu po fosforanowaniu'], inplace=True)

    for index, row in df.iterrows():
        name = row['nr artykułu po fosforanowaniu']
        supplier = row['dostawca ']
        description = row['nazwa artykułu ']
        n_exits_ph = row['Ilość na zawieszkę [Ph]'] if not pd.isna(row['Ilość na zawieszkę [Ph]']) else 1000
        n_exits_ktl = row['Ilość na zawieszkę [KTL]'] if not pd.isna(row['Ilość na zawieszkę [KTL]']) else 1000
        hanger_ph = row['wózek [Ph]'] if not pd.isna(row['wózek [Ph]']) else 'zawieszka'

        BrussIdent.objects.update_or_create(
            number=name,
            defaults={
                'supplier': supplier,
                'description': description,
                'n_exits_ph': n_exits_ph,
                'n_exits_ktl': n_exits_ktl,
                'n_exits_ktl': n_exits_ktl,
                'hanger_ph': hanger_ph
            }
        )


def bruss_update_all():
    bruss_update_legend()
    bruss_update_orders()
    bruss_update_stock()


@Timer(text=" FUNCTION: " + func_whitespace("faurcecia_update_ident_legend") + "{:0.4f} seconds", logger=logger.info)
@Error(text=" FUNCTION: " + func_whitespace("faurcecia_update_ident_legend") + "{}", logger=logger.error)
def faurcecia_update_ident_legend():
    from analysis.models import FaureciaIdent

    df = pd.read_excel(settings.FAURECIA_PATH,
                    sheet_name=f'LEGENDA',
                    usecols=['Plant number\nafter chromage', 'nazwa materiału', 'Linia', 'Zawieszka', 'Pakowanie', 'Typ']
                    )


    for index, row in df.iterrows():
        number = int(row['Plant number\nafter chromage'])
        description = row['nazwa materiału']
        line = row['Linia'] if not pd.isna(row['Linia']) else 0
        hanger = row['Zawieszka'] if not pd.isna(row['Zawieszka']) else 0
        capacity = row['Pakowanie'] if not pd.isna(row['Pakowanie']) else 0
        typ = row['Typ'] if not pd.isna(row['Typ']) else ''

        FaureciaIdent.objects.update_or_create(
            number=number,
            defaults={
                'description': description,
                'type': typ,
                'line': line,
                'hanger': hanger,
                'capacity': capacity
            }
        )


@Timer(text=" FUNCTION: " + func_whitespace("faurcecia_update_demand_overview") + "{:0.4f} seconds", logger=logger.info)
@Error(text=" FUNCTION: " + func_whitespace("faurcecia_update_demand_overview") + "{}", logger=logger.error)
def faurecia_update_demand_overview():
    from analysis.models import FaureciaDemand, FaureciaIdent
    df = pd.read_excel(settings.FAURECIA_PATH,
                    sheet_name=f'MANIFESTY',
                    usecols=['nr identu', 'ilość', 'z']
                    )
    date_formats = [
            "%Y-%m-%d",  # "2023-05-21"
            "%Y-%m-%d %H:%M:%S",  # "2023-05-21 00:00:00"
            "%d/%m/%Y",  # "21/05/2023"
            "%d.%m.%Y",  # "21.05.2023"
            "%m/%d/%Y",  # "05/21/2023"
            "%d-%m-%Y",  # "21-05-2023"
            "%m-%d-%Y",  # "05-21-2023"
            "%B %d, %Y", # "May 21, 2023"
            "%b %d, %Y", # "May 21, 2023"
            "%d %B %Y",  # "21 May 2023"
            "%d %b %Y",  # "21 May 2023"
        ]
    
    for index, row in df.iterrows():
        if pd.isna(row['nr identu']):
            break
        
        ident_value = int(row['nr identu'])
        quantity = row['ilość']
        date = row['z']

        for date_format in date_formats:
            try:
                date_obj = datetime.datetime.strptime(date, date_format)
            except:
                continue

        ident, created = FaureciaIdent.objects.get_or_create(number=ident_value)
        FaureciaDemand.objects.update_or_create(
            ident=ident,
            date=date_obj,
            defaults={
                'quantity': quantity,
            }
        )


@Timer(text=" FUNCTION: " + func_whitespace("faurecia_update_production") + "{:0.4f} seconds", logger=logger.info)
@Error(text=" FUNCTION: " + func_whitespace("faurecia_update_production") + "{}", logger=logger.error)
def faurecia_update_production():
    from analysis.models import FaureciaProduction, FaureciaIdent

    start_day = datetime.datetime.now() - datetime.timedelta(days=7)
    end_day = datetime.datetime.now() + datetime.timedelta(days=7)

    df = pd.read_excel(settings.PRODUCTION_PATH,
                        sheet_name=f'FAURECIA ',
                        usecols=['Ident.', 'Data', 'pcs']
                        )

    if not pd.api.types.is_datetime64_any_dtype(df['Data']):
        df['Data'] = pd.to_datetime(df['Data'])

    df = df.loc[(df['Data'] >= start_day) & (df['Data'] <= end_day)]

    df['pcs'] = df['pcs'].fillna(0)


    df['pcs'] = pd.to_numeric(df['pcs'], errors='coerce')

    df['pcs'] = df['pcs'].astype('Int64')

    df = df.groupby(['Ident.', 'Data'])['pcs'].sum().reset_index()

    for index, row in df.iterrows():
        ident_value = row['Ident.']
        quantity = row['pcs']
        date = row['Data']

        ident, created = FaureciaIdent.objects.get_or_create(number=ident_value)
        FaureciaProduction.objects.update_or_create(
            ident=ident,
            date=date,
            defaults={
                'quantity': quantity,
            }
        )


@Timer(text=" FUNCTION: " + func_whitespace("faurecia_update_shipments") + "{:0.4f} seconds", logger=logger.info)
@Error(text=" FUNCTION: " + func_whitespace("faurecia_update_shipments") + "{}", logger=logger.error)
def faurecia_update_shipments():
    from analysis.models import FaureciaShipment, FaureciaIdent

    start_day = datetime.datetime.now() - datetime.timedelta(days=7)
    end_day = datetime.datetime.now() + datetime.timedelta(days=7)

    df = pd.read_excel(settings.FAURECIA_PATH,
                    sheet_name=f'WYSYŁKA',
                    header=3,
                    usecols=['Shiping Date', 'article no.', 'pcs']
                    )
    df = df[df.apply(lambda row: isinstance(row['Shiping Date'], datetime.datetime), axis=1)]

    df = df.loc[(df['Shiping Date'] >= start_day) &
                (df['Shiping Date'] <= end_day)]
    df.dropna(subset=['article no.', 'pcs'], inplace=True)

    df['article no.'] = df['article no.'].apply(convert_to_int_or_zero)
    df = df[df['article no.'] != 0]

    df = df.groupby(['article no.', 'Shiping Date'])['pcs'].sum().reset_index()


    for index, row in df.iterrows():
        ident_value = row['article no.']
        quantity = row['pcs']
        date = row['Shiping Date']

        ident, created = FaureciaIdent.objects.get_or_create(number=ident_value)
        FaureciaShipment.objects.update_or_create(
            ident=ident,
            date=date,
            defaults={
                'quantity': quantity,
            }
        )


@Timer(text=" FUNCTION: " + func_whitespace("faurecia_update_orders") + "{:0.4f} seconds", logger=logger.info)
@Error(text=" FUNCTION: " + func_whitespace("faurecia_update_orders") + "{}", logger=logger.error)
def faurecia_update_orders():
    from analysis.models import FaureciaOrders, FaureciaIdent

    start_day = datetime.datetime.now() - datetime.timedelta(days=7)
    end_day = datetime.datetime.now() + datetime.timedelta(days=7)

    df = pd.read_excel(settings.FAURECIA_PATH,
                    sheet_name=f'RAW',
                    header=3,
                    )

    df = df.loc[(df['data'] >= start_day) &
                    (df['data'] <= end_day)]

    df['ilość '] = pd.to_numeric(df['ilość '], errors='coerce').astype(int)
    for index, row in df.iterrows():
        ident = int(row['nr identu'])
        quantity = int(row['ilość '])
        order_id = row['Kod']
        date_delivered = row['data']
        delivery_note = row['Nr referencyjny ']
        client = row['Info Dostawca']

        try:
            date_delivered_str = date_delivered.strftime('%Y-%m-%d')
        except:
            date_delivered_str = None
        
        if client.lower() not in ['teknia', 'faurecia']:
            client = None

        ident, created = FaureciaIdent.objects.get_or_create(number=ident)
        FaureciaOrders.objects.update_or_create(
            ident=ident,
            orderId=order_id,
            defaults={
                'quantity': quantity,
                'dateDelivered': date_delivered_str,
                'delivery_note': delivery_note,
                'client': client,
            }
        )


@Timer(text=" FUNCTION: " + func_whitespace("siegenia_update_legend ") + "{:0.4f} seconds", logger=logger.info)
@Error(text=" FUNCTION: " + func_whitespace("siegenia_update_legend ") + "{}", logger=logger.error)
def siegenia_update_legend():
    from analysis.models import SiegeniaIdent

    df = pd.read_excel(settings.SIEGENIA_ORDERS_PATH,
                    sheet_name=f'DANE',
                    usecols=['Materiał', 'Opis materiału', 'Waga brutto', 'Długość', 'Szerokość', 'Wysokość', 'Cena po podwyżce 01.06.2024 / €']
                    )

    df.dropna(subset=['Materiał'], inplace=True)

    for index, row in df.iterrows():
        name = row['Materiał']
        description = row['Opis materiału']
        weight = row['Waga brutto']
        length = row['Długość']
        height = row['Wysokość']
        width = row['Szerokość']
        price  = row['Cena po podwyżce 01.06.2024 / €']


        SiegeniaIdent.objects.update_or_create(
            number=name,
            defaults={
                'description': description,
                'price_euro': price,
                'weight_kg': weight,
                'width_mm': width,
                'length_mm': length,
                'height_mm': height,
            }
        )


@Timer(text=" FUNCTION: " + func_whitespace("siegenia_update_orders") + "{:0.4f} seconds", logger=logger.info)
@Error(text=" FUNCTION: " + func_whitespace("siegenia_update_orders") + "{}", logger=logger.error)
def siegenia_update_orders():
    from analysis.models import SiegeniaOrders, SiegeniaIdent

    start_day = datetime.datetime.now() - datetime.timedelta(days=7)
    end_day = datetime.datetime.now() + datetime.timedelta(days=7)

    df = pd.read_excel(settings.SIEGENIA_ORDERS_PATH,
                    sheet_name=f'RAW',
                    header=4,
                    )

    df = df.loc[(df['Data dostawy'] >= start_day) &
                    (df['Data dostawy'] <= end_day)]

    df['Ilość Quantity'] = pd.to_numeric(df['Ilość Quantity'], errors='coerce').astype(int)

    for index, row in df.iterrows():
        ident = row['Materiał']
        quantity = int(row['Ilość Quantity'])
        order_id = 'SGN' + '0' * (6 - len(str(int(row['nr wiersza'])))) + str(int(row['nr wiersza']))
        date_delivered = row['Data dostawy']
        delivery_note = row['Zamówienie']

        ident, created = SiegeniaIdent.objects.get_or_create(number=ident)
        SiegeniaOrders.objects.update_or_create(
            ident=ident,
            orderId=order_id,
            defaults={
                'dateDelivered': date_delivered,
                'quantity': quantity,
                'delivery_note': delivery_note
            }
        )


@Timer(text=" FUNCTION: " + func_whitespace("metal_martin_update_legend ") + "{:0.4f} seconds", logger=logger.info)
@Error(text=" FUNCTION: " + func_whitespace("metal_martin_update_legend ") + "{}", logger=logger.error)
def metal_martin_update_legend():
    from analysis.models import MetalMartinIdent

    df = pd.read_excel(settings.METAL_MARTIN_ORDERS_PATH,
                    sheet_name=f'LEGENDA',
                    usecols=['Part No.', 'Nazwa materiału', 'Opakowanie wyjściowe', 'Ilość na zawieszkę [KTL]', 'Ilość na zawieszkę [Ph]']
                    )

    df.dropna(subset=['Part No.'], inplace=True)

    for index, row in df.iterrows():
        name = row['Part No.']
        description = row['Nazwa materiału']
        packaging = row['Opakowanie wyjściowe']
        n_exits_ph = row['Ilość na zawieszkę [Ph]'] if not pd.isna(row['Ilość na zawieszkę [Ph]']) else 100
        n_exits_ktl = row['Ilość na zawieszkę [KTL]'] if not pd.isna(row['Ilość na zawieszkę [KTL]']) else 100
        MetalMartinIdent.objects.update_or_create(
            number=name,
            defaults={
                'description': description,
                'packaging': packaging,
                'n_exits_ktl': n_exits_ktl,
                'n_exits_ph': n_exits_ph
            }
        )   


@Timer(text=" FUNCTION: " + func_whitespace("metal_martin_update_orders") + "{:0.4f} seconds", logger=logger.info)
@Error(text=" FUNCTION: " + func_whitespace("metal_martin_update_orders") + "{}", logger=logger.error)
def metal_martin_update_orders():
    from analysis.models import MetalMartinOrders, MetalMartinIdent

    start_day = datetime.datetime.now() - datetime.timedelta(days=7)
    end_day = datetime.datetime.now() + datetime.timedelta(days=7)
    
    df = pd.read_excel(settings.METAL_MARTIN_ORDERS_PATH,
                    sheet_name=f'RAW',
                    )

    df = df.loc[(df['Data dostawy'] >= start_day) &
                    (df['Data dostawy'] <= end_day)]
    
    for index, row in df.iterrows():
        ident = row['Materiał']
        try:
            quantity = int(row['Ilość '])
        except:
            continue

        order_id = row['Numer karty']
        date_delivered = row['Data dostawy']
        date_deadline = row['Na kiedy']
        date_shipment = row['Data wysyłki']
        delivery_note = row['Nr. LS\'a']

        try:
            date_delivered_str = date_delivered.strftime('%Y-%m-%d')
        except:
            date_delivered_str = None

        try:
            date_deadline = date_deadline.strftime('%Y-%m-%d')
        except:
            date_deadline = date_delivered_str

        try:
            date_shipment = date_shipment.strftime('%Y-%m-%d')
        except:
            date_shipment = None


        with transaction.atomic():
            ident, created = MetalMartinIdent.objects.get_or_create(number=ident)
            MetalMartinOrders.objects.update_or_create(
                ident=ident,
                orderId=order_id,
                defaults={
                    'quantity': quantity,
                    'dateDelivered': date_delivered_str,
                    'dateDeadline': date_deadline,
                    'delivery_note': delivery_note
                }
            )
            if date_shipment:
                latest_records = (Barcodes.objects
                                .filter(code=order_id, used=False)
                                .values('line')
                                .annotate(latest_id=Max('id')))

                for record in latest_records:
                    latest_barcode = Barcodes.objects.get(id=record['latest_id'])
                    latest_barcode.used = True
                    latest_barcode.save()
                    logger.info(f'[{datetime.datetime.now()}] Barcode USED TRUE {latest_barcode.timestamp} {latest_barcode.code} {latest_barcode.line}')


@Timer(text=" FUNCTION: " + func_whitespace("hutchinson_update_legend ") + "{:0.4f} seconds", logger=logger.info)
@Error(text=" FUNCTION: " + func_whitespace("hutchinson_update_legend ") + "{}", logger=logger.error)
def hutchinson_update_legend():
    from analysis.models import HutchinsonIdent

    df = pd.read_excel(settings.HUTCHINSON_ORDERS_PATH,
                    sheet_name=f'Legenda',
                    usecols=['Nr artykułu', 'Kod Hutchinson', 'packaging', 'Ilość na zawieszkę [Ph]']
                    )

    df.dropna(subset=['Nr artykułu'], inplace=True)

    for index, row in df.iterrows():
        name = row['Nr artykułu']
        description = row['Kod Hutchinson']
        packaging = row['packaging']
        n_exits_ph = row['Ilość na zawieszkę [Ph]']

        try:
            if isinstance(row['Ilość na zawieszkę [Ph]'], int):
                n_exits_ph = int(row['Ilość na zawieszkę [Ph]'])
            else:
                n_exits_ph = 0
        except:
            n_exits_ph = 0

        HutchinsonIdent.objects.update_or_create(
            number=name,
            defaults={
                'description': description,
                'packaging': packaging,
                'n_exits_ph': n_exits_ph
            }
        )


@Timer(text=" FUNCTION: " + func_whitespace("hutchinson_update_orders") + "{:0.4f} seconds", logger=logger.info)
@Error(text=" FUNCTION: " + func_whitespace("hutchinson_update_orders") + "{}", logger=logger.error)
def hutchinson_update_orders():
    from analysis.models import HutchinsonOrders, HutchinsonIdent

    start_day = datetime.datetime.now() - datetime.timedelta(days=7)
    end_day = datetime.datetime.now() + datetime.timedelta(days=7)
    
    df = pd.read_excel(settings.HUTCHINSON_ORDERS_PATH,
                    sheet_name=f'Zamówienia',
                    )

    df = df.loc[(df['data dostawy GTM'] >= start_day) &
                    (df['data dostawy GTM'] <= end_day)]

    df['ilość'] = pd.to_numeric(df['ilość'], errors='coerce').astype(int)

    for index, row in df.iterrows():
        ident = row['nr artykułu']
        quantity = int(row['ilość'])
        order_id = row['Kod']
        date_delivered = row['data dostawy GTM']

        try:
            date_delivered_str = date_delivered.strftime('%Y-%m-%d')
        except:
            date_delivered_str = None


        ident, created = HutchinsonIdent.objects.get_or_create(number=ident)
        HutchinsonOrders.objects.update_or_create(
            ident=ident,
            orderId=order_id,
            defaults={
                'quantity': quantity,
                'dateDelivered': date_delivered_str
            }
        )


@Timer(text=" FUNCTION: " + func_whitespace("adient_update_ident_legend") + "{:0.4f} seconds", logger=logger.info)
@Error(text=" FUNCTION: " + func_whitespace("adient_update_ident_legend") + "{}", logger=logger.error)
def adient_update_ident_legend():
    from analysis.models import AdientIdent

    df = pd.read_excel(settings.ADIENT_ORDERS_PATH,
                    sheet_name=f'Legenda',
                    usecols=['Ident surowy', 'Nazwa']
                    )

    for index, row in df.iterrows():
        number = row['Ident surowy']
        if pd.isna(number):
            continue
        
        description = row['Nazwa']

        AdientIdent.objects.update_or_create(
            number=number,
            defaults={
                'description': description,
            }
        )


@Timer(text=" FUNCTION: " + func_whitespace("adient_update_orders") + "{:0.4f} seconds", logger=logger.info)
@Error(text=" FUNCTION: " + func_whitespace("adient_update_orders") + "{}", logger=logger.error)
def adient_update_orders():
    from analysis.models import AdientOrders, AdientIdent
    
    df = pd.read_excel(settings.ADIENT_ORDERS_PATH,
                        sheet_name=f'Adient Dostawy',
                        header=2,
                        usecols=['Kod GTM', 'Data dostawy','Numer dokumentu', 'Ident', 'Ilość',
                                  'Data produkcji', 'Data Wysyłki'],
                        skiprows=list(range(3, AdientOrders.objects.count()))
                        )

    df = df[(df.apply(lambda row: isinstance(row['Data dostawy'], datetime.datetime), axis=1))]


    
    for index, row in df.iterrows():
        production_date = row['Data produkcji'] if not pd.isna(row['Data produkcji']) else None
        delivery_note = row['Numer dokumentu']
        shipment_date = row['Data Wysyłki'] if not pd.isna(row['Data Wysyłki']) else None
        ident_value = row['Ident']

        if pd.isna(ident_value):
            break

        ident, created = AdientIdent.objects.get_or_create(number=ident_value)

        try:
            AdientOrders.objects.update_or_create(
                    orderId=row['Kod GTM'],
                    defaults={
                        'dateDelivered': row['Data dostawy'],
                        'delivery_note': delivery_note,
                        'ident': ident,
                        'quantity': row['Ilość'],
                        'production_date': production_date,
                        'shipment_date': shipment_date,
                }
            )
        except ValidationError as val:
            print(val)
            continue
        except TypeError as t:
            print(t)
            continue
        except ValueError as v:
            print(v)
            continue

