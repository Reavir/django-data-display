import datetime

def start_end_day_cw(calendar_week):
    if calendar_week < 1 or calendar_week > 53:
        raise ValueError("Calendar week must be between 1 and 53 inclusive.")
    
    current_year = datetime.datetime.now().year
    january_4th = datetime.date(current_year, 1, 4)
    start_of_year_week = january_4th - datetime.timedelta(days=january_4th.weekday())
    
    first_day_of_week = start_of_year_week + datetime.timedelta(weeks=(calendar_week - 1))
    last_day_of_week = first_day_of_week + datetime.timedelta(days=6)
    
    return (
        datetime.datetime.combine(first_day_of_week, datetime.time.min),
        datetime.datetime.combine(last_day_of_week, datetime.time.min)
    )


def number_separator(number):
    num_str = str(number)[::-1]
    result = ""

    for i in range(len(num_str)):
        result += num_str[i]
        if (i + 1) % 3 == 0 and i != len(num_str) - 1 and num_str[i+1] != '-':
            try:
                if num_str[i+1] != '-':
                    result += " "
            except:
                break

    return result[::-1]


def convert_to_int_or_zero(value):
    try:
        return int(value)
    except ValueError:
        return 0
    

def func_whitespace(input_str):
    longest = "faurcecia_update_demand_overview"

    return input_str + ' ' * (len(longest) + 9 - len(input_str) + 1)


def count_working_days(start_date, end_date):
    # Convert input to strings if they are not already strings
    if isinstance(start_date, (datetime.date, datetime.datetime)):
        start_date = start_date.strftime("%Y-%m-%d")
    if isinstance(end_date, (datetime.date, datetime.datetime)):
        end_date = end_date.strftime("%Y-%m-%d")

    # Convert input strings to datetime objects
    start_date = datetime.datetime.strptime(start_date, "%Y-%m-%d").date()
    end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d").date()

    # Calculate the number of working days between two dates
    working_days = 0
    current_date = start_date

    while current_date <= end_date:
        if current_date.weekday() < 5:  # Monday to Friday are considered working days
            working_days += 1
        current_date += datetime.timedelta(days=1)
    return working_days 


def convert_to_datetime(date_str, min_time):
    # Input string
    current_year = datetime.datetime.now().year

    # Mapping of Polish month names to their numerical representations
    month_names = {
        'sty': 1, 'lut': 2, 'mar': 3, 'kwi': 4,
        'maj': 5, 'cze': 6, 'lip': 7, 'sie': 8,
        'wrz': 9, 'paź': 10, 'lis': 11, 'gru': 12
    }

    # Split the input string
    day_name, day_number, month_name = date_str.split()

    # Convert month name to numerical representation
    month_number = month_names[month_name]

    # Create a datetime object
    if min_time:
        date_obj = datetime.date(current_year, month_number, int(day_number))
        date_obj = datetime.datetime.combine(date_obj, datetime.time.min)
    else:
        date_obj = datetime.date(current_year, month_number, int(day_number))
        date_obj = datetime.datetime.combine(date_obj, datetime.time.max)

    return date_obj

