import requests


url = "https://v06wnnc1-8000.euw.devtunnels.ms/tools/barcodes/api/post/"

data = {
    "barcodeData": "",
    "selectedOption": ""
}

response = requests.post(url, data=data)

print("Status Code:", response.status_code)
print("Response JSON:", response.json())
