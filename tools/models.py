from django.db import models
from django.utils import timezone

class PrasyLabel(models.Model):
    number = models.IntegerField()
    description = models.TextField()

class Hangers(models.Model):
    line1 = models.IntegerField()
    line2 = models.IntegerField()

class SavedPlan(models.Model):
    number_days = models.IntegerField()
    timestamp = models.DateTimeField()
    header = models.JSONField()
    rows_dict = models.JSONField()
    additional_data = models.JSONField()

    def __str__(self):
        return str(self.timestamp)
    
class Barcodes(models.Model):
    code = models.CharField(max_length=50)
    line = models.CharField(max_length=50)
    timestamp = models.DateTimeField()
    name = models.TextField(default='', null=True, blank=True)
    additional_info = models.TextField(null=True, blank=True)
    used = models.BooleanField(default=False)

class BrussExport(models.Model):
    sic_id = models.IntegerField()
    json_data = models.JSONField()
    timestamp = models.DateTimeField(default=timezone.now)

class SavedPlanP(models.Model):
    plan_json = models.JSONField()
    timestamp = models.DateTimeField()

    def __str__(self):
        return str(self.timestamp)
    
class SavedPlanKTL(models.Model):
    plan_json = models.JSONField()
    timestamp = models.DateTimeField()

    def __str__(self):
        return str(self.timestamp)