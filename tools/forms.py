# forms.py
import datetime
from django import forms
from utils.calculations import start_end_day_cw
from tools.models import Hangers, SavedPlan, BrussExport, SavedPlanP, SavedPlanKTL

class OrderForm(forms.Form):
    def __init__(self, *args, **kwargs):
        order_choices = kwargs.pop('order_choices')
        super(OrderForm, self).__init__(*args, **kwargs)

        for order in order_choices:
            self.fields[f'{order.number}_{order.description}'] = forms.IntegerField(
                label=order.description,
                required=False,
                )


class PlanForm(forms.Form):

    start_date = forms.DateField(widget=forms.DateInput(attrs={'type': 'date'}), label='Start Date')
    end_date = forms.DateField(widget=forms.DateInput(attrs={'type': 'date'}), label='End Date')
    deliveries = forms.BooleanField(label='Substract Deliveries', required=False)
    production = forms.BooleanField(label='Substract Production', required=False)

    prio_id = forms.CharField(widget=forms.TextInput(attrs={'class': 'select2-autocomplete'}),label='Priority', required=False)
    excl_id = forms.CharField(widget=forms.TextInput(attrs={'class': 'select2-autocomplete'}),label='Exclude', required=False)

    line1 = forms.IntegerField(label='Line 1', required=False)
    line2 = forms.IntegerField(label='Line 2', required=False)
  

    def __init__(self, *args, **kwargs):
        super(PlanForm, self).__init__(*args, **kwargs)
        date_start_str, date_end_str = start_end_day_cw(datetime.date.today().isocalendar()[1])
        self.fields['start_date'].initial = date_start_str
        self.fields['end_date'].initial = date_end_str
        self.fields['deliveries'].initial = False
        self.fields['production'].initial = False

        hangers_instance = Hangers.objects.latest('id')
        self.fields['line1'].initial = hangers_instance.line1
        self.fields['line2'].initial = hangers_instance.line2


class SavedPlanForm(forms.Form):
    selected_timestamp = forms.ModelChoiceField(
        queryset=SavedPlan.objects.all().order_by('-timestamp'),
        empty_label='Select Timestamp',
        widget=forms.Select(attrs={'class': 'form-control'})
    )


class BrussExportForm(forms.ModelForm):
    class Meta:
        model = BrussExport
        fields = ['sic_id']

    def __init__(self, *args, **kwargs):
        super(BrussExportForm, self).__init__(*args, **kwargs)
    
        latest_sic_ids = BrussExport.objects.values_list('id', 'sic_id', 'timestamp').distinct().order_by('-timestamp')
        choices = [(id, f'WZ 3 {"0" * (3 - len(str(sic_id))) + str(sic_id)}/{timestamp.strftime("%m/%Y")}') for id, sic_id, timestamp in latest_sic_ids]
        self.fields['sic_id'].widget = forms.Select(choices=choices, attrs={'readonly': 'readonly'})


class BrussExportFormAll(forms.ModelForm):
    class Meta:
        model = BrussExport
        fields = ['sic_id']

    def __init__(self, *args, filter_date=None, **kwargs):
        super(BrussExportFormAll, self).__init__(*args, **kwargs)

        if filter_date is None:
            filter_date = datetime.date.today() - datetime.timedelta(days=1)

        latest_sic_ids = BrussExport.objects.filter(timestamp__date=filter_date).values_list('id', 'sic_id', 'timestamp').distinct().order_by('-timestamp')
        choices = [("", "WZ 3 ---/--/--")]
        choices += [(id, f'WZ 3 {"0" * (3 - len(str(sic_id))) + str(sic_id)}/{timestamp.strftime("%m/%Y")}') for id, sic_id, timestamp in latest_sic_ids]
        self.fields['sic_id'].widget = forms.Select(choices=choices, attrs={'readonly': 'readonly'})
        

class DateInputForm(forms.Form):
    selected_date = forms.DateField(widget=forms.DateInput(attrs={'type': 'date'}))

    def __init__(self, *args, filter_date=None, **kwargs):
        super(DateInputForm, self).__init__(*args, **kwargs)

        if filter_date is None:
            filter_date = datetime.date.today() - datetime.timedelta(days=1)

        self.fields['selected_date'].initial = filter_date


class NewBrussBarcode(forms.Form):
    start_order_nr = forms.IntegerField(label='Kod Początkowy', required=False)
    order_id = forms.CharField(label='Order ID:', required=False)
    ident_number = forms.CharField(label='Article ID', required=False)
    ident_description = forms.CharField(label='Article Name', required=False)
    kanban = forms.CharField(label='Kanban', required=False)
    articleQuantity = forms.IntegerField(label='Quantity', required=False)
    packaging = forms.CharField(label='Packaging', required=False)
    ls_number = forms.CharField(label='LS number', required=False)
    
    # Date picker for Target Delivery Date
    dateDeadline = forms.DateTimeField(
        label='Target Delivery Date',
        required=False,
        widget=forms.widgets.DateInput(attrs={'type': 'date'})
    )

    # Date picker for Generation Date
    dateNow = forms.DateTimeField(
        label='Generation Date',
        required=False,
        widget=forms.widgets.DateTimeInput(attrs={'type': 'datetime-local'}),
        input_formats=['%Y-%m-%dT%H:%M:%S']
    )


class SavedPlanPForm(forms.Form):
    selected_timestamp = forms.ChoiceField(
        choices=[],  # Initially empty choices
        widget=forms.Select(attrs={'class': 'form-control'})
    )

    def __init__(self, *args, **kwargs):
        super(SavedPlanPForm, self).__init__(*args, **kwargs)
        
        # Get all SavedPlanP instances and order by timestamp
        saved_plans = SavedPlanP.objects.all().order_by('-timestamp')
        
        # Create choices list with custom value as the first option
        choices = [('', 'Zapisane plany:')]
        choices += [(saved_plan.id, str(saved_plan)) for saved_plan in saved_plans]

        self.fields['selected_timestamp'].choices = choices


class SavedPlanKTLForm(forms.Form):
    selected_timestamp = forms.ChoiceField(
        choices=[],  # Initially empty choices
        widget=forms.Select(attrs={'class': 'form-control'})
    )

    def __init__(self, *args, **kwargs):
        super(SavedPlanKTLForm, self).__init__(*args, **kwargs)
        
        # Get all SavedPlanKTL instances and order by timestamp
        saved_plans = SavedPlanKTL.objects.all().order_by('-timestamp')
        
        # Create choices list with custom value as the first option
        choices = [('', 'Zapisane plany:')]
        choices += [(saved_plan.id, str(saved_plan)) for saved_plan in saved_plans]

        self.fields['selected_timestamp'].choices = choices
    
class VDA4913DateInput(forms.Form):
    selected_date = forms.DateField(widget=forms.DateInput(attrs={'type': 'date'}))
