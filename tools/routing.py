from django.urls import path
from tools import consumers  # Import the consumer class

websocket_urlpatterns = [
    path('ws/scanned-codes/', consumers.BarcodeConsumer.as_asgi()),
    path('ws/plan_ktl_updates/', consumers.PlanKTLConsumer.as_asgi()),
    path('ws/plan_p_updates/', consumers.PlanPConsumer.as_asgi()),
    path("ws/clients/<str:page_name>/", consumers.ConnectedClientConsumer.as_asgi()),
]
