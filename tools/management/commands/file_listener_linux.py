from django.core.management.base import BaseCommand

import datetime
import logging
import re
import time
import os

from utils.update_data import *
from mysite.settings import RAW_MATERIAL_PATH, PRODUCTION_PATH, SHIPMENT_PATH, BRUSS_ORDERS_PATH, FAURECIA_PATH, SIEGENIA_ORDERS_PATH, METAL_MARTIN_ORDERS_PATH
from mysite.settings import OVERVIEWS_PATH

logger = logging.getLogger(__name__)

file_change_times = {}

path_to_tasks = {
    RAW_MATERIAL_PATH: [mubea_update_stock, mubea_update_orders],
    PRODUCTION_PATH: [mubea_update_production, mubea_update_ident_legend, faurecia_update_production],
    SHIPMENT_PATH: [mubea_update_shipments],
    BRUSS_ORDERS_PATH : [bruss_update_legend, bruss_update_orders],
    FAURECIA_PATH: [faurecia_update_demand_overview, faurcecia_update_ident_legend, faurecia_update_shipments, faurecia_update_orders],
    SIEGENIA_ORDERS_PATH: [siegenia_update_legend, siegenia_update_orders],
    METAL_MARTIN_ORDERS_PATH: [metal_martin_update_legend, metal_martin_update_orders]
}

paths_to_watch = [RAW_MATERIAL_PATH, PRODUCTION_PATH, SHIPMENT_PATH, BRUSS_ORDERS_PATH, FAURECIA_PATH, SIEGENIA_ORDERS_PATH, METAL_MARTIN_ORDERS_PATH]
folders_to_watch = [OVERVIEWS_PATH]

class Command(BaseCommand):
    help = 'Watch for file changes and execute a task'

    def handle(self, *args, **options):
        previous_timestamps = {file_path: os.path.getmtime(file_path) for file_path in paths_to_watch}
        previous_sizes = {path: self.get_size(path) for path in folders_to_watch}

        self.stdout.write(self.style.SUCCESS('File watcher started. Press Ctrl+C to stop.'))

        try:
            while True:
                time.sleep(5)

                for file_path in paths_to_watch:
                    current_timestamp = os.path.getmtime(file_path)
                    
                    if current_timestamp != previous_timestamps[file_path]:
                        logger.info(f"[{datetime.datetime.now()}] File {file_path} has changed")
                        print(f"[{datetime.datetime.now()}] File {file_path} has changed")

                        previous_timestamps[file_path] = current_timestamp

                        for task_func in path_to_tasks[file_path]:
                            task_func()

                for folder_path in folders_to_watch:
                    current_size = self.get_size(folder_path)
                    
                    if current_size != previous_sizes[folder_path]:
                        logger.info(f"[{datetime.datetime.now()}] Folder {folder_path} has changed")
                        print(f"[{datetime.datetime.now()}] Folder {folder_path} has changed")


                        previous_sizes[folder_path] = current_size

                        latest_file = self.get_latest_file(folder_path)

                        if latest_file:
                            logger.info(f"[{datetime.datetime.now()}] Loading data from {latest_file}")
                            print(f"[{datetime.datetime.now()}] Loading data from {latest_file}")
                            mubea_update_demand_overview(file_path=latest_file)


        except KeyboardInterrupt:
            self.stdout.write(self.style.SUCCESS('File watcher stopped.'))

        except FileNotFoundError:
            pass

    def get_size(self, path):
        if os.path.isfile(path):
            return os.path.getsize(path)
        elif os.path.isdir(path):
            return sum(os.path.getsize(os.path.join(root, file)) for root, dirs, files in os.walk(path) for file in files)
        else:
            return 0

    def get_latest_file(self, folder_path):
        files = [f for f in os.listdir(folder_path) if os.path.isfile(os.path.join(folder_path, f))]
        if files:
            return max(files, key=lambda f: os.path.getmtime(os.path.join(folder_path, f)))
        else:
            return None