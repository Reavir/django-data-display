from django.core.management.base import BaseCommand
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

import datetime
import logging
import re
import time

from utils.update_data import *
from mysite.settings import RAW_MATERIAL_PATH, PRODUCTION_PATH, SHIPMENT_PATH, BRUSS_ORDERS_PATH, FAURECIA_PATH, SIEGENIA_ORDERS_PATH, METAL_MARTIN_ORDERS_PATH, HUTCHINSON_ORDERS_PATH, ADIENT_ORDERS_PATH
from mysite.settings import BANK_MUB_PATH, BANK_BRUSS_PATH, BANK_FAURECIA_PATH, BANK_SIEGENIA_PATH, BANK_METALMARTIN_PATH, BANK_HUTCHINSON_PATH, BANK_ADIENT_PATH

logger = logging.getLogger(__name__)

file_change_times = {}

wait_time = 10

path_to_tasks = {
    RAW_MATERIAL_PATH: [mubea_update_stock, mubea_update_orders],
    PRODUCTION_PATH: [mubea_update_production, mubea_update_ident_legend, faurecia_update_production],
    SHIPMENT_PATH: [mubea_update_shipments],
    BRUSS_ORDERS_PATH : [bruss_update_legend, bruss_update_orders],
    FAURECIA_PATH: [faurecia_update_demand_overview, faurcecia_update_ident_legend, faurecia_update_shipments, faurecia_update_orders],
    SIEGENIA_ORDERS_PATH: [siegenia_update_legend, siegenia_update_orders],
    METAL_MARTIN_ORDERS_PATH: [metal_martin_update_legend, metal_martin_update_orders],
    HUTCHINSON_ORDERS_PATH: [hutchinson_update_legend, hutchinson_update_orders],
    ADIENT_ORDERS_PATH: [adient_update_ident_legend, adient_update_orders]
}

paths_to_watch = [BANK_MUB_PATH, BANK_BRUSS_PATH, BANK_FAURECIA_PATH, BANK_SIEGENIA_PATH, BANK_METALMARTIN_PATH, BANK_HUTCHINSON_PATH, BANK_ADIENT_PATH]

class FileChangeHandler(FileSystemEventHandler):
    def on_modified(self, event):
        if not event.is_directory:
            current_time = time.time()
            if event.src_path not in file_change_times or current_time - file_change_times[event.src_path] >= wait_time:
                file_change_times[event.src_path] = current_time
                path = event.src_path
                if path in path_to_tasks:
                    logger.info(f"[{datetime.datetime.now()}] File {event.src_path} has changed")
                    print(f"[{datetime.datetime.now()}] File {event.src_path} has changed")
                    for task_func in path_to_tasks[path]:
                        task_func()
                if re.search(r"overviews\\(\d{8})_GTM overview Tool\.xlsx", event.src_path):
                    logger.info(f"[{datetime.datetime.now()}] File {event.src_path} has changed")
                    print(f"[{datetime.datetime.now()}] Directory {event.src_path} has changed")
                    mubea_update_demand_overview(file_path=event.src_path)

class Command(BaseCommand):
    help = 'Watch for file changes and execute a task'

    def handle(self, *args, **options):
        observers = []
        for path_to_watch in paths_to_watch:
            try:
                event_handler = FileChangeHandler()
                observer = Observer()
                observer.schedule(event_handler, path_to_watch, recursive=True)
                observer.start()

                observers.append(observer)
            except FileNotFoundError:
                self.stdout.write(self.style.ERROR(f'File not found: {path_to_watch}'))

        try:
            self.stdout.write(self.style.SUCCESS('Watching for specific file changes. Press Ctrl-C to exit.'))
            while True:
                time.sleep(1)
        except KeyboardInterrupt:
            for observer in observers:
                observer.stop()
                observer.join()

            self.stdout.write(self.style.NOTICE('File change monitoring stopped.'))