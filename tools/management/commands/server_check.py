from django.core.management.base import BaseCommand

import configparser
import requests
import schedule
import smtplib
import time

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


class Command(BaseCommand):
    help = 'Send a http request to barcode/post endpoint, to check Ngrok tunnel'

    def handle(self, *args, **options):
        config = configparser.ConfigParser()
        config.read('config.ini')

        SENDER_EMAIL = config['Credentials']['mail_app_sender_mail']
        SENDER_PASSWORD = config['Credentials']['mail_app_password']

        SMTP_SERVER = config['Credentials']['mail_app_server']
        SMTP_PORT = int(config['Credentials']['mail_app_port'])
        
        MINUTES = 20

        def send_email(subject, body):
            message = MIMEMultipart()
            message["From"] = SENDER_EMAIL
            message["To"] = "lepnik01@gmail.com, piotr.babicz@galwanotechnika.eu"
            message["Subject"] = subject

            # Attach the body of the email
            message.attach(MIMEText(body, "plain"))

            try:
                server = smtplib.SMTP_SSL(SMTP_SERVER, SMTP_PORT)
                server.login(SENDER_EMAIL, SENDER_PASSWORD)
                server.sendmail(SENDER_EMAIL, message["To"], message.as_string())
                server.quit()
                print("Email sent successfully")
            except Exception as e:
                print(f"Error sending email: {str(e)}")


        def check_backend():
            url = "https://v06wnnc1-8000.euw.devtunnels.ms/tools/barcodes/api/post/"
            payload = {"selectedOption": "Test"}

            try:
                response = requests.post(url, data=payload)
                if response.status_code == 200:
                    self.stdout.write(self.style.HTTP_SUCCESS(f"Backend is working fine at {time.strftime('%Y-%m-%d %H:%M:%S')}"))
                elif response.status_code == 404:
                    self.stdout.write(self.style.HTTP_NOT_FOUND(f"Backend returned status code {response.status_code} at {time.strftime('%Y-%m-%d %H:%M:%S')}"))
                    send_email("Backend Alert", f"Backend returned status code {response.status_code}")
                else:
                    self.stdout.write(self.style.HTTP_BAD_REQUEST(f"Backend returned status code {response.status_code} at {time.strftime('%Y-%m-%d %H:%M:%S')}"))
                    send_email("Backend Alert", f"Backend returned status code {response.status_code}")
            
            except Exception as e:
                self.stdout.write(self.style.HTTP_SERVER_ERROR(f"Backend server error at {time.strftime('%Y-%m-%d %H:%M:%S')}"))
                print(f"Error connecting to backend: {str(e)}")
                send_email("Backend Alert", f"Error connecting to backend: {str(e)}")

        # Schedule the check every 20 minutes (adjust as needed)
        schedule.every(MINUTES).minutes.do(check_backend)

        self.stdout.write(self.style.SUCCESS(f'Sending test request every {MINUTES} min. Press Ctrl-C to exit.'))
        
        try:
            # Initial call
            check_backend()
            # Infinite loop to keep the script running
            while True:
                schedule.run_pending()
                time.sleep(1)

        except KeyboardInterrupt:
            self.stdout.write(self.style.NOTICE('Sending test request stopped.'))
            schedule.clear()
            