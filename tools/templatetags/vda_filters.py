from django import template

register = template.Library()

@register.filter
def get(dictionary, key):
    """Returns the value of the given key from a dictionary."""
    return dictionary.get(key, None)  # Return None if key does not exist

@register.filter
def modify_spec(spec, counter):
    """Remove the last two characters from 'spec' and add a zero-padded counter."""
    if len(spec) >= 2:
        modified_spec = spec[:-2] + str(counter).zfill(2)  # Remove last 2 chars, add padded counter
    else:
        modified_spec = spec  # If spec is less than 2 chars, leave it unchanged
    return modified_spec