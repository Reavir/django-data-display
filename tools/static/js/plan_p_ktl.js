

function updateTileLogos() {
    $('.tile').each(function() {
        var topMiddleValue = $(this).find('.top-middle').text().trim();
        var bottomMiddleValue = $(this).find('.bottom-middle').text().trim();
        var prio = $(this).data('prio');
        if (prio) {
            if (prio === 'prio1') {
                $(this).removeClass('priority-medium');
                $(this).addClass('priority-high');
            } else if (prio === 'prio2') {
                $(this).removeClass('priority-high');
                $(this).addClass('priority-medium');
            } else {
                $(this).removeClass('priority-high');
                $(this).removeClass('priority-medium');
            } 
        }

        var logoSrc = getLogoBasedOnValues(topMiddleValue, bottomMiddleValue);


        $(this).find('.bottom-left-logo').attr('src', logoSrc);
    });
}

function formatDate(inputDate) {
    if (inputDate.includes('-')) {
        return inputDate.split('-').reverse().join('.');
    } else {
        return inputDate.split('.').reverse().join('-');
    }
}

function showContextMenu(e, tile) {
    var contextMenuList = $("#context-menu-list");
    contextMenuList.empty(); // Clear previous items

    $(".day-data").each(function() {        
        $(this).find(".column").each(function() {
            var columnId = $(this).attr('id');
            var columnTitle = $(this).find('h2').text();
            
            // Create list item for the column and append to day's column list
            var listItem = $('<li data-column="' + columnId + '">' + columnTitle + '</li>');
            contextMenuList.append(listItem);
        });
    });

    listItem = $('<li data-column="column-4">' + "Do dodania" + '</li>');
    contextMenuList.append(listItem);

    listItem = $('<li class="li-delete-tile" data-column="delete">' + "Usuń" + '</li>');
    contextMenuList.append(listItem);

    $("#context-menu").css({
        top: e.pageY + "px",
        left: e.pageX + "px"
    }).show().data("tile", tile);
}

function updateTotalExits() {
    $('.column').each(function() {
        var totalExits = 0;
        $(this).find('.tile:visible .bottom-right-hangers').each(function() {
            // Parse the text and ensure it's a valid number (avoid NaN)
            var value = parseFloat($(this).text());
            if (!isNaN(value)) {
                totalExits += value;
            }
        });
        $(this).find('.total-exits').text("#: " + Math.ceil(totalExits));
    });
}

function showToast(message) {
    var toastContainer = $('#toast-container');

    // Create a new toast element
    var toast = $('<div class="toast-notification"></div>').text(message);

    // Append the toast to the container
    toastContainer.append(toast);

    // Animate the toast
    toast.animate({ opacity: 1 }, 400).delay(2000).fadeOut(400, function() {
        $(this).remove();
    });
}

function UIHandler() {
    // Get all day elements
    var dayElements = $('.day-data');
    var currentIndex = 0;

    function showDay(index) {
        dayElements.removeClass('active');
        $(dayElements[index]).addClass('active');
        updateTotalExits();
    }

    // Find the first .day-data that contains at least one .tile
    dayElements.each(function(index) {
        if ($(this).find('.tile').length > 0) {
            currentIndex = index; // Set the index to the first match
            return false; // Break the loop
        }
    });

    // Add the 'active' class to the first matching day-data
    $(dayElements[currentIndex]).addClass('active');
    updateTotalExits();
    
    // Handle previous day button click
    $('.prev-day').on('click', function() {
        if (currentIndex > 0) {
            currentIndex--;
            showDay(currentIndex);
        }
    });

    // Handle next day button click
    $('.next-day').on('click', function() {
        if (currentIndex < dayElements.length - 1) {
            currentIndex++;
            showDay(currentIndex);
        }
    });

    $('#get-plan').change(function() {
        $('#get-plan-form').submit();
    });

    $('#hideSideBarButton').click(function() {
        $('.sticky-buttons-top').slideToggle('slow'); // Toggle visibility with a slide effect
        $(this).toggleClass('rotate-180'); // Toggle rotation class
    });

    $('#searchInputToAdd').on('keyup', function() {
        var value = $(this).val().toLowerCase();
        $('.tilesToAdd .tile').filter(function() {
            var topMiddleText = $(this).find('.top-middle').text().toLowerCase();
            var bottomMiddleText = $(this).find('.bottom-middle').text().toLowerCase();
            $(this).toggle(topMiddleText.includes(value) || bottomMiddleText.includes(value));
        });
    });

    $('#searchDays').on('keyup', function() {
        var value = $(this).val().toLowerCase();
        if (value === '') {
            $('.orders-container').css('display', 'block');
            dayElements.removeClass('active');
            $(dayElements[currentIndex]).addClass('active');
            $('.prev-day').show();
            $('.next-day').show();

            $('.column').each(function() {
                var column = $(this);
                var tiles = column.find('.tile');
                tiles.each(function() {
                    $(this).toggle(true);
                });
                column.toggle(true);
            });
        } else {
            // Execute additional functions when the search field is not empty
            $('.orders-container').css('display', 'flex');
            dayElements.addClass('active');
            $('.prev-day').hide();
            $('.next-day').hide();
        
            dayElements.each(function() {
                var dayData = $(this);
                var columns = dayData.find('.column');
                var hasVisibleTiles = false;

                columns.each(function() {
                    var column = $(this);
                    var tiles = column.find('.tile');
                    var visibleTiles = 0;

                    tiles.each(function() {
                        var topMiddleText = $(this).find('.top-middle').text().toLowerCase();
                        var bottomMiddleText = $(this).find('.bottom-middle').text().toLowerCase();
                        var topRightText = $(this).find('.top-right').text().toLowerCase();
                        var topLeftText = $(this).find('.top-left').text().toLowerCase();
                        var isVisible = topMiddleText.includes(value) || bottomMiddleText.includes(value) || topRightText.includes(value) || topLeftText.includes(value);
                        $(this).toggle(isVisible);
                        if (isVisible) {
                            visibleTiles++;
                        }
                    });

                    // Toggle the column visibility
                    column.toggle(visibleTiles > 0);

                    if (visibleTiles > 0) {
                        hasVisibleTiles = true;
                    }
                });

                // Hide the .day-data element if no columns have visible tiles
                if (hasVisibleTiles){
                    dayData.addClass('active');
                } else {
                    dayData.removeClass('active');
                }
            });
        }
    });

    $(document).on('keydown', function (event) {
        if (event.key == 'MediaRewind'  ) {
            event.preventDefault();
            if (currentIndex > 0) {
                currentIndex--;
                showDay(currentIndex);
            }
        }

        else if (event.key == 'MediaPause') {
            event.preventDefault();
            currentIndex = 0;
            showDay(currentIndex);
        }
        
        else if (event.key == 'MediaFastForward') {
            event.preventDefault();
            if (currentIndex < dayElements.length - 1) {
                currentIndex++;
                showDay(currentIndex);
            }
        }
        
        else if (event.key == 'MediaPlay') {
            event.preventDefault();
            window.location.reload();
        }

        else if (event.code == 'ZoomToggle') {
            event.preventDefault();
            if (!document.fullscreenElement) {
                // If not in fullscreen, request fullscreen
                if (document.documentElement.requestFullscreen) {
                    document.documentElement.requestFullscreen();
                } else if (document.documentElement.mozRequestFullScreen) { // Firefox
                    document.documentElement.mozRequestFullScreen();
                } else if (document.documentElement.webkitRequestFullscreen) { // Chrome, Safari, Opera
                    document.documentElement.webkitRequestFullscreen();
                } else if (document.documentElement.msRequestFullscreen) { // IE/Edge
                    document.documentElement.msRequestFullscreen();
                }
                $(".column").css("margin", "0 0");
            } else {
                // If in fullscreen, exit fullscreen
                if (document.exitFullscreen) {
                    document.exitFullscreen();
                } else if (document.mozCancelFullScreen) { // Firefox
                    document.mozCancelFullScreen();
                } else if (document.webkitExitFullscreen) { // Chrome, Safari, Opera
                    document.webkitExitFullscreen();
                } else if (document.msExitFullscreen) { // IE/Edge
                    document.msExitFullscreen();
                }
                $(".column").css("margin", "0 10px");
            }
        }
    });

    $('#editTilePrompt input').on('click', function() {
        let selectedOption = $(this).val();
        if (selectedOption === 'prio1') {
            $('#tileEdit').removeClass('priority-medium');
            $('#tileEdit').addClass('priority-high');
        }
        else if (selectedOption === 'prio2') {
            $('#tileEdit').removeClass('priority-high');
            $('#tileEdit').addClass('priority-medium');
        } else {
            $('#tileEdit').removeClass('priority-high');
            $('#tileEdit').removeClass('priority-medium');
        } 
        
    });

    // $(document).on('keydown', function (event) {
    //     // Extract key properties
    //     const properties = {
    //         key: event.key,
    //         code: event.code,
    //         keyCode: event.keyCode,
    //         altKey: event.altKey,
    //         ctrlKey: event.ctrlKey,
    //         shiftKey: event.shiftKey,
    //         metaKey: event.metaKey,
    //         repeat: event.repeat,
    //     };

    //     // Format the properties for display
    //     const formattedProperties = Object.entries(properties)
    //         .map(([key, value]) => `${key}: ${value}`)
    //         .join('\n');

    //     // Update the output div
    //     $('#output').text(formattedProperties);
    // });
}

function tileHandler(token) {
    var selectedTiles = [];

    var lastSelectedTile = null;

    $("#editTilePrompt").dialog({
        autoOpen: false,
        modal: true,
        width: 350,
        open: function() {
            // Clear previous options
            let targetHour = $("#editTilePrompt").data("targetHour"); // Get stored tile reference
            let targetPrio = $("#editTilePrompt").data("targetPrio"); // Get stored tile reference
            let targetTile = $("#editTilePrompt").data("targetTile");

            $("#tilePromptTextInput").val(targetHour);
            $('#tileEdit').find('.top-right').val(formatDate(targetTile.find('.top-right').text()));
            $('#tileEdit').find('.top-left').val(formatDate(targetTile.find('.top-left').text()));
            $('#tileEdit').find('.top-middle-hour').text(targetTile.find('.top-middle-hour').text());
            $('#tileEdit').find('.top-middle').text(targetTile.find('.top-middle').text());
            $('#tileEdit').find('.middle-2').text(targetTile.find('.middle-2').text());
            $('#tileEdit').find('.middle').text(targetTile.find('.middle').text());
            $('#tileEdit').find('.bottom-middle').text(targetTile.find('.bottom-middle').text());
            $('#tileEdit').find('.bottom-right-hangers').text(targetTile.find('.bottom-right-hangers').text());

            if (targetPrio === 'prio1') {
                $("input[name='choice'][value='prio1']").prop('checked', true);
                $('#tileEdit').removeClass('priority-medium');
                $('#tileEdit').addClass('priority-high');
            } else if (targetPrio === 'prio2') {
                $("input[name='choice'][value='prio2']").prop('checked', true);
                $('#tileEdit').removeClass('priority-high');
                $('#tileEdit').addClass('priority-medium');
            } else {
                $("input[name='choice'][value='none']").prop('checked', true);
                $('#tileEdit').removeClass('priority-medium');
                $('#tileEdit').removeClass('priority-high');
            }
        },
        buttons: {
            "Zatwierdź": function () {
                let selectedOption = $("input[name='choice']:checked").val();
                let targetTile = $("#editTilePrompt").data("targetTile"); // Get stored tile reference

                targetTile.find('.top-right').text(formatDate($('#tileEdit').find('.top-right').val()));
                targetTile.find('.top-left').text(formatDate($('#tileEdit').find('.top-left').val()));
                targetTile.find('.top-middle-hour').text($('#tileEdit').find('.top-middle-hour').text());
                targetTile.find('.top-middle').text($('#tileEdit').find('.top-middle').text());
                targetTile.find('.middle-2').text($('#tileEdit').find('.middle-2').text());
                targetTile.find('.middle').text($('#tileEdit').find('.middle').text());
                targetTile.find('.bottom-middle').text($('#tileEdit').find('.bottom-middle').text());
                targetTile.find('.bottom-right-hangers').text($('#tileEdit').find('.bottom-right-hangers').text());

                if ($('#tileEdit').find('.top-middle-hour').text() !== '') {
                    // Remove existing 'top-middle-hour' elements before inserting a new one
                    targetTile.find(".top-middle-hour").remove();
                    $("<div class='top-middle-hour'>" + $('#tileEdit').find('.top-middle-hour').text() + "</div>").insertBefore(targetTile.find(".top-middle"));
                } else {
                    targetTile.find(".top-middle-hour").remove();
                }

                if ($('#tileEdit').find('.middle-2').text() !== '') {
                    targetTile.find(".middle-2").remove();
                    $("<div class='middle-2'>" + $('#tileEdit').find('.middle-2').text() + "</div>").insertBefore(targetTile.find(".middle"));
                } else {
                    targetTile.find(".middle-2").remove();
                }


                if (selectedOption === 'prio1') {
                    targetTile.data('prio', selectedOption);
                    targetTile.removeClass('priority-medium');
                    targetTile.addClass('priority-high');
                }
                else if (selectedOption === 'prio2') {
                    targetTile.data('prio', selectedOption);
                    targetTile.removeClass('priority-high');
                    targetTile.addClass('priority-medium');
                } else {
                    targetTile.removeData('prio');
                    targetTile.removeClass('priority-high');
                    targetTile.removeClass('priority-medium');
                }   
                $(this).dialog("close");
            },
            "Anuluj": function () {
                $(this).dialog("close");
            }
        }
    });

    $(document).on("click", ".tile", function (event) {
        $("#editTilePrompt").dialog("close");

        var $this = $(this);

        // Ripple effect
        const existingRipple = this.querySelector('.ripple');
        if (existingRipple) existingRipple.remove();

        // Get the click position and button dimensions
        const rect = this.getBoundingClientRect();
        const ripple = document.createElement('span');
        const size = Math.max(rect.width, rect.height);
        const x = event.clientX ? event.clientX - rect.left - size / 2 : event.clientX || rect.width / 2 - size / 2;
        const y = event.clientY ? event.clientY - rect.top - size / 2 : event.clientY || rect.height / 2 - size / 2;

        // Set ripple properties
        ripple.style.width = ripple.style.height = `${size}px`;
        ripple.style.left = `${x}px`;
        ripple.style.top = `${y}px`;
        ripple.classList.add('ripple');

        // Append ripple to the button
        this.appendChild(ripple);

        // Remove ripple after animation
        ripple.addEventListener('animationend', () => ripple.remove());

        // Check if the Shift key is pressed
        if (event.shiftKey && lastSelectedTile) {
            // Get all tiles in the same column
            var columnTiles = $this.closest('.column').find('.tile');
            
            // Find the index of the current tile and the last selected tile
            var startIndex = columnTiles.index(lastSelectedTile);
            var endIndex = columnTiles.index($this);

            // Ensure we always select from the lower index to the higher index
            if (startIndex > endIndex) {
                [startIndex, endIndex] = [endIndex, startIndex];
            }

            // Select all tiles in the range
            columnTiles.slice(startIndex, endIndex + 1).addClass('selected');
        } 
        
        else if (event.ctrlKey) {
            // Toggle selection with Ctrl key
            $this.toggleClass("selected");
        }

        else if (event.altKey) {
            $("#editTilePrompt").data("targetTile", $this); // Store it inside dialog
            $("#editTilePrompt").data("targetHour", $this.find(".top-middle-hour").text()); // Store it inside dialog
            $("#editTilePrompt").data("targetPrio", $this.data('prio')); // Store it inside dialog
            $("#editTilePrompt").dialog("open");
            
            // let hour = prompt("Wpisz godzinę:");
    
            // let $existingHour = $this.find(".top-middle-hour");

            // if (hour === null) return; // Exit if canceled

            // if (hour.trim() === "") {
            //     $existingHour.remove(); // Remove if empty
            // } else {
            //     if ($existingHour.length) {
            //         $existingHour.text(hour); // Update existing
            //     } else {
            //         $("<div class='top-middle-hour'>" + hour + "</div>").insertBefore($this.find(".top-middle"));
            //     }
            // }
        }

        else if ($this.hasClass("selected")) {
            $this.removeClass("selected");  
        }

        else {
            // Deselect all other tiles if neither Ctrl nor Shift is pressed
            $(".tile").removeClass("selected");
            $this.addClass("selected");
        }

        // Update the lastSelectedTile variable
        lastSelectedTile = $this;

        // Update the selectedTiles array
        selectedTiles = $(".tile.selected").toArray();
    });

    $(".column").sortable({
        connectWith: ".column",
        items: ".tile",
        placeholder: "ui-state-highlight",
        // Define a helper function to clone and drag multiple items
        helper: function (event, item) {
            if (event.shiftKey || event.ctrlKey) {
                return false; // Prevent dragging if Shift or Ctrl is pressed
            }
    
            var selected = $(".selected");
    
            // If the dragged item is not part of the selected group, select it
            if (!item.hasClass("selected")) {
                selected.removeClass("selected");
                item.addClass("selected");
                selected = item;
            }
    
            // Create a helper element to represent the group of selected items
            var helper = $('<div/>');
    
            // Clone and append each selected tile to the helper
            selected.each(function () {
                var clone = $(this).clone();
                clone.addClass('dragging'); // Add a class to distinguish
                helper.append(clone);
            });
    
            // Set the width and height of the helper based on the original tile's size
            var originalTileWidth = selected.first().outerWidth(); // Get width of the first selected tile
            var originalTileHeight = selected.first().outerHeight(); // Get height of the first selected tile
            helper.width(originalTileWidth);  // Set helper width
            helper.height(originalTileHeight * selected.length);
    
            return helper;
        },
    
        start: function (event, ui) {
            $('.tile').css('cursor', 'grabbing');

            // Prevent dragging if Shift or Ctrl is pressed
            if (event.shiftKey || event.ctrlKey) {
                $(".column").sortable("cancel");
                return;
            }
    
            // Hide only non-dragged selected tiles
            $(".selected").not(".dragging").each(function () {
                $(this).hide();  // Hide non-dragged selected tiles
            });
    
            ui.placeholder.height(ui.helper.outerHeight());
            ui.placeholder.width(ui.helper.outerWidth());
    
            // Add 'dragging' class to the actual selected tiles
            $(".selected").addClass("dragging");
        },
    
        stop: function (event, ui) {
            $('.tile').css('cursor', 'grab');
            // Show all hidden selected tiles after dragging stops
            $(".selected").each(function () {
                $(this).show();
            });
    
            // Remove 'dragging' class after dragging stops
            $(".tile").removeClass("dragging");
    
            var selected = $(".selected");
    
            // Move each selected item to the new location
            selected.each(function () {
                $(this).insertBefore(ui.item);
            });
    
            // Clear selection after moving
            $(".tile").removeClass("selected");
    
            // Update total exits or other tasks
            updateTotalExits();
        },
    
        receive: function (event, ui) {
            // Accessing properties of the target column
            var senderColumn = $(ui.sender);
            var senderPrio = senderColumn.find('.prio').text();
            var senderDate = senderColumn.closest('.day-data').find('span').text();
    
            var receiverColumn = $(ui.item).closest('.column');
            var receiverPrio = receiverColumn.find('.prio').text();
            var receiverDate = receiverColumn.closest('.day-data').find('span').text();
    
            var tileDesc = $(ui.item).find('.top-middle').text();
    
            var toastMsg = tileDesc + '\n' +
                senderDate + " | " + senderPrio + '\n' +
                "\\\/" + '\n' +
                receiverDate + " | " + receiverPrio;
    
            // Show toast message with the top-middle value
            showToast(toastMsg);
        }
    });

    $(document).on("contextmenu", ".tile", function (e) {
        e.preventDefault();
        showContextMenu(e, $(this));
    });

    $(document).on("click", function() {
        $("#context-menu").hide();
    });

    $("#context-menu-list").on("click", "li", function() {
        var columnData = $(this).data("column");
    
        // Get the tile from the context menu data
        var tile = $("#context-menu").data("tile");
        
        // Check if there are any selected tiles
        var selectedTiles = $(".selected");
    
        // Determine the tiles to move: all selected tiles or just the clicked tile
        var tilesToMove = selectedTiles.length > 0 ? selectedTiles : tile;
        
        if (columnData === 'delete') {
            var codesArray = tilesToMove.map(function () {
                return $(this).find(".bottom-middle").text().trim();
            }).get();

            if (confirm(`Usunąć zamówienia ${codesArray.join(", ")} z planowania?`)) {
                $.ajax({
                    url: "api",
                    method: 'POST',
                    data: {
                        'action': 'delete_used',
                        'codes': JSON.stringify(codesArray),
                        'csrfmiddlewaretoken': token,
                    },
                    success: function (data) {
                        console.log("response", data); 
                        tilesToMove.slideUp(400);             
                    },
                    error: function (error) {
                        console.log("error", error);
                    }
                });
            }
            
        } else {
            var senderColumn = tile.closest('.column');
            var senderPrio = senderColumn.find('.prio').text();
            var senderDate = senderColumn.closest('.day-data').find('span').text();

            var receiverColumn = $('#' + columnData);
            var receiverPrio = receiverColumn.find('.prio').text();
            var receiverDate = receiverColumn.closest('.day-data').find('span').text();

            var moveTiles = function(tiles) {
                tiles.each(function() {
                    var tileDesc = $(this).find('.top-middle').text();
                    $(this).slideUp(400);
        
                    setTimeout(function() {
                        // Move the tile to the receiver column and slide it down
                        $('#' + columnData + ' .column-header').after($(this).hide().slideDown(400));
                        updateTotalExits();
                    }.bind(this), 500);
        
                    var toastMsg = tileDesc + '\n' +
                                    senderDate + " | " + senderPrio + '\n' +
                                    "\\\/" + '\n' +
                                    receiverDate + " | " + receiverPrio;
                    showToast(toastMsg);  // Show toast notification for each tile
                });
            };
        
            // Move the tiles (either all selected or just the single one)
            moveTiles(tilesToMove);

            // Clear selection after moving tiles
            $(".selected").removeClass("selected");

            // Hide the context menu after action
            $("#context-menu").hide();
        }
    });

    function getColumnData(columnId) {
        return $(`#${columnId} .top-right`).map(function() {
            return $(this).text();
        }).get()
    }

    $('#auto-assign').on("click", function() {
        let columnData = {
            'column-1': getColumnData('column-1'),
            'column-2': getColumnData('column-2'),
            'column-3': getColumnData('column-3')
        };
        
        console.log(columnData);

        let promise = Promise.resolve();

        $('.tilesToAdd .tile').each(function() {
            let $tile = $(this);
            let column = $tile.find('.top').text();
            let currentDate = $tile.find('.top-right').text();

            let mainColumn = 'column-1';
            switch (column) {
                case 'zawieszka':
                    mainColumn = 'column-1'
                    break

                case 'tromel':
                    mainColumn = 'column-2'
                    break
             
                case 'aluminium':
                    mainColumn = 'column-3'
                    break
            }

            let currnetColumnData = columnData[mainColumn];
        
            // Search for date starting from the back
            index = currnetColumnData.lastIndexOf(currentDate);
        
            // Insert the new value right after the found index, or at the end
            if (index !== -1) {
                currnetColumnData.splice(index + 1, 0, currentDate);
            } else {
                currnetColumnData.push(currentDate);
            }

            // Slide up the current tile
            promise = promise.then(() => {
                return $tile.slideUp(500).promise(); // Wait for slideUp to finish
            }).then(() => {
                let $targetColumn = $(`#${mainColumn}`);
                let $tiles = $targetColumn.find('.tile');
                let tileOffset;
                // Insert after the found index, or at the end if not found
                if (index >= 0 && index < $tiles.length) {
                    $tiles.eq(index).after($tile);
                    tileOffset = $tiles.eq(index).offset().top;
                } else {
                    $targetColumn.append($tile);
                    tileOffset = $tiles.eq(-1).offset().top;
                }
                console.log(tileOffset);
        
                // Scroll to the tile before it slides down
                return $('html, body').animate({
                    scrollTop: tileOffset
                }, 2000).promise();
            }).then(() => {
                return $tile.slideDown(500).promise(); // Wait for slideDown to finish
            });

        });
    });
}

function newTileHandler(optionNameList, listBorderIndex) {
    $(document).on('input', '#article', function(event) {
        var article = $(this);

        enteredText = article.contents().filter(function() {
			return this.nodeType == 3;
		}).first().text().trim().toLowerCase().replace(/\s/g, '');

        var enteredTextWhole =  article.text().trim().toLowerCase();
        if (enteredTextWhole.length == 1) {
            enteredText = enteredTextWhole;
            article.html(`${enteredText}<div id="options-name" class="options-container no-print"></div>`);
            var node = $(this).contents().filter(function() {
                return this.nodeType === 3;
            }).first()[0];
        
            var range = document.createRange();
            var selection = window.getSelection();
            range.setStart(node, node.length);
            range.collapse(true);
            selection.removeAllRanges();
            selection.addRange(range);
        } 

        var hangers = $(this).closest('form').find('#hangers');
        var client = $(this).closest('form').find('#client');
        var optionsName = $(this).closest('form').find('#options-name');
        optionsName.empty();
        
        optionNameList.forEach((option, index) => {
            let optionString = `${option.number} ${option.description}`;
            if (enteredText != '' && optionString.toLowerCase().includes(enteredText)) {
                const optionElement = document.createElement('div');
                optionElement.classList.add('option');
                optionElement.textContent = optionString;
                
                optionElement.addEventListener('mousedown', function() {
                    var exitsText = option.n_exits_ph ? option.n_exits_ph : option.n_exits_ktl;
                    hangers.text(exitsText);
                    var clientText;
                    if ($(this).text().includes('KSB,AU436')) {
                        clientText = "MUB"
                    }
                    else if (index >= listBorderIndex) {
                        clientText = "MET";
                    }
                    else {
                        clientText = "KES";
                    }

                    client.text(clientText);
                    article.html(`${optionString}<div id="options-name" class="options-container no-print"></div>`);
                    optionsName.empty();

                });
                optionsName.append(optionElement);
            }
        });
    });
    
    $(document).on('focusout', '#article', function(event) {
        var optionsName = $(this).closest('form').find('#options-name');
        optionsName.remove(); // Remove the element when input loses focus
    });

    $('#create-tile-form').on('click', function() {
        $('#newTileForm').slideToggle();
    });

    $('#newTileForm').on('submit', function(event) {
        event.preventDefault(); // Prevent default form submission
        
        // Collect field values
        const deliverydate = $('#deliverydate').val();
        const deadlinedate = $('#deadlinedate').val();
        const article = $('#article').text().trim();
        const ls = $('#ls').text().trim();
        const quantity = $('#quantity').text().trim();
        const code = $('#code').text().trim();
        const hangers = $('#hangers').text().trim();
        const client = $('#client').text().trim();

        const parsedQuantity = parseFloat(quantity) || 0;
        const parsedHangers = parseFloat(hangers) || 0;

        const hangersPerQuantity = (parsedHangers > 0 && parsedQuantity > 0)
            ? roundToHalf(parsedQuantity / parsedHangers)
            : 0;

        let tileHTML =  `
                <div class="tile ui-sortable-handle">
                    <div class="top-right">${deadlinedate}</div>
                    <div class="top-left">${deliverydate}</div>
                    <div class="top-middle">${article}</div>
                    <div class="middle-2">${ls ? `LS: ${ls}` : ''}</div>
                    <div class="middle">${quantity} szt.</div>
                    <div class="bottom-middle">${code}</div>
                    <img src="${getLogoBasedOnValues(article, client)}" alt="Logo" class="bottom-left-logo">
                    <div class="bottom-right-hangers">${hangersPerQuantity}</div>
                </div>`;

        $('.tilesToAdd').prepend(tileHTML);
        $('.tilesToAdd').animate({
            scrollTop: 0        
        }, 1500);
    });
}

let retryInterval = 1000;
const maxInterval = 30000;

function barcodesSocketHandler(codes, line) {
    function connectWebSocket(reload) {
        // Initialize the WebSocket
        const socket = new WebSocket('ws://' + window.location.host + '/ws/scanned-codes/');

        socket.onmessage = function(e) {
            const barcode = JSON.parse(e.data);
            if (codes.includes(barcode.code)) {
                if (line == "KTL") {
                    if (barcode.line.includes("KTL")) {
                        if (barcode.crud_type == "create") {
                            let tileToRemove = $('.bottom-middle').filter(function() {
                                var text = $(this).text();
                                if (text.length == 0 ) {
                                    return false
                                }
                                
                                return barcode.code == text;
                            }).closest('.tile');
                            tileToRemove.each(function() {
                                var tile = $(this);
                                tile.stop(true, true).slideUp(5000);
                            });
    
                            $('html, body').scrollTop(0);
    
                            updateTotalExits();
                            console.log(`Ready KTL ${barcode.code}`);
                            toastr.info(`Ready KTL ${barcode.code}`);

                        } else if (barcode.crud_type == "delete") {
                            let tileToAdd = $('.bottom-middle').filter(function() {
                                var text = $(this).text();
                                if (text.length == 0 ) {
                                    return false
                                }
                                
                                return barcode.code == text;
                            }).closest('.tile');
    
                            tileToAdd.each(function() {
                                var tile = $(this);
                                tile.slideDown(5000);
                            });

                            console.log(`Undeleted KTL ${barcode.code}`);
                            toastr.info(`Undeleted KTL ${barcode.code}`);
                        }

                    } else if (barcode.line == "Fosforanowanie") {
                        if (barcode.crud_type == "create") {
                            let tileToAnimate = $('.bottom-middle').filter(function() {
                                var text = $(this).text();

                                if (text.length == 0 ) {
                                    return false
                                }
                                
                                return barcode.code == text;
                            }).closest('.tile');

                            
                            tileToAnimate.each(function() {
                                var intervalId = startColorCycle($(this))
                            });
                            console.log(`Ready PH ${barcode.code}`);
                            toastr.info(`Ready PH ${barcode.code}`);
                        } else if (barcode.crud_type == "delete") {
                            let tileToUnAnimate = $('.bottom-middle').filter(function() {
                                var text = $(this).text();

                                if (text.length == 0 ) {
                                    return false
                                }
                                
                                return barcode.code == text;
                            }).closest('.tile');

                            
                            tileToUnAnimate.each(function() {
                                var tile = $(this);
                                tile.stop();
                            });
                            console.log(`Undeleted Ph ${barcode.code}`);
                            toastr.info(`Undeleted Ph ${barcode.code}`);
                        }
                    }
                }
                else if (line == "Ph" && barcode.line == "Fosforanowanie") {
                    let tileToRemove = $('.bottom-middle').filter(function() {
                        var text = $(this).text();

                        if (text.length == 0 ) {
                            return false
                        }
                        
                        return barcode.code == text;
                    }).closest('.tile');

                    tileToRemove.each(function() {
                        var tile = $(this);
                        tile.slideUp(5000);
                    });

                    $('html, body').scrollTop(0);

                    updateTotalExits();
                    console.log(`Ready PH ${barcode.code}`);
                    toastr.info(`Ready PH ${barcode.code}`);
                }
            }
        };
    
        socket.onopen = function(e) {
            if (reload) {
                location.reload();  
            }

            toastr.success("Barcodes socket connection established.");
        };

        // Handle errors
        socket.onerror = function(error) {
            console.error("Barcodes socket error:", error);
        };

        // Handle closure and attempt to reconnect
        socket.onclose = function(event) {
            console.log(`Barcodes socket connection closed. Reason: ${event.reason || "Unknown"}`);
            reconnectWebSocket(); // Attempt to reconnect
        };
    }

    function reconnectWebSocket() {
        toastr.info(`Attempting to reconnect in ${retryInterval / 1000} seconds...`);
        setTimeout(() => {
            retryInterval = Math.min(retryInterval * 2, maxInterval); // Exponential backoff
            connectWebSocket(true); // Reconnect
        }, retryInterval);
    }

    connectWebSocket();
}

function planKTLSocketHandler() {
    let socket = null;

    function connectWebSocket() {
        socket = new WebSocket('ws://' + window.location.host + '/ws/plan_ktl_updates/');

        socket.onmessage = function(e) {
            const data = JSON.parse(e.data);
            // Check the type of action received from the WebSocket
            if (data.type === 'add' || data.type === 'update' || data.type === 'delete') {
                // Reload the window on any update (add, update, or delete)
                window.location.reload();
                console.log('reloaded');
            }
        };

        socket.onopen = function(e) {
            console.log('Plan KTL socket connection established');  // Show toast notification for each tile
        };

        socket.onerror = function(error) {
            console.error('Plan KTL socket Error:', error);
        };

        socket.onclose = function(event) {
            console.log(`Plan KTL socket connection closed. Reason: ${event.reason || "Unknown"}`);
            reconnectWebSocket(); // Attempt to reconnect
        };
    }

    function reconnectWebSocket() {
        console.log(`Plan KTL socket attempting to reconnect in ${retryInterval / 1000} seconds...`);
        setTimeout(() => {
            retryInterval = Math.min(retryInterval * 2, maxInterval); // Exponential backoff
            connectWebSocket(); // Reconnect
        }, retryInterval);
    }

    connectWebSocket(); 

    let highlightedText = "";
    let messegeType = "";

    $(document).on('keydown', function (event) {
        if (event.key == 'ColorF0Red' || event.key == 't') {
            $("#emailForm").fadeToggle();
        }
    });

    $("#emailForm").on("click", ".emailFormElement", function () {
        $(".emailFormElement").removeClass("highlight"); // Remove highlight from all
        highlightedText = $(this).text();
        messegeType = $(this).data('type');
        $(this).addClass("highlight"); // Add highlight to the clicked element
    });

    $("#emailForm").on("click", "#sendEmail", function () {
        if (confirm("Wysłać wiadomość?") == true) {
            if (socket && socket.readyState === WebSocket.OPEN) {
                socket.send(JSON.stringify({
                    'type': "status",
                    'title': `KTL produkcja [${messegeType}]`,
                    'message': highlightedText,
                }));

                toastr.success('Wysłano wiadomość do serwera.');
                $(".emailFormElement").removeClass("highlight");
                $("#emailForm").fadeOut();
            } else {
                toastr.info('Nie można wysłać wiadomości. Połączenie przerwane.');
            }
        }
    });

    function captureAndSendScreenshotAsJson(intervalInMS) {
        function captureAndSend() {
          html2canvas(document.body).then(canvas => {
            const imageData = canvas.toDataURL("image/png");
            const timestamp = Date.now();
            
            if (socket && socket.readyState === WebSocket.OPEN) {
                socket.send(JSON.stringify({
                    'type': "screenshot",
                    'timestamp': formatTimestamp(timestamp),
                    'data': imageData,
                }));
            }
          });
        }
      
        setInterval(captureAndSend, intervalInMS);
      }
      
    captureAndSendScreenshotAsJson(60 * 60 * 1000);
}

function planPSocketHandler() {
    let socket = null;

    function connectWebSocket() {
        socket = new WebSocket('ws://' + window.location.host + '/ws/plan_p_updates/');

        socket.onmessage = function(e) {
            const data = JSON.parse(e.data);
            // Check the type of action received from the WebSocket
            if (data.type === 'add' || data.type === 'update' || data.type === 'delete') {
                // Reload the window on any update (add, update, or delete)
                window.location.reload();
                console.log('reloaded');
            }
        };

        socket.onopen = function(e) {
            console.log('Plan Ph socket connection established');  // Show toast notification for each tile
        };

        socket.onerror = function(error) {
            console.error('Plan Ph socket Error:', error);
        };

        socket.onclose = function(event) {
            console.log(`Plan Ph socket connection closed. Reason: ${event.reason || "Unknown"}`);
            reconnectWebSocket(); // Attempt to reconnect
        };
    }

    function reconnectWebSocket() {
        console.log(`Plan Ph socket attempting to reconnect in ${retryInterval / 1000} seconds...`);
        setTimeout(() => {
            retryInterval = Math.min(retryInterval * 2, maxInterval); // Exponential backoff
            connectWebSocket(); // Reconnect
        }, retryInterval);
    }

    connectWebSocket();

    let highlightedText = "";
    let messegeType = "";

    $(document).on('keydown', function (event) {
        if (event.key == 'ColorF0Red' || event.key == 't') {
            $("#emailForm").fadeToggle();
        }
    });

    $("#emailForm").on("click", ".emailFormElement", function () {
        $(".emailFormElement").removeClass("highlight"); // Remove highlight from all
        highlightedText = $(this).text();
        messegeType = $(this).data('type');
        $(this).addClass("highlight"); // Add highlight to the clicked element
    });

    $("#emailForm").on("click", "#sendEmail", function () {
        if (confirm("Wysłać wiadomość?") == true) {
            if (socket && socket.readyState === WebSocket.OPEN) {
                socket.send(JSON.stringify({
                    'type': "status",
                    'title': `Fosfor produkcja [${messegeType}]`,
                    'message': highlightedText,
                }));

                toastr.success('Wysłano wiadomość do serwera.');
                $(".emailFormElement").removeClass("highlight");
                $("#emailForm").fadeOut();
            } else {
                toastr.info('Nie można wysłać wiadomości. Połączenie przerwane.');
            }
        }
    });
}

function clientSocketHandler() {
    $(document).ready(function () {
        let pageName = window.location.pathname.replaceAll("/", "") || "home";
        //$("#page-name").text(pageName);
        let socket = new WebSocket("ws://" + window.location.host + "/ws/clients/" + pageName + "/");

        socket.onmessage = function (event) {
            let data = JSON.parse(event.data);
            let $clientList = $("#client-list");
            $clientList.empty();
            
            $.each(data.clients, function (index, client) {
                const $li = $("<li>");
                const $dot = $("<div>").addClass("client-dot");
                const $name = $("<span>").text(client);
        
                // Append the dot and name to the list item
                $li.append($dot).append($name);
        
                // Append the list item to the client list
                $clientList.append($li);
            });
        };
    });
}

function clientHSocketHandler() {
    $(document).ready(function () {
        let pageName = window.location.pathname.replaceAll("/", "") || "home";
        //$("#page-name").text(pageName);
        let socket = new WebSocket("ws://" + window.location.host + "/ws/clients/" + pageName + "/");
        socket.onmessage = function (event) {
            let data = JSON.parse(event.data);
            console.log("Connected clients: " + data.clients.join(", "));
        };
    });
}

function containsAllChars(mainString, subString) {
    for (let char of subString) {
        if (!mainString.includes(char)) {
            return false;
        }
    }
    return true;
}

function roundToHalf(value) {
    const rounded = Math.round(value * 2) / 2;
    // return rounded % 1 === 0 ? rounded.toFixed(1) : rounded;
    return rounded;
}

function formatTimestamp(timestamp) {
    const date = new Date(timestamp);
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0'); // Add leading zero for single-digit months
    const day = String(date.getDate()).padStart(2, '0');
    const hours = String(date.getHours()).padStart(2, '0');
    const minutes = String(date.getMinutes()).padStart(2, '0');
    const seconds = String(date.getSeconds()).padStart(2, '0');
  
    return `${year}-${month}-${day} ${hours}_${minutes}_${seconds}`;
}

var colors = ['#ffffff', '#aef5bd']; // Array of colors to pulse between
function startColorCycle(tile) {
    currentIndex = 0
    intervalId = setInterval(function() {
        tile.animate({
            backgroundColor: colors[currentIndex]
        }, 3000);
        
        // Switch to the next color
        currentIndex = (currentIndex + 1) % colors.length;
    }, 3000);

    return intervalId;
}