function datePickerHandler(deliShip) {
    // List of highlighted dates
    const highlightedDates = $('.tile-container').map(function () {
        return $(this).data('date');
    }).get();

    const today = $.datepicker.formatDate("yy-mm-dd", new Date());

    // Initialize the datepicker
    $("#datepicker").datepicker({
        firstDay: 1,
        dateFormat: "yy-mm-dd",
        beforeShowDay: function (date) {
            const formattedDate = $.datepicker.formatDate("yy-mm-dd", date);
        
            // Check if the date is today and in the highlightedDates array
            const isToday = formattedDate === today;
            const isHighlighted = highlightedDates.includes(formattedDate);

            if (isToday && isHighlighted) {
                return [true, `highlighted-${deliShip}-today`, "Dostawa dziś"]; // Add a new combined class
            }
        
            if (isToday) {
                return [true, "highlighted-today", "Dziś"]; // Highlight today
            }
        
            if (isHighlighted) {
                return [true, `highlighted-${deliShip}`, "Wysyłka"]; // Highlight specific dates
            }
        
            return [true, "regular-date", ""];
        },
        onSelect: function (selectedDate) {
            // Hide all `.tile-container` elements
            $(".tile-container").hide();
            $(".logo").removeClass("highlighted inactive");

            // Find the element with the matching `data-date` and make it visible
            const matchingTile = $(`.tile-container[data-date='${selectedDate}']`);
            
            // Get the data-client values from matching tile containers
            const clientData = matchingTile.map(function() {
                return $(this).data('client'); // Get the 'data-client' attribute value
            }).get(); // Convert jQuery object to an array of 'data-client' values

            // Loop through each logo in the logo-bar
            $(".logo-track .logo").each(function() {
                const logoClient = $(this).data('client'); // Get the 'data-client' value for each logo

                // Check if the logo's client matches any of the clientData values
                if (clientData.includes(logoClient)) {
                    // Change the styling of the matching logo
                    $(this).css({
                        "opacity": "1",  // Highlight matching logo (full opacity)
                        "filter": "none"
                    });
                } else {
                    $(this).css({
                        "opacity": "0.3",
                        "filter": "grayscale(100%)"
                    });
                }
            });
        },
        monthNames: [ "Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień" ],
        dayNamesMin: [ "nd", "pn", "wt", "śr", "cz", "pt", "sb" ],
        showAnim: 'slideDown',
    });

    $(".logo").on("click", function() {
        const logoClient = $(this).data('client'); // Get the data-client attribute of the clicked logo

        // Get the selected date from the datepicker
        const selectedDate = $("#datepicker").datepicker("getDate");
        if (selectedDate) {
            // Format the selected date to match the format used in the data-date attribute (e.g., 'yy-mm-dd')
            const formattedDate = $.datepicker.formatDate("yy-mm-dd", selectedDate);

            // Find the tile-container elements that match both data-client and data-date
            const matchingTile = $(`.tile-container[data-client='${logoClient}'][data-date='${formattedDate}']`);

            // Hide all tile-containers (optional: if you want to hide all others first)
            $(".tile-container").hide();

            // Show the matching tile-container(s)
            matchingTile.show();
        }

        $(".logo").removeClass("highlighted inactive");  // Remove the highlight and inactive classes from all logos

        // Add 'highlighted' class to the clicked logo
        $(this).addClass("highlighted");

        // Optionally, add 'inactive' class to logos that are not selected
        $(".logo").not(this).addClass("inactive");
    });
}

function stockHandler() {
    function updateTop() {
        var sourceHeight = $('.sticky-bar').outerHeight();
        $('#stickyTableHeader').css('top', sourceHeight - 0.5 + 'px');
    }
  
    updateTop();
  
    $(window).resize(function() {
        updateTop();
    });
}

function barcodesSocketHandler(deliShip) {
    let retryInterval = 1000; // Start with 1 second
    const maxInterval = 30000; // Maximum delay between retries (30 seconds)
    let warehouseStrings = [];
    if (deliShip == 'delivery') {
        warehouseStrings = ["Magazyn przyjęcie (Kolejowa)"] 
    } else {
        warehouseStrings = ["Magazyn wysyłkowy (Kolejowa)", "Magazyn (Kolejowa) Bruss WZ"]
    }
    function connectWebSocket() {
        // Initialize the WebSocket
        const socket = new WebSocket('ws://' + window.location.host + '/ws/scanned-codes/');

        socket.onmessage = function(e) {
            const barcode = JSON.parse(e.data);
            
            if (warehouseStrings.every(str => barcode.line.includes(str))) {
                let tileToRemove = $('.bottom-middle').filter(function() {
                    return barcode.code == $(this).text();
                }).closest('.tile');

                if (barcode.crud_type == 'create') {
                    tileToRemove.each(function() {
                        var tile = $(this);
                        tile.slideUp(5000);
                        console.log(`Zeskanowano ${barcode.code}`);
                        toastr.info(`Zeskanowano ${barcode.code}`);
                    });
                } else if (barcode.crud_type == 'delete') {
                    tileToRemove.each(function() {
                        var tile = $(this);
                        tile.slideDown(5000);
                        console.log(`Cofnięto ${barcode.code}`);
                        toastr.info(`Cofnięto ${barcode.code}`);
                    });
                }
            }
        };
    
        socket.onopen = function(e) {
            toastr.success("Barcodes socket connection established.");
        };

        // Handle errors
        socket.onerror = function(error) {
            console.error("Barcodes socket error:", error);
        };

        // Handle closure and attempt to reconnect
        socket.onclose = function(event) {
            console.log(`Barcodes socket connection closed. Reason: ${event.reason || "Unknown"}`);
            reconnectWebSocket(); // Attempt to reconnect
        };
    }

    function reconnectWebSocket() {
        toastr.info(`Attempting to reconnect in ${retryInterval / 1000} seconds...`);
        setTimeout(() => {
            retryInterval = Math.min(retryInterval * 2, maxInterval); // Exponential backoff
            connectWebSocket(); // Reconnect
        }, retryInterval);
    }

    connectWebSocket();
}