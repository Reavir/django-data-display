function downloadFile(content, fileName) {
    const blob = new Blob([content], { type: "application/octet-stream" });
    const link = document.createElement("a");
    link.href = URL.createObjectURL(blob);
    link.download = fileName;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}

function formHandler() {
    // Validate form
    function hideValidationSpan(inputElement) {
        validationElement = inputElement.next('span');
        validationElement.text('');
        validationElement.hide();
    }

    function showValidationSpan(inputElement, validationMessage) {
        validationElement = inputElement.next('span');
        validationElement.text(validationMessage);
        validationElement.show();
    }

    function validateForm() {
        let invalidInputIDs = [];

        $('input').each(function() {
            let isValid = true;

            let $inputElement = $(this);
            let inputId = $inputElement.attr('id');
            let inputValue = $inputElement.val();
            let inputPad = $inputElement.data('pad');
            let inputRequired = $inputElement.data('required');

            hideValidationSpan($inputElement)

            if (inputRequired && inputValue === '') {
                showValidationSpan($inputElement, 'Field is required.');
                invalidInputIDs.push(inputId);
                isValid = false;
            }
            else if (inputPad) {
                let inputPadValue = parseInt(inputPad); 
                if (inputValue.length > inputPadValue) {
                    showValidationSpan($inputElement, `Field must be no more than ${inputPadValue} characters.`);
                    invalidInputIDs.push(inputId);
                    isValid = false;
                }
            }

            if (!isValid) {
                $inputElement.closest('.vda-row').show();
            }
        });

        if (invalidInputIDs.length === 0) {
            return true;
        }
        
        // Scroll to first invalid field
        const $closestForm = $(`#${invalidInputIDs[0]}`).closest('.vda-form')
        $closestForm.show();
        $(`#${$closestForm.attr('id').slice(0, 8)}__spec-selector`).addClass('active');
        $('body').animate({
            scrollTop: $(`#${invalidInputIDs[0]}`).closest('label').offset().top - 2*$('#submit-form-container').height()
        }, 1000);

        return false
    }

    // Submit form
    $('#submit-form-button').on('click', (e) => {
        e.preventDefault();
        $('[id*="711_07"]').val(dateToday());
        $('[id*="712_06"]').val(dateToday());
        $('[id*="713_04"]').val(dateToday());
        $('[id*="712_07"]').val(timeNow());

        if (validateForm()) {
            $('.vda-form:visible').each(function() {
                let allData = '';
                let fileName = $(this).data('dn');

                $(this).find('.vda-row').each(function () {
                    let rowData = ''
                    $(this).find('.row-input').each(function() {
                        $this = $(this);
                        rowData += formatInput($this);
                    })
                    // Accumulate rowData to allData
                    if (allData) {
                        allData += '\n';
                    }
                    allData += rowData;
                });
                console.log(allData)
                downloadFile(allData, fileName);
            });
        }
    });

    $('#id_selected_date').on('change', function() {
        $('#date-select-form').submit();
    });
}       

function uiHandler() {
    function createValidationSpans() {
        $('.vda-row input:not([hidden])').each(function() {
            let inputId = $(this).attr('id');
            let validationId = inputId ? `validation-${inputId}` : '';
            $(this).after(`<span class="validation-message" id="${validationId}"></span>`);
        });
    }

    createValidationSpans();

    $('h2').on('click', function() {
        $(this).next('div').toggle();
        $(this).toggleClass('active');
    });

    $('h3').on('click', function() {
        $(this).next('div').toggle();
        $(this).toggleClass('active');
    });

    $('.spec-selector').on('click', function() {
        $(this).toggleClass('active');
        const spec = $(this).data('spec');
        $(`#${spec}__vda-form`).toggle();
    });

    $('.vda-row').hide();
}

function formatInput(element) {
    const padAmmount = element.data('pad');
    const inputType = element.attr('type');
    const elementValue = element.val();
    
    if (inputType === 'text') {
        return elementValue.padEnd(padAmmount, ' ');
    }
    
    else if (inputType === 'number') {
        return elementValue.padStart(padAmmount, '0');
    }
    
    else if (inputType === 'date') {
        if (elementValue === ''){
            return "000000";
        }
        return elementValue.replace(/-/g, '').substring(2);
    }

    else if (inputType === 'time') {
        if (elementValue === ''){
            return "0000";
        }
        return elementValue.replace(/:/g, '');
    }

    else {
        return elementValue;
    }

}

function dateToday() {
    const today = new Date();
    const formattedDate = today.toISOString().split('T')[0];

    return formattedDate;
}

function timeNow() {
    const today = new Date();
    const formattedTime = today.toTimeString().split(' ')[0].substring(0, 5);
    return formattedTime;
}

$(document).ready(() => {
    uiHandler();
    formHandler();
});