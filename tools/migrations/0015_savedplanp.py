# Generated by Django 4.2.3 on 2024-06-12 12:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tools', '0014_barcodes_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='SavedPlanP',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('plan_json', models.JSONField()),
                ('timestamp', models.DateTimeField()),
            ],
        ),
    ]
