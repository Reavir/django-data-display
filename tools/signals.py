from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from .models import Barcodes, SavedPlanKTL, SavedPlanP
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer

@receiver(post_save, sender=Barcodes)
def barcode_saved(sender, instance, created, **kwargs):
    channel_layer = get_channel_layer()

    # Check if timestamp is already a string (and format only if it's a datetime object)
    timestamp = instance.timestamp
    if isinstance(timestamp, str):
        # If it's already a string, just use it as is
        timestamp = timestamp
    else:
        # If it's a datetime object, format it
        timestamp = timestamp.strftime("%Y-%m-%d %H:%M:%S")

    operation_type = "create" if created else "update"

    async_to_sync(channel_layer.group_send)(
        "scanned_codes",
        {
            'type': 'send_scanned_code',
            'crud_type': operation_type,  # Include the operation type
            'code': instance.code,
            'timestamp': timestamp,
            'line': instance.line,
            'name': instance.name,
            'additional_info': instance.additional_info if instance.additional_info is not None else '',
            'used': instance.used
        }
    )

@receiver(post_delete, sender=Barcodes)
def barcode_deleted(sender, instance, **kwargs):
    channel_layer = get_channel_layer()

    # Check if timestamp is already a string (and format only if it's a datetime object)
    timestamp = instance.timestamp
    if isinstance(timestamp, str):
        # If it's already a string, just use it as is
        timestamp = timestamp
    else:
        # If it's a datetime object, format it
        timestamp = timestamp.strftime("%Y-%m-%d %H:%M:%S")

    async_to_sync(channel_layer.group_send)(
        "scanned_codes",
        {
            'type': 'send_scanned_code',
            'crud_type': 'delete',  # Indicate a delete operation
            'code': instance.code,
            'timestamp': timestamp,
            'line': instance.line,
            'name': instance.name,
            'additional_info': instance.additional_info if instance.additional_info is not None else '',
            'used': instance.used
        }
    )

@receiver(post_save, sender=SavedPlanKTL)
def saved_plan_ktl(sender, instance, created, **kwargs):
    """
    Trigger when a new SavedPlanKTL is added or updated.
    """
    channel_layer = get_channel_layer()
    
    # Determine if it's a new or updated plan
    if created:
        action = 'add'
    else:
        action = 'update'
    
    async_to_sync(channel_layer.group_send)(
        "plan_ktl_updates",  # WebSocket group name
        {
            'type': 'send_saved_plan_ktl',
            'action': action,  # 'add' or 'update'

        }
    )

@receiver(post_delete, sender=SavedPlanKTL)
def deleted_plan_ktl(sender, instance, **kwargs):
    """
    Trigger when a SavedPlanKTL instance is deleted.
    """
    channel_layer = get_channel_layer()

    # Notify the frontend that a plan was deleted
    async_to_sync(channel_layer.group_send)(
        "plan_ktl_updates",  # WebSocket group name
        {
            'type': 'send_deleted_plan_ktl',
            'action': 'delete',  # Indicating a delete action
        }
    )

@receiver(post_save, sender=SavedPlanP)
def saved_plan_p(sender, instance, created, **kwargs):
    """
    Trigger when a new SavedPlanP is added or updated.
    """
    channel_layer = get_channel_layer()
    
    # Determine if it's a new or updated plan
    if created:
        action = 'add'
    else:
        action = 'update'
    
    async_to_sync(channel_layer.group_send)(
        "plan_p_updates",  # WebSocket group name
        {
            'type': 'send_saved_plan_p',
            'action': action,  # 'add' or 'update'

        }
    )

@receiver(post_delete, sender=SavedPlanP)
def deleted_plan_p(sender, instance, **kwargs):
    """
    Trigger when a SavedPlanP instance is deleted.
    """
    channel_layer = get_channel_layer()

    # Notify the frontend that a plan was deleted
    async_to_sync(channel_layer.group_send)(
        "plan_p_updates",  # WebSocket group name
        {
            'type': 'send_deleted_plan_p',
            'action': 'delete',  # Indicating a delete action
        }
    )

