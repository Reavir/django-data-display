from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Count, Max, Min, IntegerField, Q, Sum, Subquery, CharField, F, Value, Func
from django.db.models.functions import Cast, Substr, Length
from analysis.models import BrussIdent, MubeaIdent, FaureciaIdent, BrussOrders, MetalMartinIdent, MetalMartinOrders, FaureciaOrders, MubeaOrders, MubeaStock, MubeaShipment, SiegeniaOrders
from tools.forms import *
from tools.models import PrasyLabel, Hangers, SavedPlan, Barcodes, BrussExport, SavedPlanP, SavedPlanKTL
from utils.calculations import start_end_day_cw, count_working_days, convert_to_datetime
from utils.plan_ni_cr_cal import make_plan as make_plan_ni_cr
from utils.plan_p_cal import make_plan as make_plan_p, mm_query as plan_p_mm_query, b_query as plan_p_b_query, get_data as plan_p_get_data
from utils.plan_ktl_cal import make_plan as make_plan_ktl, mm_query as plan_ktl_mm_query, b_query as plan_ktl_b_query, get_data as plan_ktl_get_data, ph_query as ph_ready 
from utils.printer import check_printer_ready
from utils.get_data import get_add_bcode
from utils.mail_handler import send_email
from django.db import transaction
from django.http.response import Http404
from django.http import HttpResponse
from django.template import loader
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from collections import defaultdict

import datetime
import json
import logging
import os
import pandas as pd
import re
import sqlite3
import traceback


logger = logging.getLogger(__name__)


CLIENT_CODE_CONDITIONS = {
    'Bruss': Q(code__startswith='KES') | Q(code__regex=r'^\d{5}$') | Q(code__startswith='000B') | Q(code__startswith='BRS') | Q(code__startswith='MRG') | Q(code__startswith='GTM'),
    'Mubea': Q(code__startswith='MUB') | Q(code__regex=r'^\d{6}$'),
    'Siegenia': Q(code__startswith='SGN'),
    'Faurecia': Q(code__startswith='FRC'),
    'Teknia': Q(code__startswith='TKN'),
    'Martin Metal': Q(code__startswith='MET'),
    'GTM': Q(code__regex=r'^\d{5}-\d+$')
}


def press_label(request):
    try:
        idents_with_orders_prasy = (
            BrussIdent.objects
            .filter(supplier='Prasy')
            .annotate(order_count=Count('brussorders'))
            .order_by('-order_count')
        )
        order_id = []
        order_name = []
        if request.method == 'POST':
            form = OrderForm(request.POST, order_choices=idents_with_orders_prasy)
            if form.is_valid():
                data = form.cleaned_data
                for order, qty in zip(data.keys(), data.values()):
                    if qty is not None and qty != 0:
                        for n in range(1, qty+1):
                            number = order[0:5]
                            name = order[6:]
                            for check in [r'/ PH', r'/PH']:
                                if check in name:
                                    name = name.replace(check, "")
                                    
                            new_object = PrasyLabel(number=number, description=name)
                            new_object.save()
                            new_object_id = new_object.id

                            order_id.append(f'{number}-{new_object_id}')
                            order_name.append(name)

                context = {
                    'form': form,
                    'order_id': order_id,
                    'order_name': order_name,
                }
                return render(request, 'label_print.html', context)
        else:
            form = OrderForm(order_choices=idents_with_orders_prasy)

        context = {
            'form': form,
            'printerStatus': check_printer_ready(),
            'form_success': 0,
        }
        return render(request, 'press_label.html', context)
    
    except Exception as e:
        import traceback
        error_message = f"An error occurred: {str(e)}\n{traceback.format_exc()}"

        return render(request, 'error.html', {'error_message': error_message})


@csrf_exempt
def check_printer_status(request):  
    try:
        if request.method == 'GET':
            context = {
                'printerStatus': check_printer_ready(),
            }
            return JsonResponse(context)

        else:
            return JsonResponse({'status': 'failure'})

    except Exception as e:
        return JsonResponse({'status': 'failure'})


@csrf_exempt
def handle_pdf(request):
    import win32api
    import os
    
    if request.method == 'POST':
        pdf_file = request.FILES.get('file')
        source = request.POST.get('source')
        temp_file_path = os.path.join('tools\static', pdf_file.name)

        with open(temp_file_path, 'wb+') as temp_file:
            for chunk in pdf_file.chunks():
                temp_file.write(chunk)

        # ShellExecute with the file path  
         
        win32api.ShellExecute(
            0,
            "printto",
            temp_file_path,
            '"%s"' % "Xprinter XP-420B",
            ".",
            0
        )
        
        if source == 'rework':
            BrussOrders.objects.filter(orderId__contains="p").delete()


    return JsonResponse({'message': 'PDF blob received successfully'})

    
def plan_menu(request):
    try :
        return render(request, 'plan_menu.html')
    
    except Exception as e:
        import traceback
        error_message = f"An error occurred: {str(e)}\n{traceback.format_exc()}"
    
        return render(request, 'error.html', {'error_message': error_message})
    

def plan_ni_cr(request):
    try:
        if request.method == 'POST':
            plan_form = PlanForm(request.POST)
            saved_plan_form = SavedPlanForm(request.POST)
            if plan_form.is_valid():
                date_start_str = plan_form.cleaned_data['start_date']
                date_end_str = plan_form.cleaned_data['end_date']
                substract_deliveries = plan_form.cleaned_data['deliveries']
                substract_production = plan_form.cleaned_data['production'] if not substract_deliveries else False
                prio_id = plan_form.cleaned_data['prio_id']
                excl_id = plan_form.cleaned_data['excl_id']
                hangers_obj = get_object_or_404(Hangers, pk=1)

                hangers_obj.line1=plan_form.cleaned_data['line1']
                hangers_obj.line2=plan_form.cleaned_data['line2']


                hangers_obj.save()

            elif saved_plan_form.is_valid():
                plan_form = PlanForm()
                plan_obj = saved_plan_form.cleaned_data['selected_timestamp']
                context = {
                    'plan_form': plan_form,
                    'saved_plan_form': saved_plan_form,
                    'n_days': plan_obj.number_days,
                    'time_now': plan_obj.timestamp,
                    'days': plan_obj.header,
                    'i_dict': plan_obj.rows_dict,
                    'additional_data': plan_obj.additional_data
                }
                return render(request, 'plan_ni_cr.html', context)
        
        else:
            plan_form = PlanForm()
            saved_plan_form = SavedPlanForm()
            date_start_str, date_end_str = start_end_day_cw(datetime.date.today().isocalendar()[1])
            substract_deliveries = False
            substract_production = False
            prio_id = ''
            excl_id = ''
        
        day_list, ident_dict, number_of_days, elapsed_time = make_plan_ni_cr(
            date_start=date_start_str,
            date_end=date_end_str,
            sub_deli=substract_deliveries,
            sub_prod=substract_production,
            prio=prio_id,
            excl=excl_id
        )
        select_list = [str(x.number) for x in MubeaIdent.objects.all()] + [str(x.number) for x in FaureciaIdent.objects.all()]
        select2_dict = [{'id':suggestion, 'text': suggestion} for suggestion in select_list]
        created_time = datetime.datetime.now()
        context = {
            'plan_form': plan_form,
            'saved_plan_form': saved_plan_form,
            'n_days': number_of_days + 2,
            'time_now': created_time,
            'days': day_list,
            'i_dict': ident_dict,
            'select2_data': select2_dict,
            'additional_data': {'additional_info': f'Elapsed time: {elapsed_time:.4f} Substract deliveries {substract_deliveries} Substract production {substract_production}'},
        }

        return render(request, 'plan_ni_cr.html', context)
    
    except Exception as e:
        import traceback
        error_message = f"An error occurred: {str(e)}\n{traceback.format_exc()}"

        return render(request, 'error.html', {'error_message': error_message})


@csrf_exempt
def save_plan(request):
    if request.method == 'POST':
        try:
            data = json.loads(request.body)
            SavedPlan.objects.create(
                number_days=data['number_days'],
                timestamp=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                header=data['header'],
                rows_dict={key: value for d in data['dict'] for key, value in d.items()},
                additional_data={'additional_info': ''},
            )
            response_data = {'message': 'Plan saved successfully'}
            return JsonResponse(response_data, status=200)
        
        except json.JSONDecodeError:
            response_data = {'error': 'Invalid JSON format'}
            return JsonResponse(response_data, status=400)
    else:
        response_data = {'error': 'Invalid request method'}
        return JsonResponse(response_data, status=405)
 

def plan_ni_cr_production(request):
    try:
        if request.method == 'GET':
            plan_obj = SavedPlan.objects.last()

            indexes_to_remove = [-1,-2,-3,-5]
            plan_obj.header.pop(1)

            # Convert negative indexes to positive indexes
            indexes_to_remove = [index if index >= 0 else len(plan_obj.header) + index for index in indexes_to_remove]

            start_timestamp = convert_to_datetime(plan_obj.header[1], True)
            end_timestamp = datetime.datetime.now()
            end_timestamp = datetime.datetime.combine(end_timestamp, datetime.time.max)

            plan_obj.header = [item for index, item in enumerate(plan_obj.header) if index not in indexes_to_remove]

            codes_mub = Barcodes.objects.annotate(
                                        text_len=Length('code')
                                    ).filter(
                                        Q(timestamp__range=(start_timestamp, end_timestamp)) &
                                        Q(line__startswith="Chromoniklowanie") &
                                        (Q(code__startswith='MUB') | Q(text_len=6))
                                    ).values_list('code', flat=True)

            codes_frc = Barcodes.objects.filter(
                                        Q(timestamp__range=(start_timestamp, end_timestamp)) &
                                        Q(line__startswith="Chromoniklowanie") &
                                        (Q(code__startswith='FRC') | Q(code__startswith='TKN'))
                                    ).values_list('code', flat=True)
                                    
            orders_mub = MubeaOrders.objects.filter(orderId__in=codes_mub).values('quantity', 'ident__number')
            orders_frc = FaureciaOrders.objects.filter(orderId__in=codes_frc).values('quantity', 'ident__number')

            # Create a dictionary to store the summed quantities for each ident__number
            orders_dict = defaultdict(int)

            # Sum quantities for Mubea orders
            for order in orders_mub:
                ident_number = str(order['ident__number'])
                quantity = order['quantity']
                orders_dict[ident_number] = orders_dict.get(ident_number, 0) + quantity

            # Sum quantities for Faurecia orders
            for order in orders_frc:
                ident_number = str(order['ident__number'])
                quantity = order['quantity']
                orders_dict[ident_number] = orders_dict.get(ident_number, 0) + quantity
            
            mubea_stock_query = MubeaIdent.objects.filter(
            mubeastock__date=MubeaStock.objects.aggregate(latest_date=Max('date'))['latest_date']
                ).annotate(
                    stock_sum=Sum('mubeastock__quantity'),
                )
            stock_dict = {item.number: item.stock_sum for item in mubea_stock_query}

            for ident, values in zip(plan_obj.rows_dict.keys(), plan_obj.rows_dict.values()):
                values.pop(1)
                
                values = [item for index, item in enumerate(values) if index not in indexes_to_remove]
                if '\n' in ident:
                    ident_number = ident.split('\n')[0]
                    values[-4] = stock_dict.get(int(ident_number), 0)
                    
                    if ident_number in orders_dict:
                        substract_value = orders_dict[ident_number]
                        values[-2] = substract_value
                        values[-4] = stock_dict.get(int(ident_number), 0)

                        
                        for i in range(1, plan_obj.number_days -1):
                            if values[i] != '':
                                old_val = int(values[i])
                                val_to_rplc = int(values[i]) - substract_value

                                if val_to_rplc < 0:
                                    values[i] = ''
                                    substract_value -= old_val

                                elif val_to_rplc > 0:
                                    values[i] = val_to_rplc
                                    break

                                elif val_to_rplc == 0:
                                    values[i] = ''
                                    break
                        
                        suma = 0
                        for i in range(1, plan_obj.number_days -1):
                            if values[i] != '':
                                suma += int(values[i])

                        values[-5] = suma

                plan_obj.rows_dict[ident] = values

            formatted_time = plan_obj.timestamp.strftime('%d.%m.%Y')

            context = {
                'n_days': plan_obj.number_days,
                'time_now': formatted_time,
                'days': plan_obj.header,
                'i_dict': plan_obj.rows_dict,
                'last_update': datetime.datetime.now().strftime("%X"),
            }
            
            return render(request, 'plan_ni_cr_production.html', context)
        
        return render(request, 'error.html', {'error_message': 'Not a GET request.'})
    
    except Exception as e:
        import traceback
        error_message = f"An error occurred: {str(e)}\n{traceback.format_exc()}"

        return render(request, 'error.html', {'error_message': error_message})


def plan_p(request):
    try:
        if request.method == 'POST':
            saved_plan_form = SavedPlanPForm(request.POST)
            if saved_plan_form.is_valid():
                plan_label = saved_plan_form.cleaned_data['selected_timestamp']
                
                plan_obj = SavedPlanP.objects.filter(
                    id=plan_label,
                ).last()

                new_plan_data = {}

                for day_str, prios in plan_obj.plan_json.items():
                    day = datetime.datetime.strptime(day_str, '%d-%m-%Y')
                    new_plan_data[day] = prios

                order_ids_in_new_plan = set()

                for day, prios in new_plan_data.items():
                    for prio, orders in prios.items():
                        for order in orders:
                            order_ids_in_new_plan.add(order['orderId'])
                            try:
                                order['dateDeadline'] = datetime.datetime.strptime(order['dateDeadline'], '%d.%m.%Y')
                            except:
                                order['dateDeadline'] = datetime.datetime.strptime(order['dateDeadline'], '%Y-%m-%d')

                            try:
                                order['dateDelivered'] = datetime.datetime.strptime(order['dateDelivered'], '%d.%m.%Y')
                            except:
                                order['dateDelivered'] = datetime.datetime.strptime(order['dateDelivered'], '%Y-%m-%d')

                not_added_orders = plan_p_get_data()
                not_added_orders = [entry for entry in not_added_orders if entry['orderId'] not in order_ids_in_new_plan]

                bruss_idnets_hangers = list(BrussIdent.objects.exclude(description__isnull=True).values('n_exits_ph', 'description', 'number'))
                mm_idents_hangers = list(MetalMartinIdent.objects.exclude(description__isnull=True).values('n_exits_ph', 'description', 'number'))

                ident_list = bruss_idnets_hangers + mm_idents_hangers

                ident_list.append({
                    'n_exits_ph': 800, 'description': 'KSB,AU436 Front LH', 'number': 91152067
                })
                ident_list.append({
                    'n_exits_ph': 800, 'description': 'KSB,AU436 Front RH', 'number': 91152069 
                })

                context = {
                    'saved_plan_form': saved_plan_form,
                    'orders_by_day_and_prios': new_plan_data,
                    'plan_obj': plan_obj,
                    'not_added_orders': not_added_orders,
                    'ident_list': ident_list,
                    'bruss_list_len': len(bruss_idnets_hangers)
                }

                return render(request, 'plan_p.html', context)

        saved_plan_form = SavedPlanPForm()
        bruss_idnets_hangers = list(BrussIdent.objects.exclude(description__isnull=True).values('n_exits_ph', 'description', 'number'))
        mm_idents_hangers = list(MetalMartinIdent.objects.exclude(description__isnull=True).values('n_exits_ph', 'description', 'number'))

        ident_list = bruss_idnets_hangers + mm_idents_hangers
        ident_list.append({
            'n_exits_ph': 800, 'description': 'KSB,AU436 Front LH', 'number': 91152067
        })
        ident_list.append({
            'n_exits_ph': 800, 'description': 'KSB,AU436 Front RH', 'number': 91152069 
        })
        context = {
            'saved_plan_form': saved_plan_form,
            'ident_list': ident_list,
            'bruss_list_len': len(bruss_idnets_hangers)
        }

        return render(request, 'plan_p.html', context)
    
    except Exception as e:
        import traceback
        error_message = f"An error occurred: {str(e)}\n{traceback.format_exc()}"

        return render(request, 'error.html', {'error_message': error_message})


def plan_p_production(request):
    try:
        plan_obj = SavedPlanP.objects.last()

        new_plan_data = {}
        for day_str, prios in plan_obj.plan_json.items():
            day = datetime.datetime.strptime(day_str, '%d-%m-%Y')
            new_plan_data[day] = prios

        for day, prios in new_plan_data.items():
            for prio, orders in prios.items():
                for order in orders:
                    try:
                        order['dateDeadline'] = datetime.datetime.strptime(order['dateDeadline'], '%d.%m.%Y')
                    except:
                        order['dateDeadline'] = datetime.datetime.strptime(order['dateDeadline'], '%Y-%m-%d')

                    try:
                        order['dateDelivered'] = datetime.datetime.strptime(order['dateDelivered'], '%d.%m.%Y')
                    except:
                        order['dateDelivered'] = datetime.datetime.strptime(order['dateDelivered'], '%Y-%m-%d')

        context = {
            'timestamp': plan_obj.timestamp,
            'orders_by_day_and_prios': new_plan_data,
        }   

        return render(request, 'plan_p_production.html', context)
    
    except Exception as e:
        import traceback
        error_message = f"An error occurred: {str(e)}\n{traceback.format_exc()}"

        return render(request, 'error.html', {'error_message': error_message})


def plan_p_api(request):
    if request.method == 'POST':
        try:
            action = request.POST.get('action', '')
            if action == 'delete_used':
                codes_json = request.POST.get('codes', '[]')  # Default to empty list
                codes = json.loads(codes_json)  # Convert JSON string to Python lis

                for code in codes:
                    if "MET" in code:
                        MetalMartinOrders.objects.filter(orderId=code).update(use_in_plan_p=False)
                    else:
                        BrussOrders.objects.filter(orderId=code[3:]).update(use_in_plan_p=False)

                response_data = {'message': f'Orders {codes} deleted from phosphorus planning.'}
                logger.info(f"[{datetime.datetime.now()}] Orders {codes} deleted from phosphorus planning.")

                return JsonResponse(response_data, status=200)
            
            elif action == 'save':
                data = json.loads(request.POST.get('plan', '{}'))

                SavedPlanP.objects.create(
                    timestamp=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                    plan_json=data
                )
                response_data = {'message': 'Plan saved successfully'}
                return JsonResponse(response_data, status=200)
                
            elif action == 'delete':
                plan_id = request.POST.get('plan_obj_id', '')
                SavedPlanP.objects.filter(
                    id=int(plan_id)
                ).first().delete()
                response_data = {'message': 'Plan deleted successfully'}
                return JsonResponse(response_data, status=200)

            elif action == 'create':
                plan_json = make_plan_p()
                new_plan_data = {}

                for day_str, prios in plan_json.items():
                    day = datetime.datetime.strftime(day_str, '%d-%m-%Y')
                    new_plan_data[day] = prios

                for day, prios in new_plan_data.items():
                    for prio, orders in prios.items():
                        for order in orders:
                            try:
                                order['dateDeadline'] = datetime.datetime.strftime(order['dateDeadline'], '%d.%m.%Y')
                                order['dateDelivered'] = datetime.datetime.strftime(order['dateDelivered'], '%d.%m.%Y')
                            except:
                                order['dateDeadline'] = datetime.datetime.strptime(order['dateDeadline'], '%Y-%m-%d')
                                order['dateDelivered'] = datetime.datetime.strptime(order['dateDelivered'], '%Y-%m-%d')
                            
                plan_obj = SavedPlanP.objects.create(
                    timestamp=datetime.datetime.now().replace(microsecond=0),
                    plan_json=new_plan_data,
                )

                response_data = {
                    'message': 'Plan created successfully',
                    'plan_id': plan_obj.id,
                    'plan_timestamp': plan_obj.timestamp,
                }
                return JsonResponse(response_data, status=200)

            else:
                response_data = {'error': 'Invalid request action'}
                return JsonResponse(response_data, status=405)
            
        except json.JSONDecodeError:
            response_data = {'error': 'Invalid JSON format'}
            return JsonResponse(response_data, status=400)
    
    else:
        response_data = {'error': 'Invalid request method'}
        return JsonResponse(response_data, status=405)


def plan_p_get_codes(request):
    if request.method == 'POST':
        old_codes = json.loads(request.POST.get('codes', '{}'))
     
        bruss_orders = plan_p_b_query(n_days=30).values_list('orderId', flat=True)
        mm_orders = plan_p_mm_query(n_days=30).values_list('orderId', flat=True)

        new_codes = list(bruss_orders) + list(mm_orders)
        
        old_codes = set(old_codes)
        new_codes = set(new_codes)

        codes_to_remove = list(Barcodes.objects.filter(
            line__in=['Fosforanowanie', 'Magazyn wysyłkowy (Kolejowa)', 'Magazyn (Kolejowa) Bruss WZ'],
            timestamp__gte=datetime.datetime.now() - datetime.timedelta(days=30),
        ).values_list('code', flat=True))

        b_used = list(BrussOrders.objects.filter(
            use_in_plan_p=False
        ).values_list('orderId', flat=True))
        b_used = ["000" + code if code[0] == "B" else "KES" + code for code in b_used]

        mm_used = list(MetalMartinOrders.objects.filter(
            use_in_plan_p=False
        ).values_list('orderId', flat=True))

        codes_to_remove += b_used + mm_used

        codes_to_add =  list(new_codes - old_codes)

        oldPlanTS = request.POST.get('oldPlanTS', 0)

        if oldPlanTS != 0:
            newestPlanP = SavedPlanP.objects.last().timestamp
            oldPlanTS = datetime.datetime.strptime(oldPlanTS, '%Y-%m-%d %H:%M:%S')
            if newestPlanP > oldPlanTS:
                oldPlanTS = 1

        context = {
            'status': 'success',
            'newPlan': oldPlanTS,
            'codesToRemove': codes_to_remove,
            'codesToAdd': codes_to_add
        }
    
    return JsonResponse(context)


def plan_ktl(request):
    try:
        if request.method == 'POST':
            saved_plan_form = SavedPlanKTLForm(request.POST)
            if saved_plan_form.is_valid():
                plan_label = saved_plan_form.cleaned_data['selected_timestamp']

                plan_obj = SavedPlanKTL.objects.filter(
                    id=plan_label,
                ).last()

                new_plan_data = {}

                for day_str, prios in plan_obj.plan_json.items():
                    day = datetime.datetime.strptime(day_str, '%d-%m-%Y')
                    new_plan_data[day] = prios

                order_ids_in_new_plan = set()

                for day, prios in new_plan_data.items():
                    for prio, orders in prios.items():
                        for order in orders:
                            order_ids_in_new_plan.add(order['orderId'])
                            try:
                                order['dateDeadline'] = datetime.datetime.strptime(order['dateDeadline'], '%d.%m.%Y')
                            except:
                                order['dateDeadline'] = datetime.datetime.strptime(order['dateDeadline'], '%Y-%m-%d')

                            try:
                                order['dateDelivered'] = datetime.datetime.strptime(order['dateDelivered'], '%d.%m.%Y')
                            except:
                                order['dateDelivered'] = datetime.datetime.strptime(order['dateDelivered'], '%Y-%m-%d')

                not_added_orders = plan_ktl_get_data()
                not_added_orders = [entry for entry in not_added_orders if entry['orderId'] not in order_ids_in_new_plan]

                bruss_idnets_hangers = list(BrussIdent.objects.exclude(description__isnull=True).values('n_exits_ktl', 'description', 'number'))
                mm_idents_hangers = list(MetalMartinIdent.objects.exclude(description__isnull=True).values('n_exits_ktl', 'description', 'number'))

                ident_list = bruss_idnets_hangers + mm_idents_hangers
                ident_list.append({
                    'n_exits_ktl': 400, 'description': 'KSB,AU436 Front LH', 'number': 91152067
                })
                ident_list.append({
                    'n_exits_ktl': 400, 'description': 'KSB,AU436 Front RH', 'number': 91152069 
                })

                context = {
                    'saved_plan_form': saved_plan_form,
                    'orders_by_day_and_prios': new_plan_data,
                    'plan_obj': plan_obj,
                    'not_added_orders': not_added_orders,
                    'ident_list': ident_list,
                    'bruss_list_len': len(bruss_idnets_hangers)
                }
                
                return render(request, 'plan_ktl.html', context)

        saved_plan_form = SavedPlanKTLForm()
        bruss_idnets_hangers = list(BrussIdent.objects.exclude(description__isnull=True).values('n_exits_ph', 'description', 'number'))
        mm_idents_hangers = list(MetalMartinIdent.objects.exclude(description__isnull=True).values('n_exits_ph', 'description', 'number'))

        ident_list = bruss_idnets_hangers + mm_idents_hangers
        ident_list.append({
                    'n_exits_ktl': 400, 'description': 'KSB,AU436 Front LH', 'number': 91152067
        })
        ident_list.append({
            'n_exits_ktl': 400, 'description': 'KSB,AU436 Front RH', 'number': 91152069 
        })
        
        context = {
            'saved_plan_form': saved_plan_form,
            'ident_list': ident_list,
            'bruss_list_len': len(bruss_idnets_hangers)
        }

        return render(request, 'plan_ktl.html', context)
    
    except Exception as e:
        import traceback
        error_message = f"An error occurred: {str(e)}\n{traceback.format_exc()}"

        return render(request, 'error.html', {'error_message': error_message})


def plan_ktl_production(request):
    try:
        plan_obj = SavedPlanKTL.objects.last()

        new_plan_data = {}
        for day_str, prios in plan_obj.plan_json.items():
            day = datetime.datetime.strptime(day_str, '%d-%m-%Y')
            new_plan_data[day] = prios

        for day, prios in new_plan_data.items():
            for prio, orders in prios.items():
                for order in orders:
                    try:
                        order['dateDeadline'] = datetime.datetime.strptime(order['dateDeadline'], '%d.%m.%Y')
                    except:
                        order['dateDeadline'] = datetime.datetime.strptime(order['dateDeadline'], '%Y-%m-%d')

                    try:
                        order['dateDelivered'] = datetime.datetime.strptime(order['dateDelivered'], '%d.%m.%Y')
                    except:
                        order['dateDelivered'] = datetime.datetime.strptime(order['dateDelivered'], '%Y-%m-%d')

        context = {
            'timestamp': plan_obj.timestamp,
            'orders_by_day_and_prios': new_plan_data,
        }   

        return render(request, 'plan_ktl_production.html', context)
    
    except Exception as e:
        import traceback
        error_message = f"An error occurred: {str(e)}\n{traceback.format_exc()}"

        return render(request, 'error.html', {'error_message': error_message})


def plan_ktl_api(request):
    if request.method == 'POST':
        try:
            action = request.POST.get('action', '')
            if action == 'delete_used':
                codes_json = request.POST.get('codes', '[]')  # Default to empty list
                codes = json.loads(codes_json)  # Convert JSON string to Python lis)

                for code in codes:
                    if "MET" in code:
                        MetalMartinOrders.objects.filter(orderId=code).update(use_in_plan_ktl=False)
                    else:
                        BrussOrders.objects.filter(orderId=code[3:]).update(use_in_plan_ktl=False)

                response_data = {'message': f'Orders {codes} deleted from ktl planning.'}
                logger.info(f"[{datetime.datetime.now()}] Orders {codes} deleted from ktl planning.")

                return JsonResponse(response_data, status=200)
        
            elif action == 'save':
                data = json.loads(request.POST.get('plan', '{}'))

                SavedPlanKTL.objects.create(
                    timestamp=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                    plan_json=data
                )
                response_data = {'message': 'Plan saved successfully'}
                return JsonResponse(response_data, status=200)
            
            elif action == 'delete':
                plan_id = request.POST.get('plan_obj_id', '')
                SavedPlanKTL.objects.filter(
                    id=int(plan_id)
                ).first().delete()
                response_data = {'message': 'Plan deleted successfully'}
                return JsonResponse(response_data, status=200)

            elif action == 'create':
                plan_json = make_plan_ktl()
                new_plan_data = {}

                for day_str, prios in plan_json.items():
                    day = datetime.datetime.strftime(day_str, '%d-%m-%Y')
                    new_plan_data[day] = prios

                for day, prios in new_plan_data.items():
                    for prio, orders in prios.items():
                        for order in orders:
                            try:
                                order['dateDeadline'] = datetime.datetime.strftime(order['dateDeadline'], '%d.%m.%Y')
                                order['dateDelivered'] = datetime.datetime.strftime(order['dateDelivered'], '%d.%m.%Y')
                            except:
                                order['dateDeadline'] = datetime.datetime.strptime(order['dateDeadline'], '%Y-%m-%d')
                                order['dateDelivered'] = datetime.datetime.strptime(order['dateDelivered'], '%Y-%m-%d')
                            
                plan_obj = SavedPlanKTL.objects.create(
                    timestamp=datetime.datetime.now().replace(microsecond=0),
                    plan_json=new_plan_data,
                )

                response_data = {
                    'message': 'Plan created successfully',
                    'plan_id': plan_obj.id,
                    'plan_timestamp': plan_obj.timestamp,
                }
                return JsonResponse(response_data, status=200)
            
            else:
                response_data = {'error': 'Invalid action'}
                return JsonResponse(response_data, status=405)
        
        except json.JSONDecodeError:
            response_data = {'error': 'Invalid JSON format'}
            return JsonResponse(response_data, status=400)
    
    else:
        response_data = {'error': 'Invalid request method'}
        return JsonResponse(response_data, status=405)


def plan_ktl_get_codes(request):
    if request.method == 'POST':
        old_codes = json.loads(request.POST.get('codes', '{}'))
     
        bruss_orders = plan_ktl_b_query(n_days=30).values_list('orderId', flat=True)
        mm_orders = plan_ktl_mm_query(n_days=30).values_list('orderId', flat=True)
        ph_codes_ready = list(ph_ready(n_days=30).values_list('code', flat=True))

        new_codes = list(bruss_orders) + list(mm_orders)
        
        old_codes = set(old_codes)
        new_codes = set(new_codes)

        codes_to_remove = list(Barcodes.objects.filter(
            line__in=['Kontrola jakości (KTL)', 'KTL', 'Magazyn wysyłkowy (Kolejowa)', 'Magazyn (Kolejowa) Bruss WZ'],
            timestamp__gte=datetime.datetime.now() - datetime.timedelta(days=30),
        ).values_list('code', flat=True))

        b_used = list(BrussOrders.objects.filter(
            use_in_plan_ktl=False
        ).values_list('orderId', flat=True))
        b_used = ["000" + code if code[0] == "B" else "KES" + code for code in b_used]

        mm_used = list(MetalMartinOrders.objects.filter(
            use_in_plan_ktl=False
        ).values_list('orderId', flat=True))

        codes_to_remove += b_used + mm_used

        codes_to_add = list(new_codes - old_codes)

        oldPlanTS = request.POST.get('oldPlanTS', 0)

        if oldPlanTS != 0:
            newestPlanP = SavedPlanKTL.objects.last().timestamp
            oldPlanTS = datetime.datetime.strptime(oldPlanTS, '%Y-%m-%d %H:%M:%S')
            if newestPlanP > oldPlanTS:
                oldPlanTS = 1

        context = {
            'status': 'success',
            'newPlan': oldPlanTS,
            'codesToRemove': codes_to_remove,
            'codesToAdd': codes_to_add,
            'codesPhReady': ph_codes_ready,
        }
    
    return JsonResponse(context)


@csrf_exempt
def barcode_test(request):
    if request.method == 'POST':
        pass

    context = {

    }

    return render(request, 'barcode_scan_test.html', context)


def search_idents(request):
    query = request.GET.get('q', '')
    items = [str(x.number) for x in MubeaIdent.objects.all()] + [str(x.number) for x in FaureciaIdent.objects.all()]
    suggestions = [item for item in items if query in item]
    data = [{'id':i, 'text': suggestion} for i, suggestion in enumerate(suggestions)]
    return JsonResponse(data, safe=False)


@csrf_exempt
def receive_barcode(request):
    logger.info(f"[{datetime.datetime.now()}] Request received: Method={request.method}, Path={request.path}, Headers={dict(request.headers)}")

    if request.method == 'POST':
        content_type = request.headers.get('Content-Type', '').lower()

        if 'application/json' in content_type:
            try:
                request_data = json.loads(request.body.decode('utf-8'))
                wh_list = request_data.get("jsonData", [])
                now = datetime.datetime.now()
                current_month = now.month
                current_year = now.year

                max_id = BrussExport.objects.filter(
                    timestamp__year=current_year,
                    timestamp__month=current_month
                ).aggregate(Max('sic_id'))['sic_id__max']

                new_sic_id = max_id + 1 if max_id is not None else 1

                BrussExport.objects.create(
                    sic_id=new_sic_id,
                    json_data=wh_list,
                    timestamp=now
                )
                logger.info(f'[{datetime.datetime.now()}] Barcode SUCCESS {new_sic_id} {wh_list} {now}')

                return JsonResponse({'status': 'SUCCESS', 'message': f'WZ 3 {"0" * (3 - len(str(new_sic_id))) + str(new_sic_id)}/{now.strftime("%m/%Y")}'})

            except Exception as e:
                logger.error(f'[{datetime.datetime.now()}] Barcode FAILURE Magazyn (Kolejowa) {e}')
                return JsonResponse({'status': 'FAILURE', 'message': 'Invalid JSON format'}, status=400)
            
        barcode = request.POST.get('barcodeData')
        barcode2 = request.POST.get('barcodeData2') if request.POST.get('barcodeData2') is not None else ''
        barcode3 = request.POST.get('barcodeData3') if request.POST.get('barcodeData3') is not None else ''
        line = request.POST.get('selectedOption')
        timestamp = request.POST.get('timestamp')

        if request.POST.get('timestamp') is not None:
            timestamp = datetime.datetime.strptime(timestamp, '%Y-%m-%d %H:%M:%S')
        else:
            timestamp = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        message = 'Niepoprawny kod'
        try:
            if line == 'Test':
                message = "POST request"
                context = {'status': 'SUCCESS', 'message': message}
                logger.info(f'[{datetime.datetime.now()}] TEST Barcode INFO {message}')

                return JsonResponse(context)
            
            if barcode and line:
                if barcode3 == "Delete":
                    record_to_delete = Barcodes.objects.filter(code=barcode, line=line).last()

                    if record_to_delete:
                        record_to_delete.delete()

                        message = f"Usunięto {barcode} na {line}"
                        context = {'status': 'SUCCESS', 'message': message}
                        logger.info(f'[{datetime.datetime.now()}] Barcode SUCCESS {barcode} {barcode2} {line} {message} {timestamp}')
                        return JsonResponse(context)
                    else:
                        message = f"Nie znaleziono {barcode} na {line}"
                        context = {'status': 'FAILURE', 'message': message}
                        logger.info(f'[{datetime.datetime.now()}] Barcode FAILURE {barcode} {barcode2} {line} {message} {timestamp}')
                        return JsonResponse(context)
                    
                if line == 'Status':
                    existing_record = Barcodes.objects.filter(code=barcode).last()
                    latest_scan = existing_record.line if existing_record else None

                    if (barcode[0:3] == 'KES' and barcode[3:].isdigit() and len(barcode) == 9) or \
                          (barcode[0:6] == 'MRGGTM' and barcode[6:].isdigit()) or \
                           (barcode[0:6] == 'MTGGTM' and barcode[6:].isdigit()) or \
                            (barcode[0:4] == '000B' and barcode[4:].isdigit() and len(barcode) == 9):
                        
                        existing_bruss_order = BrussOrders.objects.filter(orderId=barcode[3:]).last()
                        remaining_quantity = existing_bruss_order.quantity if existing_bruss_order else None
                    
                    elif (barcode[0:3] == 'MET' and barcode[3:].isdigit() and len(barcode) == 9):
                        existing_mm_order = MetalMartinOrders.objects.filter(orderId=barcode).last()
                        remaining_quantity = existing_mm_order.quantity if existing_mm_order else None
                    
                    context = {'latestScan': latest_scan, 'remainingQuantity': f"{remaining_quantity} szt."}
                    logger.info(f'[{datetime.datetime.now()}] Barcode INFO {barcode} {barcode2} {line} {timestamp} {latest_scan} {remaining_quantity}')
                    return JsonResponse(context)

                elif 'Chromoniklowanie' in line:
                    if (barcode.isdigit() and len(barcode) == 6) or \
                          (barcode[0:3] == 'MUB' and barcode[3:].isdigit() and len(barcode) == 9):
                        
                        message = 'Poprawny kod Mubea'

                    elif (barcode.isdigit() and len(barcode) == 6) or \
                          (barcode[0:3] == 'ADI' and barcode[3:].isdigit() and len(barcode) == 9):
                        
                        message = 'Poprawny kod Adient'

                    elif (barcode[0] == 'F' and barcode[1:].isdigit()) or \
                          (barcode[0:3] == "FRC" and barcode[3:].isdigit()):
                        
                        message = 'Poprawny kod Faurecia'
    
                    elif (barcode[0:3] == "TKN" and barcode[3:].isdigit()):
                        
                        message = 'Poprawny kod Teknia'

                elif line == 'Fosforanowanie':
                    if (barcode[0:6].isdigit() and barcode[6] == 'K' and barcode[7].isdigit()):
    
                        message = "Niepoprawny kod\nZeskanuj kod z dołu kartki MRGGTM___"
                
                    elif (barcode[0:3] == 'KES' and barcode[3:].isdigit() and len(barcode) == 9) or (barcode[0:6] == 'MRGGTM' and barcode[6:].isdigit()) or (barcode[0:6] == 'MTGGTM' and barcode[6:].isdigit()) or\
                        (barcode[0:3] == 'BRS' and barcode[3:].isdigit() and len(barcode) == 9) or (barcode[0:4] == '000B' and barcode[4:].isdigit() and len(barcode) == 9):
                        if barcode2 != '':
                            if barcode2[0:5].isdigit() and barcode2[5] == '-' and barcode2[6:].isdigit():

                                message = 'Poprawny kod prasy + Bruss'
                                with transaction.atomic():
                                    existing_record = Barcodes.objects.filter(code=barcode2).exclude(used=True).last()
                                    if existing_record:
                                        existing_record.additional_info = barcode
                                        existing_record.used = True
                                        existing_record.save()
                                        logger.info(f'[{datetime.datetime.now()}] Barcode PRESS {barcode2} updated with {barcode}, {barcode3} {line} {message} {timestamp}')
                                    else:
                                        logger.info(f'[{datetime.datetime.now()}] Barcode PRESS code not fonud WARNING {barcode} {barcode2} {barcode3} {line} {message} {timestamp}')
                            else:

                                message = 'Niepoprawny kod prasy'
                        else:
                            message = 'Poprawny kod Bruss'

                    elif (barcode[0:3] == 'MET' and barcode[3:].isdigit() and len(barcode) == 9):

                        message = 'Poprawny kod Metal Martin'
                    
                    elif (barcode.isdigit() and len(barcode) == 6) or \
                          (barcode[0:3] == 'MUB' and barcode[3:].isdigit() and len(barcode) == 9):
                        
                        message = 'Poprawny kod Mubea'

                    if 'Niepoprawny kod' not in message:
                        with transaction.atomic():
                            suma = int(barcode3) if barcode3 != '' else 0
                            order = MetalMartinOrders.objects.filter(orderId=barcode).last()

                            if not order:
                                order = MubeaOrders.objects.filter(orderId=barcode).last()

                            if not order:
                                order = BrussOrders.objects.filter(orderId=barcode[3:]).last()

                            if not order:
                                #message = "Nie znaleziono kodu w bazie"
                                #context = {'status': 'WARNING', 'message': message}
                                order_qty = 1
                                suma = 0
                                logger.info(f'[{datetime.datetime.now()}] Barcode WARNING {barcode} {barcode2} {barcode3} {line} Nie znaleziono kodu w bazie {timestamp}')
                                #return JsonResponse(context)
                            else:
                                order_qty = order.quantity
                            
                                for i in Barcodes.objects.filter(code=barcode, line=line):
                                    try:
                                        qty = int(i.additional_info.split(',')[1])
                                        suma += qty
                                    except:  
                                        suma += order_qty   
                                        break

                            if order_qty < suma:
                                message = f"Wykorzystano o {suma - order_qty} szt. za dużo"
                                context = {'status': 'WARNING', 'message': message}
                                logger.info(f'[{datetime.datetime.now()}] Barcode WARNING {barcode} {barcode2} {barcode3} {line} {message} {timestamp}')
                                return JsonResponse(context)
                            
                            elif barcode3 == '' and order_qty - suma == 0:
                                message = f"Wykorzystano wszystkie sztuki z tej karty"
                                context = {'status': 'WARNING', 'message': message}
                                logger.info(f'[{datetime.datetime.now()}] Barcode WARNING {barcode} {barcode2} {barcode3} {line} {message} {timestamp}')
                                return JsonResponse(context)

                            else:
                                if barcode3 == '':

                                    qty = order_qty-suma
                                else:
                                    qty = barcode3

                                add_info = f"{barcode2},{qty}"

                                Barcodes.objects.create(
                                    code=barcode,
                                    line=line,
                                    timestamp=timestamp,
                                    name=get_add_bcode(barcode),
                                    additional_info=add_info
                                )

                                context = {'status': 'SUCCESS', 'message': message, 'quantity': qty}
                            
                                logger.info(f'[{datetime.datetime.now()}] Barcode INFO {barcode} {barcode2} {barcode3} {line} {message} {timestamp}')
                                return JsonResponse(context)

                elif line == 'Cynkowanie':
                    if (barcode[0:3] == 'SGN' and barcode[3:].isdigit() and len(barcode) == 9):

                        message = 'Poprwany kod Siegana'

                    elif barcode[0:2] == 'VW' and barcode[2:].isdigit() and len(barcode) == 9:

                        message = 'Poprawny kod Vorwerk'

                    elif barcode[0:3] == 'EGH' and barcode[3:].isdigit() and len(barcode) == 9:

                        message = 'Poprawny kod Grosshaus'

                    elif barcode[0:3] == 'RTA' and barcode[3:].isdigit() and len(barcode) == 9:

                        message = 'Poprawny kod RTA-Kon'

                    elif barcode.isdigit() and len(barcode) == 10:

                        message = "Niepoprawny kod, zeskanuj kod z Karty Kontrolnej:\nSGN 000 000."

                elif line == 'Kontrola jakości' or line == 'Kontrola jakości (NiCr)':
                    line = 'Kontrola jakości (NiCr)'
                    
                    if '/' in barcode2:
                        box = barcode2.split('/')[0]
                        qty = barcode2.split('/')[1]
                    elif '+' in barcode2:
                        box = barcode2.split('+')[0]
                        qty = barcode2.split('+')[1]
                    else:
                        box = barcode2.split(' ')[0]
                        qty = barcode2.split(' ')[1]

                    if (barcode.isdigit() and len(barcode) == 6) or \
                            (barcode[0:3] == 'MUB' and barcode[3:].isdigit() and len(barcode) == 9):
                        
                        message = 'Poprawny kod Mubea'

                    elif (barcode.isdigit() and len(barcode) == 6) or \
                            (barcode[0:3] == 'ADI' and barcode[3:].isdigit() and len(barcode) == 9):
                        
                        message = 'Poprawny kod Adient'

                    elif (barcode[0] == 'F' and barcode[1:].isdigit()) or \
                            (barcode[0:3] == "FRC" and barcode[3:].isdigit()):
                        
                        message = 'Poprawny kod Faurecia'

                    elif (barcode[0:3] == "TKN" and barcode[3:].isdigit()):
                        
                        message = 'Poprawny kod Teknia'

                    if message != 'Niepoprawny kod':
                        with transaction.atomic():
                            Barcodes.objects.create(
                                    code=barcode,
                                    line=line,
                                    timestamp=timestamp,
                                    name=get_add_bcode(barcode),
                                    additional_info=f"{box}, {qty}"
                                )
                            context = {'status': 'SUCCESS', 'message': message}
                            logger.info(f'[{datetime.datetime.now()}] Barcode INFO {barcode} {barcode2} {line} {message} {timestamp}')
                        
                            return JsonResponse(context)

                elif 'KTL' in line:
                    if barcode[0:2] == '30' and barcode.isdigit() and len(barcode) == 5:
                        if barcode == '30110':
                            quty = 84
                        elif barcode == '30111':
                            quty = 84
                        elif barcode == '30112':
                            quty = 84
                        elif barcode == '30082':
                            quty = 250
                        elif barcode == '30126':
                            quty = 250
                        else:
                            quty = 0

                        message = 'Poprawny kod Bruss KTL'

                        Barcodes.objects.create(
                                    code=barcode,
                                    line=line,
                                    name=get_add_bcode(barcode),
                                    timestamp=timestamp
                                )
                        
                        context = {'status': 'SUCCESS', 'message': message, 'quantity': quty}
                    
                        logger.info(f'[{datetime.datetime.now()}] Barcode INFO {barcode} {barcode2} {barcode3} {line} {message} {timestamp}')
                        return JsonResponse(context)  

                    elif (barcode[0:6].isdigit() and barcode[6] == 'K' and barcode[7].isdigit()):
    
                        message = "Niepoprawny kod\nZeskanuj kod z dołu kartki MRGGTM___"

                    elif (barcode[0:3] == 'KES' and barcode[3:].isdigit() and len(barcode) == 9) or \
                          (barcode[0:6] == 'MRGGTM' and barcode[6:].isdigit()) or \
                           (barcode[0:6] == 'MTGGTM' and barcode[6:].isdigit()) or \
                            (barcode[0:4] == '000B' and barcode[4:].isdigit() and len(barcode) == 9):
                
                        message = 'Poprawny kod Bruss'

                    elif (barcode[0:3] == 'MET' and barcode[3:].isdigit() and len(barcode) == 9):

                        message = 'Poprawny kod Metal Martin'

                    elif barcode[0:3] == 'MUB' and barcode[3:].isdigit() and len(barcode) == 9:
                        
                        message = 'Poprawny kod Mubea'

                    if 'Niepoprawny kod' not in message:
                        with transaction.atomic():
                            suma = int(barcode3) if barcode3 != '' else 0
                            if "Metal" in message:
                                order = MetalMartinOrders.objects.filter(orderId=barcode).last()
                            elif "Bruss" in message:
                                order = BrussOrders.objects.filter(orderId=barcode[3:]).last()
                            elif "Mubea" in message:
                                order = MubeaOrders.objects.filter(orderId=barcode).last()
                            
                            if not order:
                                #message = "Nie znaleziono kodu w bazie"
                                #context = {'status': 'WARNING', 'message': message}
                                qty = 0
                                logger.info(f'[{datetime.datetime.now()}] Barcode WARNING {barcode} {barcode2} {barcode3} {line} Nie znaleziono kodu w bazie {timestamp}')
                                #return JsonResponse(context)

                            else:
                                order_qty = order.quantity

                                if barcode3 == '':
                                    qty = order_qty
                                else:
                                    qty = barcode3
                            
                            # for i in Barcodes.objects.filter(code=barcode, line=line):
                            #     try:
                            #         qty = int(i.additional_info.split(',')[1])
                            #         suma += qty
                            #     except:  
                            #         suma += order_qty   
                            #         break
                            
                            # if order_qty < suma:
                            #     message = f"Wykorzystano o {suma - order_qty} szt. za dużo"
                            #     context = {'status': 'WARNING', 'message': message}
                            #     logger.info(f'[{datetime.datetime.now()}] Barcode WARNING {barcode} {barcode2} {barcode3} {line} {message} {timestamp}')
                            #     return JsonResponse(context)
                            
                            # elif barcode3 == '' and order_qty - suma == 0:
                            #     message = f"Wykorzystano wszystkie sztuki z tej karty"
                            #     context = {'status': 'WARNING', 'message': message}
                            #     logger.info(f'[{datetime.datetime.now()}] Barcode WARNING {barcode} {barcode2} {barcode3} {line} {message} {timestamp}')
                            #     return JsonResponse(context)
                            
                            # else:
                            


                            add_info = f"{barcode2},{qty}"

                            Barcodes.objects.create(
                                code=barcode,
                                line=line,
                                timestamp=timestamp,
                                name=get_add_bcode(barcode),
                                additional_info=add_info
                            )
                            if line == "Kontrola jakości (KTL)" and message == 'Poprawny kod Metal Martin':
                                #message = f"Wykorzystano {qty} szt."

                                try:
                                    package = order.ident.packaging if order.ident.packaging != 'nan' else 'Nie przypisano opakowania'

                                    context = {'status': 'SUCCESS', 'message': message, 'quantity': qty, 'packaging': package}
                                except:
                                    context = {'status': 'SUCCESS', 'message': message, 'quantity': qty, 'packaging': 'Nie przypisano opakowania'}
                            else:
                                context = {'status': 'SUCCESS', 'message': message, 'quantity': qty}
                            
                            
                        
                            logger.info(f'[{datetime.datetime.now()}] Barcode INFO {barcode} {barcode2} {barcode3} {line} {message} {timestamp}')
                            return JsonResponse(context)

                elif line == 'Prasy':
                    if (barcode[0:5].isdigit() and barcode[5] == '-' and barcode[6:].isdigit()):

                        message = 'Poprawny kod prasy'

                elif 'Magazyn' in line:
                    try:            
                        if barcode[0:2] == '30' and barcode.isdigit() and len(barcode) == 5:

                            message = 'Poprawny kod Bruss KTL'
                        
                        elif (barcode[0:6].isdigit() and barcode[6] == 'K' and barcode[7].isdigit()):
        
                            message = "Niepoprawny kod\nZeskanuj kod z dołu kartki MRGGTM___"
                    except:
                        pass

                    if (barcode.isdigit() and len(barcode) == 6) or \
                            (barcode[0:3] == 'MUB' and barcode[3:].isdigit() and len(barcode) == 9):
                        
                        message = 'Poprawny kod Mubea'
                    
                    elif (barcode.isdigit() and len(barcode) == 6) or \
                            (barcode[0:3] == 'ADI' and barcode[3:].isdigit() and len(barcode) == 9):
                        
                        message = 'Poprawny kod Adient'

                    elif (barcode[0] == 'F' and barcode[1:].isdigit()) or \
                            (barcode[0:3] == "FRC" and barcode[3:].isdigit()):
                        
                        message = 'Poprawny kod Faurecia'

                    elif (barcode[0:3] == "TKN" and barcode[3:].isdigit()):
                        
                        message = 'Poprawny kod Teknia'

                    elif (barcode[0:3] == 'KES' and barcode[3:].isdigit() and len(barcode) == 9) or (barcode[0:6] == 'MRGGTM' and barcode[6:].isdigit()) or (barcode[0:6] == 'MTGGTM' and barcode[6:].isdigit()) or\
                        (barcode[0:3] == 'BRS' and barcode[3:].isdigit() and len(barcode) == 9) or (barcode[0:4] == '000B' and barcode[4:].isdigit() and len(barcode) == 9):

                        message = 'Poprawny kod Bruss'

                    elif (barcode[0:3] == 'MET' and barcode[3:].isdigit() and len(barcode) == 9):

                        message = 'Poprawny kod Metal Martin'

                    elif (barcode[0:3] == 'SGN' and barcode[3:].isdigit() and len(barcode) == 9):

                        message = 'Poprwany kod Siegana'

                    elif barcode[0:2] == 'VW' and barcode[2:].isdigit() and len(barcode) == 9:

                        message = 'Poprawny kod Vorwerk'

                    elif barcode[0:3] == 'EGH' and barcode[3:].isdigit() and len(barcode) == 9:

                        message = 'Poprawny kod Grosshaus'

                    if barcode[0:2] != '30' and 'przyjęcie' not in line:
                        with transaction.atomic():
                            latest_records = (Barcodes.objects
                                            .filter(code=barcode, used=False)
                                            .values('line')
                                            .annotate(latest_id=Max('id')))
                            
                            for record in latest_records:
                                latest_barcode = Barcodes.objects.get(id=record['latest_id'])
                                latest_barcode.used = True
                                latest_barcode.save()
                                logger.info(f'[{datetime.datetime.now()}] Barcode USED TRUE {barcode} {barcode2} {barcode3} {line} {message} {timestamp}')


                if 'Niepoprawny kod' not in message:
                    with transaction.atomic():
                        existing_record = Barcodes.objects.filter(code=barcode, line=line).last()

                        if existing_record and (barcode2 == '' or  '-' not in barcode2) and \
                              'Magazyn' not in line and 'Bruss WZ' not in line and 'KTL' not in line and 'Fosforanowanie' not in line:
                            message = "Zduplikowany kod"
                            context = {'status': 'WARNING', 'message': message}
                            logger.info(f'[{datetime.datetime.now()}] Barcode FAILURE {barcode} {barcode2} {line} {message} {timestamp}')
                            return JsonResponse(context)
                        
                        elif existing_record and '-' in barcode2:
                            existing_record.additional_info = barcode2
                            existing_record.save()

                            message = "Uaktualniono o kod pras"
                            context = {'status': 'SUCCESS', 'message': message}
                            logger.info(f'[{datetime.datetime.now()}] Barcode INFO {barcode} {barcode2} {line} {message} {timestamp}')
                            return JsonResponse(context)
                        
                        else:
                            new_record = Barcodes.objects.create(
                                code=barcode,
                                line=line,
                                timestamp=timestamp,
                                name=get_add_bcode(barcode),
                                additional_info=barcode2
                            )
                            if 'Magazyn' in line and 'przyjęcie' not in line:
                                new_record.used = True
                                new_record.save()
                                logger.info(f'[{datetime.datetime.now()}] Barcode USED TRUE {barcode} {barcode2} {barcode3} {line} {message} {timestamp}')

                            if line == "Kontrola jakości (KTL)" and message == 'Poprawny kod Metal Martin':
                                mm_record = MetalMartinOrders.objects.filter(orderId=barcode).last()
                                try:
                                    package = mm_record.ident.packaging if mm_record.ident.packaging != 'nan' else 'Nie przypisano opakowania'

                                    context = {'status': 'SUCCESS', 'message': message, 'packaging': package}
                                except:
                                    context = {'status': 'SUCCESS', 'message': message, 'packaging': 'Nie przypisano opakowania'}
                            else:
                                context = {'status': 'SUCCESS', 'message': message}
                            
                            logger.info(f'[{datetime.datetime.now()}] Barcode INFO {barcode} {barcode2} {line} {message} {timestamp}')
                            return JsonResponse(context)
                
                else:
                    context = {'status': 'FAILURE', 'message': message}
                    error_message = f"An error occurred: {message}\n{traceback.format_exc()}"
                    logger.info(f'[{datetime.datetime.now()}] Barcode FAILURE {barcode} {barcode2} {line} {message} {timestamp} {error_message}')
                    return JsonResponse(context)
            
            else:
                message = "Kod nie został zeskanowany."
                error_message = f"An error occurred: {message}\n{traceback.format_exc()}"
                context = {'status': 'FAILURE', 'message': message}
                logger.error(f'[{datetime.datetime.now()}] Barcode FAILURE {barcode} {barcode2} {line} {message} {timestamp} {error_message}')
                return JsonResponse(context)
        
        except sqlite3.OperationalError as e:
            message = "Zeskanuj ponownie."
            error_exception = f"{e}"
            error_message = f"An error occurred: {message}\n{traceback.format_exc()}"

            context = {'status': 'FAILURE', 'message': message}
            logger.error(f'[{datetime.datetime.now()}] Barcode FAILURE Error: {e} {barcode} {barcode2} {line} {message} {timestamp} {error_message}')
            
            return JsonResponse(context)

        except Exception as e:
            message = "Błąd serwera."
            error_exception = f"{e}"
            error_message = f"An error occurred: {message}\n{traceback.format_exc()}"

            if "10061" in error_exception:
                send_email(
                    subject=error_exception,
                    body=error_message,
                    receiver="lepnik01@gmail.com, piotr.babicz@galwanotechnika.eu"
                )

            context = {'status': 'FAILURE', 'message': message}
            logger.error(f'[{datetime.datetime.now()}] Barcode FAILURE Error: {e} {barcode} {barcode2} {line} {message} {timestamp} {error_message}')
            
            return JsonResponse(context)
    else:
        message = "Not a POST request"
        error_message = f"An error occurred: {message}\n{traceback.format_exc()}"
        context = {'status': 'FAILURE', 'message': message}
        logger.error(f'[{datetime.datetime.now()}] Barcode FAILURE {error_message}')
        return JsonResponse(context)
    
    
@csrf_exempt
def get_barcode(request):
    range_count = request.GET.get('range')

    if range_count == 'default':
        date_lower = datetime.datetime.now() - datetime.timedelta(days=14)
        date_lower = datetime.datetime.combine(date_lower.date(), datetime.time.min)
        barcodes = Barcodes.objects.filter(timestamp__gte=date_lower).order_by('-timestamp')


        barcodes_dict = [
            {'code': barcode.code, 'timestamp': barcode.timestamp.strftime("%Y-%m-%d %H:%M:%S"), 'line': barcode.line, 'name': barcode.name,'additional_info': barcode.additional_info if barcode.additional_info is not None else '', 'used': barcode.used}
            for barcode in barcodes
        ]

        context = {
            'barcodes': barcodes_dict,
            'row_count': len(barcodes_dict),
            'date_range_str': f"{date_lower.replace(microsecond=0)}  -  {datetime.datetime.now().replace(microsecond=0)}"
        }

        return JsonResponse(context)

    else:
        clients = request.GET.getlist('clients')
        lines = request.GET.getlist('lines')
        date_start = request.GET.get('dateStart')
        date_end = request.GET.get('dateEnd')
        print(lines)
        for i in range(len(lines)):
            if lines[i] == "Magazyn wysyłkowy":
                lines.pop(i)
                lines[i:i] = ['Magazyn (Kolejowa) Bruss WZ', 'Magazyn wysyłkowy (Kolejowa)']

            elif lines[i] == "Magazyn przyjęcie":
                lines.pop(i)
                lines[i:i] = ['Magazyn przyjęcie (Kolejowa)']

            elif lines[i] == 'Kontrola jakości':
                lines.pop(i)
                lines[i:i] = ['Kontrola jakości (NiCr)', 'Kontrola jakości (KTL)']
            
        # Build the query
        code_conditions = Q()
        for client in clients:
            if client in CLIENT_CODE_CONDITIONS:
                code_conditions |= CLIENT_CODE_CONDITIONS[client]


        now = datetime.datetime.now().replace(microsecond=0)

        # If dateStart is not provided, get the earliest timestamp from Barcodes
        if not date_start:
            earliest_timestamp = Barcodes.objects.aggregate(Min('timestamp'))['timestamp__min']
            # If there are records in Barcodes, set date_start to the earliest timestamp,
            # otherwise, set it to the current datetime
            date_start = earliest_timestamp if earliest_timestamp else now

        # Convert date strings to datetime objects
        elif date_start:
            date_start = datetime.datetime.strptime(date_start, '%Y-%m-%d %H:%M')

        if date_end:
            date_end = datetime.datetime.strptime(date_end, '%Y-%m-%d %H:%M')

        # If date_end is not provided, set it to the current datetime
        elif not date_end:
            date_end = now

        # Swap dates if end date is earlier than start date
        if date_end < date_start:
            date_start, date_end = date_end, date_start

        # Build timestamp conditions
        timestamp_conditions = Q(timestamp__range=(date_start, date_end))

        combined_conditions = code_conditions & timestamp_conditions

        if lines:
            line_conditions = Q(line__in=lines)
            combined_conditions &= line_conditions

        # Query the database
        barcodes = Barcodes.objects.filter(combined_conditions)

        barcodes_dict = [{
            'code': barcode.code,
            'timestamp': barcode.timestamp.strftime("%Y-%m-%d %H:%M:%S"), 
            'line': barcode.line,
            'name': barcode.name, 
            'additional_info': barcode.additional_info if barcode.additional_info is not None else '',
            'used': barcode.used
        } for barcode in barcodes]

        context = {
            'barcodes': barcodes_dict,
            'row_count': len(barcodes_dict),
            'date_range_str': f"{date_start}  -  {date_end}"
        }

        return JsonResponse(context)


@csrf_exempt
def delete_barcode(request):
    if request.method == 'POST':
        try:
            barcode = request.POST.get('code')
            timestamp = request.POST.get('timestamp')
            line = request.POST.get('line')
            queryset = Barcodes.objects.filter(code=barcode, timestamp=timestamp, line=line)
            if queryset.exists():
                queryset.first().delete()
            else:
                logger.info(f'[{datetime.datetime.now()}] Barcode DELETION FAILED {barcode} {timestamp} {line}')


            logger.info(f'[{datetime.datetime.now()}] Barcode DELETED {barcode} {timestamp} {line}')
            return JsonResponse({'message': 'success'})
        
        except Exception as e:
            return JsonResponse({})


@csrf_exempt
def add_barcode(request):
    if request.method == 'POST':
        try:
            barcode = request.POST.get('code')
            timestamp = request.POST.get('timestamp')
            line = request.POST.get('line')
            add_info = request.POST.get('add_info')

            with transaction.atomic():
                Barcodes.objects.create(
                        code=barcode,
                        line=line,
                        timestamp=timestamp,
                        name=get_add_bcode(barcode),
                        additional_info=add_info
                    )
                logger.info(f'[{datetime.datetime.now()}] Barcode ADDED {barcode} {timestamp} {line} {add_info}')
                
            context = {
                'message': 'success',
            }

            return JsonResponse(context)
        
        except Exception as e:
            return JsonResponse({})
        

def view_barcodes(request):
    try:
        one_week_ago = datetime.datetime.now() - datetime.timedelta(days=14)
        barcodes = Barcodes.objects.filter(timestamp__gte=one_week_ago).order_by('-timestamp')

        barcodes_dict = {
            i: [barcode.code, barcode.timestamp.strftime("%Y-%m-%d %H:%M:%S"), barcode.line, barcode.name, barcode.additional_info if barcode.additional_info is not None else '', '', barcode.used] for i, barcode in enumerate(barcodes)
        }

        context = {
            'header': ['Kod', 'Czas', 'Linia', 'Nazwa', 'Dodatkowe info.', 'Przycisk'],
            'i_dict': barcodes_dict,
            'row_count': len(barcodes_dict),
            'date_range_str': f'{one_week_ago.replace(microsecond=0)}  -  {datetime.datetime.now().replace(microsecond=0)}'
        }
        return render(request, 'view_barcodes.html', context)
    
    except Exception as e:
        import traceback
        error_message = f"An error occurred: {str(e)}\n{traceback.format_exc()}"
    
        return render(request, 'error.html', {'error_message': error_message})


@csrf_exempt
def stock_issue_confirmation(request):
    try :
        if request.method == 'POST':    
            form = BrussExportForm(request.POST)
            if form.is_valid():
                query_id = form.cleaned_data['sic_id']
                entry = get_object_or_404(BrussExport, id=query_id)
            else:
                form = BrussExportForm()
                entry = BrussExport.objects.order_by('-timestamp').first()
        else:
            form = BrussExportForm()
            entry = BrussExport.objects.order_by('-timestamp').first()
        blank_spaces = len(entry.json_data)
        sic_id_str = f'WZ 3 {"0" * (3 - len(str(entry.sic_id))) + str(entry.sic_id)}/{entry.timestamp.strftime("%m/%Y")}'
        suma = 0
        for i in entry.json_data:
            try:
                if len(i[0]) == 5:
                    i[0] = "KTL" + i[0]

                order = BrussOrders.objects.filter(orderId=i[0][3:]).first()
                i.append(order.ident.number)
                i.append(order.ident.description)
                i.append(order.quantity)
                suma += order.quantity
                i.append('szt.')

                if 'single order' in order.kanbanCard:
                    add = 'single order'
                elif '/' not in order.kanbanCard:
                    add = order.kanbanCard 
                elif 'poprawkowe' in order.kanbanCard:
                    add = 'poprawkowe'
                else:
                    add = f"Kanban {order.kanbanCard}"

                i.append(add)
                
            except:
                for k in range(5):
                    i.append('')


        context = {
            'form': form,
            'blank': range(7 - blank_spaces),
            'sic_nr':entry.sic_id,
            'sic_str':sic_id_str,
            'json_data':entry.json_data,
            'timestamp':entry.timestamp,
            'init_sum':suma,
            'bruss_idents': list(BrussIdent.objects.exclude(description__isnull=True).values('number', 'description')),

        }
        return render(request, 'stock_issue_confirmation.html', context)
    
    except Exception as e:
        import traceback
        error_message = f"An error occurred: {str(e)}\n{traceback.format_exc()}"
        logger.info(f'[{datetime.datetime.now()}] SIC POST MAIN ERROR {error_message}')
        return render(request, 'error.html', {'error_message': error_message})


@csrf_exempt
def stock_issue_confirmation_manual(request):
    try :
        return render(request, 'stock_issue_confirmation_manual.html')

    except Exception as e:
        import traceback
        error_message = f"An error occurred: {str(e)}\n{traceback.format_exc()}"
        return render(request, 'error.html', {'error_message': error_message})


@csrf_exempt
def sic_post(request):
    try :
        if request.method == 'POST':    
            json_data = json.loads(request.body.decode('utf-8'))
            extracted_data = json_data.get('extracted_data')
            extracted_sic = json_data.get('sic_id')
            extracted_timestamp = json_data.get('timestamp')

        bruss_export_instance = get_object_or_404(BrussExport, sic_id=extracted_sic, timestamp=extracted_timestamp)
        extracted_data_list = []
        for i in extracted_data:
            extracted_data_list.append([i['orderId'], i['pl1'], i['pl2'], i['identId'], i['identName'], i['identQuantity'], i['identUnit'], i['identAdditional']])
        
        bruss_export_instance.json_data = extracted_data_list

        bruss_export_instance.save()
        logger.info(f'[{datetime.datetime.now()}] SIC POST PRINT SUCCESS {extracted_sic} {extracted_timestamp} {extracted_data}')
        return JsonResponse({'status': 'success'})
    
    except Exception as e:
        logger.info(f'[{datetime.datetime.now()}] SIC POST PRINT ERROR {e}')
        return JsonResponse({'status': 'failure'})
    

@csrf_exempt
def sic_get(request):
    try :
        if request.method == 'GET':    
            order_id = request.GET.get('orderId')
            orderId = re.sub(r'\s', '', order_id)
            orderId = orderId.upper()
            if 'GTM' not in orderId and 'B' not in orderId:
                numbers = re.findall(r'\d+', orderId)
                orderId = numbers[0]
            if 'MRG' in orderId or 'MTG' in orderId:
                orderId = orderId.replace("MRG", '')

            boi = BrussOrders.objects.filter(orderId=orderId).first()
            
            if 'single order' in boi.kanbanCard:
                kanban = 'single order'
            elif '/' not in boi.kanbanCard:
                kanban = boi.kanbanCard 
            elif 'poprawkowe' in boi.kanbanCard:
                kanban = 'poprawkowe'
            else:
                kanban = f"Kanban {boi.kanbanCard}"

            if "GTM" in orderId:
                pre_orderId = "MRG" + orderId
            elif "B" in orderId:
                pre_orderId = "000" + orderId
            elif "KTL" in kanban:
                pre_orderId = "KTL" + orderId
            else:
                pre_orderId = "KES" + orderId

            context = {
                'status': 'success',
                'orderId': pre_orderId,
                'identId': boi.ident.number,
                'identName': boi.ident.description,
                'identQuantity': boi.quantity,
                'identUnit': 'szt.',
                'identAdditional': kanban
            }

            logger.info(f'[{datetime.datetime.now()}] SIC GET SUCCESS {order_id} ')
            return JsonResponse(context)

        else:
            return JsonResponse({'status': 'failure'})

    except Exception as e:
        logger.info(f'[{datetime.datetime.now()}] SIC GET FAULURE {order_id} {e} ')
        return JsonResponse({'status': 'failure'})
    

@csrf_exempt
def sic_new(request):
    try :
        if request.method == 'POST':  
            now = datetime.datetime.now()
            current_month = now.month
            current_year = now.year

            max_id = BrussExport.objects.filter(
                timestamp__year=current_year,
                timestamp__month=current_month
            ).aggregate(Max('sic_id'))['sic_id__max']

            new_sic_id = max_id + 1 if max_id is not None else 1

            entry = BrussExport.objects.create(
                sic_id=new_sic_id,
                json_data=[['  ', '  ', '  ', '  ', '  ', '  ', 'szt.', '  ']],
                timestamp=now
            )
            
            form = BrussExportForm()

            blank_spaces = len(entry.json_data)
            sic_id_str = f'WZ 3 {"0" * (3 - len(str(entry.sic_id))) + str(entry.sic_id)}/{entry.timestamp.strftime("%m/%Y")}'
 
            context = {
                'form': form,
                'blank': range(7 - blank_spaces),
                'sic_nr':entry.sic_id,
                'sic_str':sic_id_str,
                'json_data':entry.json_data,
                'timestamp':entry.timestamp,
                'init_sum':0,
                'bruss_idents': list(BrussIdent.objects.exclude(description__isnull=True).values('number', 'description')),

            }
            logger.info(f'[{datetime.datetime.now()}] SIC NEW {sic_id_str}')
            return render(request, 'stock_issue_confirmation.html', context)
        
    except Exception as e:
        import traceback
        error_message = f"An error occurred: {str(e)}\n{traceback.format_exc()}"
        logger.info(f'[{datetime.datetime.now()}] SIC POST MAIN ERROR {error_message}')
        return render(request, 'error.html', {'error_message': error_message})


@csrf_exempt
def sic_delete(request):
    try :
        if request.method == 'POST':  
            sic_id = request.POST.get('sic_nr')  
            timestamp = request.POST.get('timestamp')

            entry = get_object_or_404(BrussExport, sic_id=sic_id, timestamp=timestamp)
            entry.delete()
            logger.info(f'[{datetime.datetime.now()}] SIC DELETE SUCCESS {entry.sic_id} {entry.timestamp} {entry.json_data}')
            return JsonResponse({'status': 'success'})

        else:
            return JsonResponse({'status': 'failure'})

    except Exception as e:
        try:
            logger.info(f'[{datetime.datetime.now()}] SIC DELETE FAULURE {sic_id} {timestamp} {entry.json_data} {e} ')
        except:
            logger.info(f'[{datetime.datetime.now()}] SIC DELETE FAULURE {e} ')

        return JsonResponse({'status': 'failure'})


@csrf_exempt
def sic_display(request):
    try :
        if request.method == 'POST':
            date_form = DateInputForm(request.POST)
            if date_form.is_valid():
                selected_date = date_form.cleaned_data['selected_date']
                form = BrussExportFormAll(request.POST, filter_date=selected_date)
            else:
                form = BrussExportFormAll(request.POST, filter_date=None)

            if form.is_valid():
                query_id = form.cleaned_data['sic_id']
                entry = get_object_or_404(BrussExport, id=query_id)
                form = BrussExportFormAll(filter_date=entry.timestamp.strftime('%Y-%m-%d'))
                date_form = DateInputForm(filter_date=entry.timestamp.strftime('%Y-%m-%d'))
            else:
                form = BrussExportFormAll(filter_date=selected_date)
                entry = BrussExport.objects.filter(timestamp__date=selected_date).order_by('-timestamp').first()

        else:
            form = BrussExportFormAll()
            date_form = DateInputForm()
            entry = BrussExport.objects.order_by('-timestamp').first()

        blank_spaces = len(entry.json_data)
        sic_id_str = f'WZ 3 {"0" * (3 - len(str(entry.sic_id))) + str(entry.sic_id)}/{entry.timestamp.strftime("%m/%Y")}'
        
        suma = 0
        for i in entry.json_data:
            try:
                suma += int(i[5])
            except:
                pass

        context = {
            'form': form,
            'date_form': date_form,
            'blank': range(7 - blank_spaces),
            'sic_nr':entry.sic_id,
            'sic_str':sic_id_str,
            'json_data':entry.json_data,
            'timestamp':entry.timestamp,
            'init_sum':suma,
            'bruss_idents': list(BrussIdent.objects.exclude(description__isnull=True).values('number', 'description')),
        }
        return render(request, 'sic_list.html', context)
    
    except AttributeError as e:
        context = {
        'form': form,
        'date_form': date_form,
        'blank': '',
        'sic_nr': '',
        'sic_str':'',
        'json_data':'',
        'timestamp':'',
        'init_sum':'',
        'bruss_idents': list(BrussIdent.objects.exclude(description__isnull=True).values('number', 'description')),

        }
        return render(request, 'sic_list.html', context)
    
    except Exception as e:
        import traceback
        error_message = f"An error occurred: {str(e)}\n{traceback.format_exc()}"
    
        return render(request, 'error.html', {'error_message': error_message})
    

@csrf_exempt
def sic_display_day(request):
    path_to_file = r"tools\static\csv\Towary.csv"
    dfa = pd.read_csv(path_to_file, delimiter='\t', encoding='utf-16')
    
    def check_enova_id(obj, df):
        if obj == '23090':
            return '2308/T'
        
        if obj == '23441/p':
            return 'KF 55,1/1 PONO'
        
        ident_dict = {
            21034: 22402,
            21918: 22444,
            22263: 22400,
            21347: 21177,
            21346: 21176,
            26349: 22403,
            23404: 30169,
            21729: 22349,
            21728: 22348,
            16606: 11111,
            21407: 26508,
            22953: 24159,
            22954: 24160,
            22984: 22982,
            22983: 23023,
            23469: 22469,
            23468: 22468,
            18597: 18596,
            18918: 23668,
            21388: 26336,
            16637: 22343,
            16625: 22345,
            16636: 22342,
            16637: 22343,
            16654: 22344
        }
        rework_list = df.iloc[:, 0].astype(str).tolist()
        df = df[~df.iloc[:, 0].astype(str).str.contains('p')]
        first_5_chars = df.iloc[:, 0].astype(str).str[:5].tolist()
        last_5_chars = df.iloc[:, 0].astype(str).str[-5:].tolist()
        full_chars = df.iloc[:, 0].astype(str)
        
        # Check if obj is in the CSV column values
        if str(obj) in rework_list:
            return str(obj)
            
        elif str(obj) in first_5_chars or str(obj) in last_5_chars:
            index = first_5_chars.index(str(obj)) if str(obj) in first_5_chars else last_5_chars.index(str(obj))
            value_from_full_chars = full_chars.iloc[index]

            return value_from_full_chars
        
        else:
            if obj in ident_dict or int(obj) in ident_dict:
                obj = int(obj)
                if str(ident_dict[obj]) in first_5_chars or str(ident_dict[obj]) in last_5_chars:
                    index = first_5_chars.index(str(ident_dict[obj])) if str(ident_dict[obj]) in first_5_chars else last_5_chars.index(str(ident_dict[obj]))
                    value_from_full_chars = full_chars.iloc[index]
                    return value_from_full_chars
                else:
                    return f'{obj} nie znaleziono w Enova'
            else:
                return f'{obj} nie znaleziono w Enova obj not in ident_dict'


    try :
        if request.method == 'POST':
            date_form = DateInputForm(request.POST)
            if date_form.is_valid():
                selected_date = date_form.cleaned_data['selected_date']
            else:
                selected_date = datetime.date.today() - datetime.timedelta(days=1)
                date_form = DateInputForm()
          
        else:
            selected_date = datetime.date.today() - datetime.timedelta(days=1)
            date_form = DateInputForm()

        entries = BrussExport.objects.filter(timestamp__date=selected_date).order_by('timestamp')

        sic_day_dict_list = []
        code_timestamp = {}
        for entry in entries:
            for i in entry.json_data:
                if 'KTL' not in i[0]:
                    sic_id_str = f'WZ 3 {"0" * (3 - len(str(entry.sic_id))) + str(entry.sic_id)}/{entry.timestamp.strftime("%m/%Y")}'
                    code_timestamp[i[0][3:]] = [entry.timestamp.strftime("%d.%m.%Y"), sic_id_str]

                if i[3] == '':
                    continue
                try:
                    sic_day_dict_list.append([i[3], int(i[5]), i[7], i[0]])

                except Exception as e:
                    print(e)
            
        sic_day_list = []
        sic_day_list_raw = []
        sic_day_list_press = []
        sic_day_list_ktl = []

        for j in sic_day_dict_list:
            name = BrussIdent.objects.filter(number=j[0]).first()
            if 'pop' in j[2]:
                j[0] += '/p'
            sic_day_list_raw.append([j[0], name.description, j[1], f"{name.supplier}\n{j[3]}\n{j[2]}"])

            ident_enova_id = check_enova_id(j[0], dfa)
            # id    name    qty
            if name.supplier == 'Prasy' and '/p' not in ident_enova_id:
                sic_day_list_press.append([ident_enova_id, name.description, j[1], f"{j[3]}\n{j[2]}"])

            elif ident_enova_id == '30110' or ident_enova_id == '30111' or ident_enova_id == '30112' \
                or ident_enova_id == '30082' or ident_enova_id == '30126':
                sic_day_list_ktl.append([ident_enova_id, name.description, j[1], f"{name.supplier}\n{j[3]}\n{j[2]}"])
            else:
                sic_day_list.append([ident_enova_id, name.description, j[1], f"{name.supplier}\n{j[3]}\n{j[2]}"])
                if ident_enova_id == '23180/23205':
                    sic_day_list_ktl.append(['30126/KTL', name.description, j[1], f"{name.supplier}\n{j[3]}\n{j[2]}"])

        context = {
            'date_form': date_form,
            'sic_day_list': sic_day_list,
            'sic_day_list_raw': sic_day_list_raw,
            'sic_day_list_press': sic_day_list_press,
            'sic_day_list_ktl': sic_day_list_ktl,
            'timestamp': selected_date,
            'data_to_copy': code_timestamp
        }

        return render(request, 'sic_list_day.html', context)

    
    except Exception as e:
        import traceback
        error_message = f"An error occurred: {str(e)}\n{traceback.format_exc()}"
    
        return render(request, 'error.html', {'error_message': error_message})
    

def warehouse_menu(request):
    try :
        return render(request, 'warehouse_menu.html')
    
    except Exception as e:
        import traceback
        error_message = f"An error occurred: {str(e)}\n{traceback.format_exc()}"
    
        return render(request, 'error.html', {'error_message': error_message})
   

def warehouse_delivery(request):
    day_start = datetime.date.today() - datetime.timedelta(days=14)

    codes_scanned = Barcodes.objects.filter(
        line="Magazyn przyjęcie (Kolejowa)", 
        timestamp__gte=day_start
    ).values('code')

    # Helper function to fetch and group orders by a given model and date field
    def fetch_and_group_orders(model, day_start, codes_scanned, FRCclient=None):
        queryset = model.objects.filter(dateDelivered__gte=day_start)
        
        # Dynamically add condition based on variable1
        if FRCclient:
            queryset = queryset.filter(client=FRCclient)
        
        # Exclude orders that match the codes_scanned subquery
        queryset = queryset.exclude(orderId__in=Subquery(codes_scanned))
        
        # Select required fields
        queryset = queryset.values(
            'dateDelivered', 'orderId', 'quantity', 'ident__number', 'ident__description', 'delivery_note'
        )
        
        # Group orders by 'dateDelivered'
        grouped_orders = defaultdict(list)
        for entry in queryset:
            grouped_orders[entry['dateDelivered']].append({
                'orderID': entry['orderId'],
                'quantity': entry['quantity'],
                'identNumber': entry['ident__number'],
                'identDescription': entry['ident__description'],
                'deliveryNote': entry['delivery_note']
            })
        
        return dict(grouped_orders)

    # Fetch and group orders for all models
    mubea_orders = fetch_and_group_orders(MubeaOrders, day_start, codes_scanned)
    metal_martin_orders = fetch_and_group_orders(MetalMartinOrders, day_start, codes_scanned)
    siegenia_orders = fetch_and_group_orders(SiegeniaOrders, day_start, codes_scanned)
    faurecia_orders = fetch_and_group_orders(FaureciaOrders, day_start, codes_scanned, FRCclient='FAURECIA')
    teknia_orders = fetch_and_group_orders(FaureciaOrders, day_start, codes_scanned, FRCclient='TEKNIA')
    
    clients_dict = {
        'mubea': mubea_orders,
        'metalmartin': metal_martin_orders,
        'siegenia': siegenia_orders,
        'faurecia': faurecia_orders,
        'teknia': teknia_orders,
    }

    context = {
        'clients_dict': clients_dict,
    }

    return render(request, 'warehouse_delivery.html', context)


def warehouse_shipment(request):
    day_start = datetime.date.today() - datetime.timedelta(days=14)

    codes_scanned_prod = Barcodes.objects.filter(
        line__in=["Kontrola jakości (NiCr)", "Kontrola jakości (KTL)", "Fosforanowanie", "Cynkowanie"], 
        timestamp__gte=day_start
    ).values('code')

    codes_scanned_wh = Barcodes.objects.filter(
        line__in=["Magazyn wysyłkowy (Kolejowa)", "Magazyn (Kolejowa) Bruss WZ"], 
        timestamp__gte=day_start
    ).values('code')

    codes_scanned_prod_bruss = Barcodes.objects.filter(
        line__in=["Kontrola jakości (KTL)", "Fosforanowanie"], 
        timestamp__gte=day_start
    ).annotate(
        code_substr=Func(F('code'), Value(4), function='substr', output_field=CharField())
    ).values('code_substr')

    codes_scanned_wh_bruss = Barcodes.objects.filter(
        code__contains=['KES', '000B'],
        line__in=["Magazyn (Kolejowa) Bruss WZ"], 
        timestamp__gte=day_start
    ).annotate(
        code_substr=Func(F('code'), Value(4), function='substr', output_field=CharField())
    ).values('code_substr')

    def fetch_and_group_orders(model, day_start, codes_scanned_prod, codes_scanned_wh, FRCclient=None):
        queryset = model.objects.filter(created_at__gte=day_start)
        
        if FRCclient:
            queryset = queryset.filter(client=FRCclient)
        
        queryset = queryset.filter(orderId__in=Subquery(codes_scanned_prod))

        queryset = queryset.exclude(orderId__in=Subquery(codes_scanned_wh))
        
        queryset = queryset.values(
            'orderId', 'quantity', 'ident__description', 'delivery_note'
        )
        
        orders = []
        for entry in queryset:
            orders.append({
                'orderID': entry['orderId'],
                'quantity': entry['quantity'],
                'identDescription': entry['ident__description'],
                'deliveryNote': entry['delivery_note']
            })
        
        return orders

    bruss_orders = fetch_and_group_orders(BrussOrders, day_start, codes_scanned_prod_bruss, codes_scanned_wh_bruss)
    mubea_orders = fetch_and_group_orders(MubeaOrders, day_start, codes_scanned_prod, codes_scanned_wh)
    metal_martin_orders = fetch_and_group_orders(MetalMartinOrders, day_start, codes_scanned_prod, codes_scanned_wh)
    siegenia_orders = fetch_and_group_orders(SiegeniaOrders, day_start, codes_scanned_prod, codes_scanned_wh)
    faurecia_orders = fetch_and_group_orders(FaureciaOrders, day_start, codes_scanned_prod, codes_scanned_wh, FRCclient='FAURECIA')
    teknia_orders = fetch_and_group_orders(FaureciaOrders, day_start, codes_scanned_prod, codes_scanned_wh, FRCclient='TEKNIA')

    clients_dict = {
        'bruss': bruss_orders,
        'mubea': mubea_orders,
        'metalmartin': metal_martin_orders,
        'siegenia': siegenia_orders,
        'faurecia': faurecia_orders,
        'teknia': teknia_orders,
    }

    context = {
        'clients_dict': clients_dict,
    }

    return render(request, 'warehouse_shipment.html', context)


def warehouse_stock(request):
    table_headers = [
        {'name': "Numer artykułu surowego", 'help_text': 'Numer artykułu klienta przed pokryciem.'},
        {'name': "Numer arykułu pokrytego", 'help_text': 'Numer arykułu klienta po procesie.'},
        {'name': "Typ artykułu", 'help_text': 'Stange - Sztanga/Bügel - Zagłówek.'},
        {'name': "Nazwa artykułu", 'help_text': 'Pełna nazwa artykułu klienta.'},
        {'name': "Towary przychodzące", 'help_text': 'Zamówienia dodane do pliku "Raw material 2023 (2).xlsm" nie zeskanowene jeszcze przez Magazyn. (Sprawdza do 7 dni wstecz)'},
        {'name': "Towary niepokryte", 'help_text': 'Suma stanów z pliku "Raw material 2023 (2).xlsm" pomniejszone o kolumnę "Towary przychodzące".'},
        {'name': "Towary pokryte przed QS", 'help_text': 'Zamówienia zeskanowane przez produkcje i nie zeskanowe przez QS. (Sprawdza do 7 dni wstecz)'},
        {'name': "Towary pokryte po QS", 'help_text': 'Zamówienie zeskanowane przez QS i nie wysłane. (Sprawdza do 7 dni wstecz)'},
        {'name': "Ostatnia dostawa", 'help_text': 'Data ostatniej dostawy od klienta.'},
        {'name': "Ostatnia wysyłka", 'help_text': 'Data ostatniej wysyłki do klienta'},
    ]

    table_data = []

    seven_days_ago = datetime.date.today() - datetime.timedelta(days=7)
    seven_days_deliveries = list(Barcodes.objects.filter(timestamp__gte=seven_days_ago, line="Magazyn przyjęcie (Kolejowa)").values_list('code', flat=True))
    production_query = Barcodes.objects.filter(
        timestamp__gte=seven_days_ago,
        line__in=['Chromoniklowanie (Linia I)', 'Chromoniklowanie (Linia II)']
    )

    control_query = Barcodes.objects.filter(
        timestamp__gte=seven_days_ago,
        line='Kontrola jakości (NiCr)'
    )

    for ident in MubeaIdent.objects.annotate(number_length=Length('number')).filter(number_length__gte=8):
        row_data = []
        '''Check if article was ever shipped'''
        last_shipping = MubeaShipment.objects.filter(ident=ident).last()
        if not last_shipping:
            continue

        '''Article number raw'''
        row_data.append(ident.number_raw)
        
        '''Article number plated'''
        row_data.append(ident.number)

        '''Article type'''
        row_data.append(ident.type)

        '''Article name'''
        row_data.append(ident.description)

        '''Incoming'''
        ''' CHECK ON DEVELOPMENT '''
        incoming = MubeaOrders.objects.filter(
            dateDelivered__gte=seven_days_ago,
            ident=ident
        ).exclude(
            orderId__in=seven_days_deliveries
        ).aggregate(Sum('quantity'))
        
        incoming_qty = 0

        if incoming['quantity__sum']:
            incoming_qty = incoming['quantity__sum']

        row_data.append(incoming_qty)

        '''Non-plated'''
        unplated = MubeaStock.objects.filter(ident=ident).last()
        unplated_qty = 0

        if unplated:
            unplated_qty = unplated.quantity
        
        unplated_qty -= incoming_qty

        row_data.append(unplated_qty)

        '''Plated (BQC)'''
        plated_bqc = production_query.filter(name__contains=ident.number).exclude(code__in=control_query.values_list('code', flat=True))
        plated_bqc_qty = 0
        
        for i in plated_bqc:
            plated_bqc_qty += int(re.findall(r'\d+', i.name)[-1])

        row_data.append(plated_bqc_qty)

        '''Plated (AQC)'''
        plated_aqc = control_query.filter(name__contains=ident.number, used=False)
        plated_aqc_qty = 0
        
        for i in plated_aqc:
            plated_aqc_qty += int(re.findall(r'\d+', i.additional_info)[-1])
        
        row_data.append(plated_aqc_qty)

        '''Last delivery'''
        last_delivery = MubeaOrders.objects.filter(ident=ident).last()
        if last_delivery:
            row_data.append(last_delivery.dateDelivered)

        '''Last shipment'''
        row_data.append(last_shipping.date)

        ''' Append finished row '''
        table_data.append(row_data)


    context = {
        'tableHeaders': table_headers,
        'tableData': table_data
    }

    return render(request, 'warehouse_stock.html', context)


@csrf_exempt
def bruss_menu(request):
    try :
        return render(request, 'bruss_menu.html')
    
    except Exception as e:
        import traceback
        error_message = f"An error occurred: {str(e)}\n{traceback.format_exc()}"
    
        return render(request, 'error.html', {'error_message': error_message})
    

@csrf_exempt
def new_bruss_barcode(request):
    try :
        highest_id_order = BrussOrders.objects.filter(orderId__contains="B").aggregate(Max('id'))
        if highest_id_order['id__max']:
            highest_id_order_instance = BrussOrders.objects.get(id=highest_id_order['id__max'])
            match = re.search(r'\d+', highest_id_order_instance.orderId)
            if match:
                extracted_number = int(match.group())
                latest_order_nr = extracted_number
            else:
                print("No number found in the string")

        article_ids = BrussIdent.objects.annotate(
            order_count=Count('brussorders', filter=Q(brussorders__orderId__startswith="B"))
        ).order_by('-order_count')

        context = {
            'savedBarcodeGTM': f"000B{latest_order_nr:05d}",
            'barcodeGTM': f"000B{latest_order_nr+1:05d}",
            'articleIDs': article_ids
        }

        return render(request, 'new_bruss_barcode.html', context)
    
    except Exception as e:
        import traceback
        error_message = f"An error occurred: {str(e)}\n{traceback.format_exc()}"
    
        return render(request, 'error.html', {'error_message': error_message})
    

@csrf_exempt
def new_rework_barcode(request):
    try :
        prefix = 'B'

        filtered_objects = BrussOrders.objects.filter(orderId__startswith=prefix)

        filtered_objects = filtered_objects.annotate(
            numeric_part=Cast(Substr('orderId', len(prefix) + 1), IntegerField())
        )

        max_numeric_value = filtered_objects.aggregate(Max('numeric_part'))['numeric_part__max']

        if max_numeric_value is None:
            max_numeric_value = 1
        else:
            max_numeric_value += 1

        code = '{}{:05}'.format("B", max_numeric_value)
        code_bc = '000' + code

        filtered_rework = BrussOrders.objects.filter(
            Q(kanbanCard__contains='pop') & Q(orderId__contains='p')
        )

        codes_to_print = []
        codes_to_save = []
        for i in filtered_rework:
            code = '{}{:05}'.format("B", max_numeric_value)
            code_bc = '000' + code

            codes_to_print.append([code_bc, i.ident.description, f'{i.quantity} szt.', 'poprawkowe'])
            codes_to_save.append(code)
            max_numeric_value += 1

        context = {
            'printerStatus': check_printer_ready(),
            'codes_to_print': codes_to_print,
            'codes_to_save': codes_to_save
        }

        return render(request, 'new_bruss_rework.html', context)
    
    except Exception as e:
        import traceback
        error_message = f"An error occurred: {str(e)}\n{traceback.format_exc()}"
    
        return render(request, 'error.html', {'error_message': error_message})


@csrf_exempt
def single_card_view(request):
    if request.method == "POST":
        html_content = request.POST.get("html_content", "")
        title = request.POST.get("title", "New Tab")
    else:
        html_content = request.GET.get("html_content", "")
        title = request.GET.get("title", "New Tab")

    template = loader.get_template('single_card.html')
    context = {
        'element_content': html_content,
        'title': title
    }
    return HttpResponse(template.render(context, request))


@csrf_exempt
def bruss_db(request):
    try :
        if request.method == 'GET':
            query = request.GET.get('table', None)
            if not query:
                return render(request, 'bruss_db.html')

            if query == 'orders':
                data = list(
                    BrussOrders.objects.select_related('ident').values(
                        'created_at',
                        'ident__number',
                        'ident__description',
                        'dateDeadline',
                        'dateProduction',
                        'dateShipment',
                        'dateConfirmed',
                        'delivery_note',
                        'kanbanCard',
                        'orderId',
                        'quantity',
                        'ident__hanger_ph',
                    )
                )

            elif query == 'idents':
                data = list(BrussIdent.objects.all().values())

            elif query == 'press':
                data = list(PrasyLabel.objects.all().values())

            elif query == 'sic':
                data = list(BrussExport.objects.all().values())

            elif query == 'plan_p':
                data = list(SavedPlanP.objects.all().values())
                for item in data:
                    item['plan_json'] = json.dumps(item['plan_json'], indent=4)

            elif query == 'plan_ktl':
                data = list(SavedPlanKTL.objects.all().values())
                for item in data:
                    item['plan_json'] = json.dumps(item['plan_json'], indent=4)

            else:
                return JsonResponse()
            
            headers = list(data[0].keys()) if data else []

            context = {
                'headers': headers,
                'data': data
            }
            return JsonResponse(context)
    except Exception as e:
        import traceback
        error_message = f"An error occurred: {str(e)}\n{traceback.format_exc()}"
    
        return render(request, 'error.html', {'error_message': error_message})


@csrf_exempt  # Disable CSRF for testing (use proper authentication in production)
def upload_screenshot(request):
    UPLOAD_DIR = "uploads/screenshots/"
    if request.method == "POST" and request.FILES.get("screenshot"):
        screenshot = request.FILES["screenshot"]

        # Ensure upload directory exists
        os.makedirs(UPLOAD_DIR, exist_ok=True)

        # Save file with a unique name (timestamp-based)
        file_path = os.path.join(UPLOAD_DIR, screenshot.name)
        file_name = default_storage.save(file_path, ContentFile(screenshot.read()))

        return JsonResponse({"message": "Screenshot uploaded successfully", "file": file_name})

    return JsonResponse({"error": "No file received"}, status=400)


def vda_delivery(request):
    try:
        if request.method == 'POST':
            date_form = VDA4913DateInput(request.POST)

            if date_form.is_valid():
                date_form_str = date_form.cleaned_data['selected_date']
                mub_ships = MubeaShipment.objects.filter(date=date_form_str)

                # Use defaultdict for automatic list creation
                grouped_ships = defaultdict(list)
                grouped_weights = defaultdict(int)
                raw_specs = {}

                # Fetch all shipments at once and process them in Python (avoids multiple queries)
                for ship in mub_ships:                    
                    cleaned_spec = re.sub(r'\D', '', ship.spec)

                    grouped_ships[cleaned_spec].append(ship)
                    grouped_weights[cleaned_spec] += ship.weight

                    if ship.spec not in raw_specs:
                        raw_specs[ship.spec] = cleaned_spec
                        
                context = {
                    'groupedShipArticles': dict(grouped_ships),
                    'groupedShipWeights': dict(grouped_weights),
                    'rawSpecs': raw_specs,
                    'dateForm': date_form
                }

                return render(request, 'vda4913.html', context)

            else:
                return JsonResponse({"error": "Invlaid date_form"}, status=400)
        else:
            date_form = VDA4913DateInput()
            context = {
                'shipArticles': [],
                'dateForm': date_form
            }

        return render(request, 'vda4913.html', context)
    
    except Exception as e:
        import traceback
        error_message = f"An error occurred: {str(e)}\n{traceback.format_exc()}"
    
        return render(request, 'error.html', {'error_message': error_message})