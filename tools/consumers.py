import base64
import json
import os

from channels.generic.websocket import AsyncWebsocketConsumer
from utils.mail_handler import send_email

internal_ip_mapping = {
    "192.168.1.88": "Piotr",
    "192.168.1.69": "Piotr",
    "192.168.1.239": "Paweł",
    "192.168.1.116": "Michał",
    "192.168.1.127": "Oliwia",
    "192.168.1.177": "TV PH",
    "192.168.1.194": "TV KTL",
}

connected_clients = {}

class BarcodeConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        # Join the "scanned_codes" group
        self.group_name = "scanned_codes"
        await self.channel_layer.group_add(
            self.group_name,
            self.channel_name
        )
        await self.accept()

    async def disconnect(self, close_code):
        # Leave the "scanned_codes" group
        await self.channel_layer.group_discard(
            self.group_name,
            self.channel_name
        )

    # Receive message from group
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        code = text_data_json['code']

        # Send the message to WebSocket
        await self.send(text_data=json.dumps({
            'code': code
        }))

    # Receive message from the channel layer
    async def send_scanned_code(self, event):
        # Extract all necessary data from the event
        code = event['code']
        crud_type = event['crud_type']
        timestamp = event['timestamp']
        line = event['line']
        name = event['name']
        additional_info = event['additional_info']
        used = event['used']

        # Send the barcode data to the WebSocket client
        await self.send(text_data=json.dumps({
            'code': code,                # The barcode code
            'crud_type': crud_type,                # The barcode code
            'timestamp': timestamp,      # The timestamp when barcode was saved
            'line': line,                # The line associated with the barcode
            'name': name,                # The name associated with the barcode
            'additional_info': additional_info,  # Any additional info (if available)
            'used': used                 # Whether the barcode was used
        }))

class PlanKTLConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.group_name = "plan_ktl_updates"  # Group name for plan updates
        await self.channel_layer.group_add(self.group_name, self.channel_name)
        await self.accept()

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(self.group_name, self.channel_name)

    # This method is triggered when a saved plan is added or updated
    async def send_saved_plan_ktl(self, event):
        action = event['action']  # 'add' or 'update'

        # Send the updated plan to WebSocket clients
        await self.send(text_data=json.dumps({
            'type': action,  # Either 'add' or 'update'
        }))

    # This method is triggered when a saved plan is deleted
    async def send_deleted_plan_ktl(self, event):
        # Send a delete action to WebSocket clients
        await self.send(text_data=json.dumps({
            'type': 'delete',  # Indicating a delete action
        }))

    async def receive(self, text_data):
        # This method is called when a message is received from the WebSocket.
        text_data_json = json.loads(text_data)
        message_type = text_data_json["type"]

        if message_type == "screenshot":
            image_data = text_data_json["data"]
            timestamp = text_data_json["timestamp"] 

            image_bytes = base64.b64decode(image_data.split(',')[1]) 

            # Create a filename (you can customize this)
            filename = f"screenshot_{timestamp}.png" 
            filepath = os.path.join("screenshots", filename) 

            # Create the "screenshots" directory if it doesn't exist
            os.makedirs(os.path.dirname(filepath), exist_ok=True) 

            # Save the image to the file
            with open(filepath, "wb") as f:
                f.write(image_bytes)

            # Save logic here

        elif message_type == "status":
            title = text_data_json["title"]
            message = text_data_json["message"]

            # Execute your function based on the received message
            await self.async_send_email(title, message)

            # Send a response to the WebSocket (optional)
            await self.send(text_data=json.dumps({
                'message': "Message received and processed."
            }))

    async def async_send_email(self, title, message):
        send_email(
            subject=title,
            body=message,
            receiver="piotr.babicz@galwanotechnika.eu"
        )

class PlanPConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.group_name = "plan_p_updates"  # Group name for plan updates
        await self.channel_layer.group_add(self.group_name, self.channel_name)
        await self.accept()

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(self.group_name, self.channel_name)

    # This method is triggered when a saved plan is added or updated
    async def send_saved_plan_p(self, event):
        action = event['action']  # 'add' or 'update'

        # Send the updated plan to WebSocket clients
        await self.send(text_data=json.dumps({
            'type': action,  # Either 'add' or 'update'
        }))

    # This method is triggered when a saved plan is deleted
    async def send_deleted_plan_p(self, event):
        # Send a delete action to WebSocket clients
        await self.send(text_data=json.dumps({
            'type': 'delete',  # Indicating a delete action
        }))

    async def receive(self, text_data):
        # This method is called when a message is received from the WebSocket.
        text_data_json = json.loads(text_data)
        message_type = text_data_json["type"]

        if message_type == "screenshot":
            image_data = text_data_json["data"]
            timestamp = text_data_json["timestamp"] 

            image_bytes = base64.b64decode(image_data.split(',')[1]) 

            # Create a filename (you can customize this)
            filename = f"p_screenshot_{timestamp}.png" 
            filepath = os.path.join("screenshots", filename) 

            # Create the "screenshots" directory if it doesn't exist
            os.makedirs(os.path.dirname(filepath), exist_ok=True) 

            # Save the image to the file
            with open(filepath, "wb") as f:
                f.write(image_bytes)

            # Save logic here

        elif message_type == "status":
            title = text_data_json["title"]
            message = text_data_json["message"]

            # Execute your function based on the received message
            await self.async_send_email(title, message)

            # Send a response to the WebSocket (optional)
            await self.send(text_data=json.dumps({
                'message': "Message received and processed."
            }))

    async def async_send_email(self, title, message):
        send_email(
            subject=title,
            body=message,
            receiver="piotr.babicz@galwanotechnika.eu"
        )

class ConnectedClientConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.page = self.scope["url_route"]["kwargs"]["page_name"]
        self.group_name = f"clients_{self.page}"
        
        # Get the client's internal IP address from scope
        ip_address = self.scope["client"][0]
        
        # Assign a custom name based on the IP address, or default to "Unknown"
        client_name = internal_ip_mapping.get(ip_address, f"Unknown-{ip_address}")
        
        self.client_name = client_name

        if self.page not in connected_clients:
            connected_clients[self.page] = set()
        
        connected_clients[self.page].add(self.client_name)

        await self.channel_layer.group_add(self.group_name, self.channel_name)
        await self.accept()
        await self.send_clients_list()

    async def disconnect(self, close_code):
        connected_clients[self.page].discard(self.client_name)
        if not connected_clients[self.page]:  # Clean up empty sets
            del connected_clients[self.page]

        await self.channel_layer.group_discard(self.group_name, self.channel_name)
        await self.send_clients_list()

    async def send_clients_list(self):
        clients_list = list(connected_clients.get(self.page, []))
        await self.channel_layer.group_send(
            self.group_name,
            {
                "type": "update_clients",
                "clients": clients_list
            }
        )

    async def update_clients(self, event):
        await self.send(text_data=json.dumps({"clients": event["clients"]}))
