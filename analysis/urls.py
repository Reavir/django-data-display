from django.urls import path
from . import views

urlpatterns = [
    path('mubea/', views.mubea_analysis, name='mubea_analysis'),
    path('mubea_year/', views.mubea_year, name='mubea_year'),
    path('bruss/', views.bruss_analysis, name='bruss_analysis'),
    path('usage', views.usage, name='usage'),
    path('usage/siegenia', views.usage_siegenia, name='usage_siegenia'),
    path('usage/mubea', views.usage_mubea, name='usage_mubea'),
    path('usage/nicr', views.usage_nicr, name='usage_nicr'),
    path('usage/zn', views.usage_zn, name='usage_zn'),
]
