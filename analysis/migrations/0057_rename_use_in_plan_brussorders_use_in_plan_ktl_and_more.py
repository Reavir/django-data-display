# Generated by Django 4.2.3 on 2025-02-10 10:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0056_alter_brussorders_use_in_plan_and_more'),
    ]

    operations = [
        migrations.RenameField(
            model_name='brussorders',
            old_name='use_in_plan',
            new_name='use_in_plan_ktl',
        ),
        migrations.RenameField(
            model_name='metalmartinorders',
            old_name='use_in_plan',
            new_name='use_in_plan_ktl',
        ),
        migrations.AddField(
            model_name='brussorders',
            name='use_in_plan_p',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='metalmartinorders',
            name='use_in_plan_p',
            field=models.BooleanField(default=True),
        ),
    ]
