# Generated by Django 5.1.6 on 2025-03-03 12:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0061_mubeashipment_row_no'),
    ]

    operations = [
        migrations.AddField(
            model_name='mubeaident',
            name='number_raw',
            field=models.CharField(default='00000000', max_length=22),
        ),
        migrations.AlterField(
            model_name='mubeaident',
            name='number',
            field=models.CharField(max_length=20, unique=True),
        ),
    ]
