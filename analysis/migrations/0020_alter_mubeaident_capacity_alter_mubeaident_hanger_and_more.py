# Generated by Django 4.2.3 on 2024-01-15 07:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0019_alter_faureciaident_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mubeaident',
            name='capacity',
            field=models.IntegerField(default=1),
        ),
        migrations.AlterField(
            model_name='mubeaident',
            name='hanger',
            field=models.IntegerField(default=1),
        ),
        migrations.AlterField(
            model_name='mubeaident',
            name='line',
            field=models.IntegerField(default=1),
        ),
    ]
