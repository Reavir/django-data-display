# Generated by Django 4.2.3 on 2023-11-10 10:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0006_remove_brussorders_delay_remove_brussorders_week_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='brussorders',
            name='orderId',
            field=models.CharField(max_length=255),
        ),
    ]
