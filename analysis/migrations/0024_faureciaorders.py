# Generated by Django 4.2.3 on 2024-02-27 10:02

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0023_siegeniaident_siegeniaorders'),
    ]

    operations = [
        migrations.CreateModel(
            name='FaureciaOrders',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('orderId', models.CharField(max_length=255)),
                ('quantity', models.IntegerField()),
                ('ident', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='analysis.faureciaident')),
            ],
        ),
    ]
