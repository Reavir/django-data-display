# Generated by Django 4.2.3 on 2023-12-05 12:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0014_mubeaident_hanger_mubeaident_line'),
    ]

    operations = [
        migrations.AddField(
            model_name='mubeaident',
            name='capacity',
            field=models.IntegerField(default=0),
        ),
    ]
