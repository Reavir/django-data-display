# Generated by Django 4.2.3 on 2024-04-15 11:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0031_metalmartinorders_packaging'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='metalmartinorders',
            name='packaging',
        ),
        migrations.AddField(
            model_name='metalmartinident',
            name='packaging',
            field=models.CharField(default='', max_length=255),
        ),
    ]
