# Generated by Django 4.2.3 on 2024-10-18 07:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0041_alter_hutchinsonorders_ident'),
    ]

    operations = [
        migrations.AddField(
            model_name='metalmartinorders',
            name='dateDeadline',
            field=models.DateField(null=True),
        ),
    ]
