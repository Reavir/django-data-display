# Generated by Django 4.2.3 on 2024-05-21 09:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0032_remove_metalmartinorders_packaging_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='mubeaident',
            name='client',
            field=models.CharField(blank=True, default='', max_length=255, null=True),
        ),
    ]
