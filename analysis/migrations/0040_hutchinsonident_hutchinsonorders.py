# Generated by Django 4.2.3 on 2024-09-17 07:23

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0039_rename_n_exits_brussident_n_exits_ph_and_more'),
    ]

    operations = [
        migrations.CreateModel(
            name='HutchinsonIdent',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.CharField(max_length=255, unique=True)),
                ('description', models.TextField(blank=True, null=True)),
                ('n_exits_ph', models.IntegerField(default=0)),
                ('packaging', models.CharField(default='', max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='HutchinsonOrders',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('orderId', models.CharField(max_length=255)),
                ('quantity', models.IntegerField()),
                ('dateDelivered', models.DateField(null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('ident', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='analysis.metalmartinident')),
            ],
        ),
    ]
