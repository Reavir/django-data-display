# Generated by Django 4.2.3 on 2024-01-25 13:15

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0020_alter_mubeaident_capacity_alter_mubeaident_hanger_and_more'),
    ]

    operations = [
        migrations.CreateModel(
            name='MubeaOrders',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('orderId', models.CharField(max_length=255)),
                ('delivery_date', models.DateField(null=True)),
                ('delivery_note', models.CharField(max_length=10)),
                ('charge', models.CharField(max_length=255)),
                ('raw_ident_name', models.IntegerField()),
                ('quantity', models.IntegerField()),
                ('production_date', models.DateField(null=True)),
                ('shipment_date', models.DateField(null=True)),
                ('ident', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='analysis.mubeaident')),
            ],
        ),
    ]
