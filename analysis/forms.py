from django import forms
import datetime

class MubeaYearForm(forms.Form):
    date_from = forms.IntegerField(label='From:', required=False)
    date_to = forms.IntegerField(label='To:', required=False)

    def __init__(self, *args, **kwargs):
        super(MubeaYearForm, self).__init__(*args, **kwargs)
        self.fields['date_from'].initial = datetime.datetime.now().year
        self.fields['date_to'].initial = datetime.datetime.now().year

class UsageDateForm(forms.Form):
    date_from = forms.DateField(widget=forms.DateInput(attrs={'type': 'date'}), label='From:', required=True)
    date_to = forms.DateField(widget=forms.DateInput(attrs={'type': 'date'}), label='To:', required=True)
