import datetime
import locale
import logging
import math
import time
import pandas as pd
from statsmodels.tsa.seasonal import seasonal_decompose
from django.db.models import Sum, Q
from django.shortcuts import render
from django.db.models.functions import ExtractWeek, ExtractYear
from django.db import connection
from .forms import MubeaYearForm, UsageDateForm
from utils.calculations import number_separator, start_end_day_cw
from .models import MubeaDemand, MubeaProduction, MubeaShipment, MubeaStock, SiegeniaOrders
from tools.models import Barcodes
import numpy as np
from sklearn.linear_model import LinearRegression
from django.views.decorators.csrf import csrf_exempt


def mubea_analysis(request):
    logger = logging.getLogger(__name__)
    start_time = time.time()
    logger.info(f'[{datetime.datetime.now()}]' + ' START VIEW:  mubea_analysis')
    if request.method == 'POST':
        selected_cw = request.POST.get("cw")
        if '/' not in selected_cw:    
            year = datetime.datetime.now().year % 100 
            cw = f'{int(selected_cw)}/{year}'
            calendar_week = int(selected_cw)

        else:
            calendar_week = int(selected_cw.split('/')[0])
            cw = selected_cw
    else:
        cw = datetime.date.today().isocalendar()[1]
        calendar_week = cw
        year = datetime.datetime.now().year % 100
        cw = f'{cw}/{year}'

    # QUEERY
    try:
        start_day, end_day = start_end_day_cw(calendar_week)
        start_day = start_day.strftime('%Y-%m-%d')
        end_day = end_day.strftime('%Y-%m-%d')
        #
        # In range of weeks in a year
        #
        demand_query_year = MubeaDemand.objects.annotate(
            week_number=ExtractWeek('date'),
            year_number=ExtractYear('date')
            ).values('week_number', 'year_number').annotate(
                total_demand=Sum('quantity')
            )
        shipment_query_year = MubeaShipment.objects.annotate(
            week_number=ExtractWeek('date'),
            year_number=ExtractYear('date')
            ).values('week_number', 'year_number').annotate(
                total_shipment=Sum('quantity')
            )
        production_query_year = MubeaProduction.objects.annotate(
            week_number=ExtractWeek('date'),
            year_number=ExtractYear('date')
            ).values('week_number', 'year_number').annotate(
                total_production=Sum('l1_quantity') + Sum('l2_quantity') + Sum('l7_quantity')
            )
        df_demand_year = pd.DataFrame(list(demand_query_year))
        df_demand_year['ww/yy'] = df_demand_year.apply(lambda row: f"{row['week_number']:02d}/{str(row['year_number'])[-2:]}", axis=1)
        df_demand_year = df_demand_year.drop(['week_number', 'year_number'], axis=1)
        df_shipment_year = pd.DataFrame(list(shipment_query_year))
        df_shipment_year['ww/yy'] = df_shipment_year.apply(lambda row: f"{row['week_number']:02d}/{str(row['year_number'])[-2:]}", axis=1)
        df_shipment_year = df_shipment_year.drop(['week_number', 'year_number'], axis=1)
        df_production_year = pd.DataFrame(list(production_query_year))
        df_production_year['ww/yy'] = df_production_year.apply(lambda row: f"{row['week_number']:02d}/{str(row['year_number'])[-2:]}", axis=1)
        df_production_year = df_production_year.drop(['week_number', 'year_number'], axis=1)

        data_year_range = pd.merge(df_demand_year, df_shipment_year, on='ww/yy', how='outer')
        data_year_range = pd.merge(data_year_range, df_production_year, on='ww/yy', how='outer')

        data_year_range = data_year_range.fillna(0)
        data_year_range['total_backlog'] = data_year_range.apply(lambda row: row['total_demand'] - row['total_shipment'], axis=1)
        total_backlog = number_separator(int(data_year_range['total_backlog'].sum()))

        data_year_range = data_year_range[~data_year_range['ww/yy'].str.endswith('/22')]
        data_year_range = data_year_range.sort_values(by=['ww/yy'], key=lambda x: x.str.split('/').apply(lambda x: (int(x[1]), int(x[0]))))
        data_year_range_weeks = list(data_year_range['ww/yy'])
        data_year_range_demand = list(data_year_range['total_demand'])
        data_year_range_shipment = list(data_year_range['total_shipment'])
        data_year_range_production = list(data_year_range['total_production'])
        data_year_range_backlog = list(data_year_range['total_backlog'])
        #
        # In range of days in a week
        #
        demand_query_week = MubeaDemand.objects.filter(
            date__range=[start_day , end_day],
            ).values('ident__number').annotate(total_demand=Sum('quantity'))
        shipment_query_week = MubeaShipment.objects.filter(
                date__range=[start_day , end_day],
            ).values('ident__number').annotate(total_shipment=Sum('quantity'))
        production_query_week = MubeaProduction.objects.filter(
                date__range=[start_day , end_day],
            ).values('ident__number').annotate(total_production=Sum('l1_quantity') + Sum('l2_quantity') + Sum('l7_quantity'))

        stock_query_week = MubeaStock.objects.filter(
                date=start_day,
            ).values('ident__number').annotate(total_stock=Sum('quantity'))

        df_demand = pd.DataFrame(list(demand_query_week))
        df_shipment = pd.DataFrame(list(shipment_query_week))
        df_production = pd.DataFrame(list(production_query_week))
        df_stock = pd.DataFrame(list(stock_query_week))
        try:
            data_date_range = pd.merge(df_demand, df_shipment, on='ident__number', how='outer')
        except:
            df_shipment = pd.DataFrame(index=df_demand.index)
            df_shipment['ident__number'] = df_demand['ident__number']
            df_shipment['total_shipment'] = 0
            data_date_range = pd.merge(df_demand, df_shipment, on='ident__number', how='outer')

        try:
            data_date_range = pd.merge(data_date_range, df_production, on='ident__number', how='outer')
        except:
            df_production = pd.DataFrame(index=data_date_range.index)
            df_production['ident__number'] = data_date_range['ident__number']
            df_production['total_production'] = 0
            data_date_range = pd.merge(data_date_range, df_production, on='ident__number', how='outer')
        
        if df_stock.empty:
            df_stock = pd.DataFrame({'total_stock': [0]})
            data_date_range['total_stock'] = df_stock['total_stock']
        else:
            data_date_range = pd.merge(data_date_range, df_stock, on='ident__number', how='outer')

        data_date_range = data_date_range.fillna(0)
        data_date_range = data_date_range[(data_date_range['total_production'] != 0) | (data_date_range['total_shipment'] != 0) | (data_date_range['total_demand'] != 0)]

        data_date_range['total_demand'] = data_date_range['total_demand'].astype(int)
        data_date_range['total_shipment'] = data_date_range['total_shipment'].astype(int)
        data_date_range['total_production'] = data_date_range['total_production'].astype(int)
        data_date_range['total_stock'] = data_date_range['total_stock'].astype(int)
        
        data_date_range_ident = list(data_date_range['ident__number'])
        data_date_range_demand = list(data_date_range['total_demand'])
        data_date_range_stock = list(data_date_range['total_stock'])
        data_date_range_shipment = list(data_date_range['total_shipment'])
        data_date_range_production = list(data_date_range['total_production'])
        #
        # Percentages
        #
        with_demand = data_date_range[data_date_range['total_demand'] != 0].copy()
        with_demand['shipment per demand'] = with_demand['total_shipment'] / with_demand['total_demand']
        with_demand['shipment per demand'] = with_demand['shipment per demand'].clip(upper=1)

        total_percentage = min(math.ceil(with_demand['shipment per demand'].sum()/len(with_demand['shipment per demand']) * 100), 100)
        all_id_w_demand = list(with_demand['ident__number'])
        ship_per_demand = list(with_demand['shipment per demand'])

        context = {
            'calendar_week': cw,
            'total_percentage_plus': total_percentage,
            'total_percentage_minus' : 100 - total_percentage,
            'id_per_ident': all_id_w_demand,
            'id_per_values': ship_per_demand,

            'demand_ident': data_date_range_ident,
            'demand_values': data_date_range_demand,
            'stock_values': data_date_range_stock,
            'shipment_values': data_date_range_shipment,
            'production_values': data_date_range_production,

            'weeks_list': data_year_range_weeks,
            'total_shipment_list': data_year_range_shipment,
            'total_demand_list': data_year_range_demand,
            'total_production_list': data_year_range_production,
            'total_backlog_list': data_year_range_backlog,
            'total_backlog': total_backlog,
        }
        
        end_time = time.time()
        
        logger.info(f'[{datetime.datetime.now()}]' + ' END VIEW:    mubea_analysis           %0.4f seconds' % (end_time - start_time))
        
        return render(request, 'Mubea_excel_display.html', context)
        
    except Exception as e:
        import traceback
        error_message = f"An error occurred: {str(e)}\n{traceback.format_exc()}"
        logger.info(f'[{datetime.datetime.now()}]' + ' END VIEW:    mubea_analysis           ' + error_message)
        context = {
            'calendar_week': cw
        }

        return render(request, 'Mubea_excel_display.html', context)


def bruss_analysis(request):
    logger = logging.getLogger(__name__)
    start_time = time.time()
    logger.info(f'[{datetime.datetime.now()}]' + ' START VIEW:  bruss_analysis')
    if request.method == 'POST':
        selected_cw = request.POST.get("cw")
        if '/' not in selected_cw:    
            year = datetime.datetime.now().year % 100 
            cw = f'{int(selected_cw)}/{year}'
            calendar_week = int(selected_cw)

        else:
            calendar_week = int(selected_cw.split('/')[0])
            cw = selected_cw
    else:
        cw = datetime.date.today().isocalendar()[1]
        calendar_week = cw
        year = datetime.datetime.now().year % 100
        cw = f'{cw}/{year}'

    try:
        start_day, end_day = start_end_day_cw(calendar_week)
        start_day = start_day.strftime('%Y-%m-%d')
        end_day = end_day.strftime('%Y-%m-%d')
        # analysis_brussident"."number" AS number,
        # "analysis_brussident"."supplier" AS supplier,
        sql_query = f'''
            SELECT
                "analysis_brussident"."description" AS desc,
                COALESCE(stock_quantity, 0) AS stock,
                COALESCE(orders_quantity, 0) AS ordres,
                COALESCE(shipment, 0),
                COALESCE(confirmation, 0),
                COALESCE(deadline, 0)
            FROM 
                "analysis_brussident"
            LEFT OUTER JOIN (
            SELECT
                "ident_id",
                "quantity" AS stock_quantity
            FROM
                "analysis_brussstock"
            GROUP BY
                "ident_id"
            ) AS "analysis_brussstock"
            ON
            ("analysis_brussident"."id" = "analysis_brussstock"."ident_id")
                LEFT OUTER JOIN (
                SELECT
                    "ident_id",
                    "quantity" AS orders_quantity,
                    "dateShipment" AS shipment,
                    "dateConfirmed" AS confirmation,
                    "dateDeadline" AS deadline
                FROM
                    "analysis_brussorders"
                WHERE
                    ("dateConfirmed" >= "{start_day}" AND "dateConfirmed" <= "{end_day}")
                GROUP BY
                    "ident_id"
                ) AS "analysis_brussorders"
                ON
                ("analysis_brussident"."id" = "analysis_brussorders"."ident_id")
        '''

        with connection.cursor() as cursor:
            cursor.execute(sql_query)
            result = cursor.fetchall()
        

        all_id = []
        all_stock = []
        all_order = []
        all_delay = []

        for row in result:
            name = row[0]
            stock = row[1]
            order = row[2]
            delay = row[3]

            if order == 0 and delay == 0:
                continue

            all_id.append(name)
            all_stock.append(stock)
            all_order.append(order)
            all_delay.append(delay)

        # Combine the lists into a list of tuples
        combined_lists = list(zip(all_id, all_stock, all_order, all_delay))
        sorted_combined = sorted(combined_lists, key=lambda x: x[2], reverse=True)
        all_id, all_stock, all_order, all_delay = zip(*sorted_combined)

        end_time = time.time()

        context = {
                'calendar_week': cw,
                'all_id': all_id,
                'all_stock': all_stock,
                'all_order': all_order,
                'all_delay': all_delay
        }

        logger.info(f'[{datetime.datetime.now()}]' + ' END VIEW:    bruss_analysis           %0.4f seconds' % (end_time - start_time))

        return render(request, 'Bruss_excel_display.html', context)
    
    except Exception as e:
        import traceback
        error_message = f"An error occurred: {str(e)}\n{traceback.format_exc()}"

        return render(request, 'error.html', {'error_message': error_message})
    

@csrf_exempt
def mubea_year(request):
    if request.method == 'POST':
        year_form = MubeaYearForm(request.POST)
        if year_form.is_valid():
            date_from = year_form.cleaned_data['date_from']
            date_to = year_form.cleaned_data['date_to']
    else:
        year_form = MubeaYearForm()
        date_from = datetime.datetime.now().year
        date_to = datetime.datetime.now().year

    try:
        if date_from == date_to:
            result = MubeaProduction.objects.filter(
                date__year=date_from
            ).values(
                'ident__number',
                'ident__type',
                'date__month',
                'date__year',
                'ident__description'
            ).annotate(
                total_quantity=Sum('l1_quantity') + Sum('l2_quantity') + Sum('l7_quantity'),
            )
        else:
            if date_from > date_to:
                date_from, date_to = date_to, date_from

            result = MubeaProduction.objects.filter(
                date__range=(datetime.date(date_from, 1, 1), datetime.date(date_to, 12, 31))
            ).values(
                'ident__number',
                'ident__type',
                'date__month',
                'date__year',
                'ident__description'
            ).annotate(
                total_quantity=Sum('l1_quantity') + Sum('l2_quantity') + Sum('l7_quantity'),
            )

        st_bu_dict = {}
        month_dict = {}
        ident_val_dict = {}
        for i in result:
            st_bu_dict[i['ident__type']] = st_bu_dict.get(i['ident__type'], 0) + i['total_quantity']
            #month_dict[i['date__month']] = month_dict.get(i['date__month'], 0) + i['total_quantity']
            month_dict[f"{i['date__month']}/{i['date__year']}"] = month_dict.get(f"{i['date__month']}/{i['date__year']}", 0) + i['total_quantity']
            ident_val_dict[f"{i['ident__number']}  {i['ident__description']}"] = ident_val_dict.get(f"{i['ident__number']}  {i['ident__description']}", 0) + i['total_quantity']

        month_dict = dict(sorted(month_dict.items(), key=lambda item: (int(item[0].split('/')[1]), int(item[0].split('/')[0]))))

        # Convert dictionary to numpy arrays
        X = np.array([i for i in range(len(month_dict.keys()))]).reshape(-1, 1)
        y = np.array(list(month_dict.values()))

        # Create a linear regression model
        model = LinearRegression()

        # Fit the model to the data
        model.fit(X, y)

        # Make predictions
        predictions = model.predict(X)

        # Print the slope (coefficient) and intercept of the line
        df = pd.DataFrame(list(month_dict.items()), columns=['Date', 'Quantity'])

        # Convert the 'Date' column to datetime format
        df['Date'] = pd.to_datetime(df['Date'], format='%m/%Y')

        # Set the 'Date' column as the index
        df.set_index('Date', inplace=True)
        season_data = ''
        trend_data = ''
        # Perform seasonal decomposition
        if date_from != date_to:
            result = seasonal_decompose(df['Quantity'], model='multiplicative', period=12, extrapolate_trend='freq')
            season_data = dict(zip(df.index.strftime('%m/%Y'), result.seasonal * 1100000))
            trend_data = dict(zip(df.index.strftime('%m/%Y'), result.trend))
            residual_data = dict(zip(df.index.strftime('%m/%Y'), result.resid))


        context = {
            'year_form': year_form,
            'shape_dict': st_bu_dict,
            'month_dict': month_dict,
            'ident_val_dict': ident_val_dict,
            'chart_data': {
                'X': X.flatten().tolist(),
                'y': y.tolist(),
                'predictions': predictions.tolist(),
                }  ,
            'seasonal_data': season_data,
            'trend_data': trend_data
        }

        return render(request, 'mubea_year.html', context)
    
    except Exception as e:
        import traceback
        error_message = f"An error occurred: {str(e)}\n{traceback.format_exc()}"

        return render(request, 'error.html', {'error_message': error_message})


def usage(request):
    try:
        return render(request, 'usage.html')
    
    except Exception as e:
        import traceback
        error_message = f"An error occurred: {str(e)}\n{traceback.format_exc()}"

        return render(request, 'error.html', {'error_message': error_message})


def usage_siegenia(request):
    try:
        start_date = datetime.datetime.now() - datetime.timedelta(days=1)
        start_date = datetime.datetime.combine(start_date, datetime.time.min)

        end_date = datetime.datetime.now()
        end_date = datetime.datetime.combine(end_date, datetime.time.max)
        barcodes_query = Barcodes.objects.filter(
            (
                Q(line='Cynkowanie') &
                Q(code__regex=r'^\d{10}$') &
                Q(timestamp__range=(start_date, end_date))
            ) |
            (
                Q(code__startswith='SGN') &
                Q(line='Cynkowanie') &
                Q(timestamp__range=(start_date, end_date))
            )
        )
        codes_to_search = barcodes_query.values_list('code', flat=True)

        siegenia_dict = (
            SiegeniaOrders.objects
            .filter(orderId__in=codes_to_search)
            .values('ident__number')
            .annotate(total_quantity=Sum('quantity'))
            .values_list('ident__number', 'ident__description', 'ident__weight_kg', 'ident__width_mm', 'ident__length_mm', 'ident__height_mm', 'ident__price_euro', 'total_quantity')
        )
        siegenia_list = []
        header = ['ID', 'Name','Total Quantity', 'Weight [kg]', 'Surface [m²]','Volume [dm³]', 'Price [€]']
        for i in siegenia_dict:
            qty = i[7]
            t_weight = round(i[2] * qty, 2) if i[2] is not None else 'No data'
            price = round(qty * i[6], 2) if i[6] != 0 else 'No data'
            try:
                width  = i[3] / 1000
                length = i[4] / 1000
                height = i[5] / 1000

                siegenia_list.append([i[0], i[1], qty, t_weight, round(qty * 2 * (width * length + width * height + length * height), 2), round(width * length * height * 1000, 4), price])

            except TypeError:
                
                siegenia_list.append([i[0], i[1], qty, t_weight, 'No data', 'No data', price])

        context = {
            'header': header,
            'table_data': siegenia_list,
            'date_from': start_date,
            'date_to': end_date
        }
        return render(request, 'usage_siegenia.html', context)
    
    except Exception as e:
        import traceback
        error_message = f"An error occurred: {str(e)}\n{traceback.format_exc()}"

        return render(request, 'error.html', {'error_message': error_message})


def usage_mubea(request):
    try:
        if request.method == 'POST':
            date_form = UsageDateForm(request.POST)
            if date_form.is_valid():
                start_date = date_form.cleaned_data['date_from']
                start_date = datetime.datetime.combine(start_date, datetime.time.min)

                end_date = date_form.cleaned_data['date_to']
                end_date = datetime.datetime.combine(end_date, datetime.time.max)

        else:
            date_form = UsageDateForm()
            today = datetime.datetime.now()
            # Calculate the number of days to Monday
            days_to_monday = today.weekday() - 0  # 0 for Monday
            if days_to_monday < 0:
                # If today is Sunday (6), we want to go back 6 days to Monday (0)
                days_to_monday += 7

            # Calculate Monday of the current week
            monday = today - datetime.timedelta(days=days_to_monday)
            start_date = datetime.datetime.combine(monday, datetime.time.min)

            # Calculate Sunday of the current week
            sunday = monday + datetime.timedelta(days=6)
            end_date = datetime.datetime.combine(sunday, datetime.time.max)

        mubea_query = MubeaProduction.objects.filter(date__range=(start_date, end_date))
        locale.setlocale(locale.LC_ALL, 'pl_PL.UTF-8')
        mubea_dict = {}
        total_turnover = 0
        total_weight = 0
        total_qty = 0
        total_qty_stange = 0
        total_qty_bugel = 0
        total_surface = 0
        total_ah = 0
        total_ni = 0

        for i in mubea_query:
            total_turnover += (i.l1_quantity + i.l2_quantity) * i.ident.price_eur
            total_weight += (i.l1_quantity + i.l2_quantity) * i.ident.weight_kg
            total_surface += (i.l1_quantity + i.l2_quantity) * i.ident.area_dm /100
            total_qty += (i.l1_quantity + i.l2_quantity) 
            total_ah += (i.l1_quantity + i.l2_quantity) * (i.ident.area_dm *10*1.1*0.00001*8.9*1.05*1000 - i.ident.area_dm *10*1.1*0.00001*8.9*1.05*1000 /5) /1.095
            total_ni += (i.l1_quantity + i.l2_quantity) * i.ident.area_dm * 10*1.1*0.00001*8.9*1.05
            if i.ident.type == "Stange":
                total_qty_stange += (i.l1_quantity + i.l2_quantity)
            else:
                total_qty_bugel += (i.l1_quantity + i.l2_quantity)
            
            if i.ident.number not in mubea_dict:
                mubea_dict[i.ident.number] = [i.ident.description, i.ident.type, i.l1_quantity, i.l2_quantity, i.ident.area_dm]
            else:
                mubea_dict[i.ident.number] = [i.ident.description, i.ident.type, mubea_dict[i.ident.number][2] + i.l1_quantity, mubea_dict[i.ident.number][3] + i.l2_quantity, i.ident.area_dm]
    
        total_table = [
            ['Turnover', locale.format_string("%.2f", total_turnover, grouping=True) + ' €'],
            ['Weight', locale.format_string("%.2f", total_weight, grouping=True) + ' kg'],
            ['Quantity', locale.format_string("%d", total_qty, grouping=True) + ' pcs.'],
            ['∟ Stange', locale.format_string("%d", total_qty_stange, grouping=True) + ' pcs.'],
            ['∟ Bügel', locale.format_string("%d", total_qty_bugel, grouping=True) + ' pcs.'],
            ['Surface', locale.format_string("%.2f", total_surface, grouping=True) + ' m²'],
            ['Theoretical Ah', locale.format_string("%.2f", total_ah, grouping=True) + ' Ah'],
            ['Theoretical Ni', locale.format_string("%.2f", total_ni, grouping=True) + ' kg'],
        ]

        mubea_list = []
        for key, value in zip(mubea_dict.keys(), mubea_dict.values()):
            l_1 = value[2]
            l_2 = value[3]
            area = value[4]
            if value[1] == 'Stange':
                otf = 1.1
                soll = area*10*otf*0.00001*8.9*1.05*1000
                ah = (soll - soll/5) /1.095

                mubea_list.append([key, value[1], value[0], l_1, l_2, area, round((l_1 + l_2) * area /100, 2), '10,00', '1,10', round(soll, 2), round(soll * 1.12, 2), round(ah, 2), round(soll * (l_1 + l_2) /1000, 2)])

            else:
                otf = 1.2
                soll = area*10*otf*0.00001*8.9*1.05*1000
                ah = (soll - soll/5) /1.095

                mubea_list.append([key, value[1], value[0], l_1, l_2, area, round((l_1 + l_2) * area /100, 2), '10,00', '1,20', round(soll, 2), round(soll * 1.12, 2), round(ah, 2), round(soll * (l_1 + l_2) /1000, 2)])

        header_main = ['Mubea Part Number ', 'Type', 'Description', 'Production L1', 'Production L2', 'Surface/pc. [dm²]', 'Total surface [m²]', 'Layer thickness [µm]', 'Over-target factor 10% = 1µm', 'theoritical Ni/pc.', 'contractual Ni/pc.', 'Ah/pc.', 'Total Ni [kg]']

        context = {
            'date_form': date_form,
            'header_main': header_main,
            'table_data_main': mubea_list,
            'date_from': start_date,
            'date_to': end_date,
            'total_table': total_table
        }
        return render(request, 'usage_mubea.html', context)
    
    except Exception as e:
        import traceback
        error_message = f"An error occurred: {str(e)}\n{traceback.format_exc()}"

        return render(request, 'error.html', {'error_message': error_message})


def usage_nicr(request):
    try:
        
        return render(request, 'usage_chni.html')
    
    except Exception as e:
        import traceback
        error_message = f"An error occurred: {str(e)}\n{traceback.format_exc()}"

        return render(request, 'error.html', {'error_message': error_message})


def usage_zn(request):
    try:
        
        return render(request, 'usage_zinc.html')
    
    except Exception as e:
        import traceback
        error_message = f"An error occurred: {str(e)}\n{traceback.format_exc()}"

        return render(request, 'error.html', {'error_message': error_message})
