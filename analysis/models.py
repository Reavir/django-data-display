from django.db import models

# MUBEA

class MubeaIdent(models.Model):
    number = models.IntegerField(unique=True)
    type = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)
    area_dm = models.FloatField(default=0)
    line = models.IntegerField(default=1)
    hanger = models.IntegerField(default=1)
    capacity = models.IntegerField(default=1)
    price_eur = models.FloatField(default=0)
    weight_kg = models.FloatField(default=0)
    client = models.CharField(max_length=255, default='', null=True, blank=True)

class MubeaStock(models.Model):
    ident = models.ForeignKey(MubeaIdent, on_delete=models.CASCADE)
    date = models.DateField()
    quantity = models.IntegerField()

class MubeaProduction(models.Model):
    ident = models.ForeignKey(MubeaIdent, on_delete=models.CASCADE)
    date = models.DateField()
    l1_quantity = models.IntegerField(default=0)
    l2_quantity = models.IntegerField(default=0)
    l7_quantity = models.IntegerField(default=0)

class MubeaShipment(models.Model):
    ident = models.ForeignKey(MubeaIdent, on_delete=models.CASCADE)
    row_no = models.IntegerField(unique=True)
    orderId = models.CharField(max_length=255, null=True)
    spec = models.CharField(max_length=255, null=True)
    delivery_note = models.CharField(max_length=10, null=True) 
    charge = models.CharField(max_length=255, null=True)
    weight = models.IntegerField(null=True)
    packaging = models.CharField(max_length=255, null=True)
    date = models.DateField()
    quantity = models.IntegerField()

class MubeaDemand(models.Model):
    ident = models.ForeignKey(MubeaIdent, on_delete=models.CASCADE)
    date = models.DateField()
    quantity = models.IntegerField()

class MubeaOrders(models.Model):
    orderId = models.CharField(max_length=255, unique=True)
    dateDelivered = models.DateField(null=True)
    delivery_note = models.CharField(max_length=10)  
    charge = models.CharField(max_length=255)
    raw_ident_name = models.IntegerField()
    ident = models.ForeignKey(MubeaIdent, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    production_date = models.DateField(null=True)
    shipment_date = models.DateField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)


# BRUSS

class BrussIdent(models.Model):
    number = models.IntegerField(unique=True)
    supplier = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)
    n_exits_ktl = models.IntegerField(default=0)
    n_exits_ph = models.IntegerField(default=0)
    packaging = models.CharField(max_length=255, default='')
    days_deadline = models.IntegerField(default=0)
    default_quantity = models.IntegerField(default=0)
    hanger_ph = models.CharField(max_length=255)

    def __str__(self):
        return f'{self.number} {self.description}'

class BrussOrders(models.Model):
    ident = models.ForeignKey(BrussIdent, on_delete=models.CASCADE)
    dateDeadline = models.DateField(null=True)
    dateProduction = models.DateField(null=True)
    dateShipment = models.DateField(null=True)
    dateConfirmed = models.DateField(null=True)
    delivery_note = models.CharField(max_length=255)
    kanbanCard = models.CharField(max_length=255)  
    orderId = models.CharField(max_length=255)
    quantity = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    use_in_plan_p = models.BooleanField(default=True)
    use_in_plan_ktl = models.BooleanField(default=True)

class BrussStock(models.Model):
    ident = models.ForeignKey(BrussIdent, on_delete=models.CASCADE)
    quantity = models.IntegerField()

# FAURECIA

class FaureciaIdent(models.Model):
    number = models.IntegerField(unique=True)
    type = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)
    line = models.IntegerField(default=0)
    hanger = models.IntegerField(default=0)
    capacity = models.IntegerField(default=0)
    client = models.CharField(max_length=255, default='', null=True, blank=True)

class FaureciaProduction(models.Model):
    ident = models.ForeignKey(FaureciaIdent, on_delete=models.CASCADE)
    date = models.DateField()
    quantity = models.IntegerField(default=0)

class FaureciaShipment(models.Model):
    ident = models.ForeignKey(FaureciaIdent, on_delete=models.CASCADE)
    date = models.DateField()
    quantity = models.IntegerField()

class FaureciaDemand(models.Model):
    ident = models.ForeignKey(FaureciaIdent, on_delete=models.CASCADE)
    date = models.DateField()
    quantity = models.IntegerField()

class FaureciaOrders(models.Model):
    ident = models.ForeignKey(FaureciaIdent, on_delete=models.CASCADE)
    orderId = models.CharField(max_length=255)
    quantity = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    dateDelivered = models.DateField(null=True)
    delivery_note = models.CharField(max_length=255)
    client = models.CharField(max_length=255, null=True)

# SIEGENIA
    
class SiegeniaIdent(models.Model):
    number = models.CharField(max_length=255, unique=True)
    description = models.TextField(null=True, blank=True)
    weight_kg = models.FloatField(null=True, blank=True)
    width_mm  = models.FloatField(null=True, blank=True)
    length_mm = models.FloatField(null=True, blank=True)
    height_mm = models.FloatField(null=True, blank=True)
    price_euro = models.FloatField(null=True, blank=True)

class SiegeniaOrders(models.Model):
    ident = models.ForeignKey(SiegeniaIdent, on_delete=models.CASCADE)
    orderId = models.CharField(max_length=255)
    quantity = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    dateDelivered = models.DateField(null=True)
    delivery_note = models.CharField(max_length=10)  

# METAL MARTIN
    
class MetalMartinIdent(models.Model):
    number = models.CharField(max_length=255, unique=True)
    description = models.TextField(null=True, blank=True)
    n_exits_ph = models.IntegerField(default=0)
    n_exits_ktl = models.IntegerField(default=0)
    packaging = models.CharField(max_length=255, default='')

class MetalMartinOrders(models.Model):
    ident = models.ForeignKey(MetalMartinIdent, on_delete=models.CASCADE)
    orderId = models.CharField(max_length=255)
    quantity = models.IntegerField()
    dateDeadline = models.DateField(null=True)
    dateDelivered = models.DateField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    delivery_note = models.CharField(max_length=10)
    use_in_plan_p = models.BooleanField(default=True)
    use_in_plan_ktl = models.BooleanField(default=True)

# HUTCHINSON
    
class HutchinsonIdent(models.Model):
    number = models.CharField(max_length=255, unique=True)
    description = models.TextField(null=True, blank=True)
    n_exits_ph = models.IntegerField(default=0)
    packaging = models.CharField(max_length=255, default='')

class HutchinsonOrders(models.Model):
    ident = models.ForeignKey(HutchinsonIdent, on_delete=models.CASCADE)
    orderId = models.CharField(max_length=255)
    quantity = models.IntegerField()
    dateDelivered = models.DateField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)

# ADIENT

class AdientIdent(models.Model):
    number = models.IntegerField(unique=True)
    description = models.TextField(null=True, blank=True)
    line = models.IntegerField(default=2)
    client = models.CharField(max_length=255, default='Adient')
    type = models.CharField(max_length=255, default='Bügel')

class AdientOrders(models.Model):
    orderId = models.CharField(max_length=255, unique=True)
    dateDelivered = models.DateField(null=True)
    delivery_note = models.CharField(max_length=10)  
    ident = models.ForeignKey(AdientIdent, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    production_date = models.DateField(null=True)
    shipment_date = models.DateField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)
